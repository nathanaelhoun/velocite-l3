package fr.ufc.l3info.oprog.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Visiteur réalisant des vérifications sur l'AST du fichier de stations.
 */
public class ASTCheckerVisitor implements ASTNodeVisitor {

    private final Map<String, ERROR_KIND> errorMap;
    private final ArrayList<String> nomStations;
    private final ArrayList<String> declarations;

    public ASTCheckerVisitor() {
        nomStations = new ArrayList<>();
        errorMap = new HashMap<>();
        declarations = new ArrayList<>();
    }

    public Map<String, ERROR_KIND> getErrors() {
        return errorMap;
    }

    @Override
    public Object visit(ASTNode n) { return null; }

    @Override
    public Object visit(ASTListeStations n) {
        if (n == null || n.children == null || n.children.size() == 0) {
            assert n != null;
            errorMap.put("Error : EMPTY LIST at : " + n.getLCPrefix(), ERROR_KIND.EMPTY_LIST);
        } else {
            for (ASTNode child : n.children) {
                child.accept(this);
                for (int i = declarations.size(); i < 3; ++i) {
                    errorMap.put(i + " Error : MISSING DECLARATION in station at " + child.getLCPrefix(),
                            ERROR_KIND.MISSING_DECLARATION);
                }
                declarations.clear();
            }
        }

        return null;
    }

    @Override
    public Object visit(ASTStation n) {
        for (ASTNode child : n.children) {
            child.accept(this);
        }
        return null;
    }

    @Override
    public Object visit(ASTDeclaration n) {
        if (n != null && n.children.size() >= 2) {
            for (ASTNode child : n.children) {
                child.accept(this);
            }
            String field = n.children.get(0).value;
            String value = n.children.get(1).value;
            if (field.contains("capacite")) {
                if (!value.matches("^[1-9][0-9]*$")) {
                    errorMap.put("Error : WRONG NUMBER VALUE at " + n.getLCPrefix(),
                            ERROR_KIND.WRONG_NUMBER_VALUE);
                }
            } else {
                double v = Double.parseDouble(value);
                if (field.contains("latitude")) {
                    if (v < -90 || v > 90) {
                        errorMap.put("Error : WRONG NUMBER VALUE at " + n.getLCPrefix(),
                                ERROR_KIND.WRONG_NUMBER_VALUE);
                    }
                } else {
                    if (v < -180 || v > 180) {
                        errorMap.put("Error : WRONG NUMBER VALUE at " + n.getLCPrefix(),
                                ERROR_KIND.WRONG_NUMBER_VALUE);
                    }
                }
            }
        }
        return null;
    }

    @Override
    public Object visit(ASTChaine n) {
        if (n != null) {
            if (n.value.matches("^\\W*$")) {
                errorMap.put("Error : EMPTY STATION NAME at : " + n.getLCPrefix(), ERROR_KIND.EMPTY_STATION_NAME);
            } else {
                if (nomStations.contains(n.value.trim())) {
                    errorMap.put("Error : DUPLICATE STATION NAME at : " + n.getLCPrefix(), ERROR_KIND.DUPLICATE_STATION_NAME);
                } else {
                    nomStations.add(n.value.trim());
                }
            }
        }
        return null;
    }

    @Override
    public Object visit(ASTIdentificateur n) {
        if (n != null) {
            String d = n.value.trim();
            if (declarations.contains(d)) {
                errorMap.put("Error : DUPLICATE DECLARATION at " + n.getLCPrefix(), ERROR_KIND.DUPLICATE_DECLARATION);
            } else {
                declarations.add(d);
            }
        }
        return null;
    }

    @Override
    public Object visit(ASTNombre n) { return null; }
}

enum ERROR_KIND {
    EMPTY_LIST,
    EMPTY_STATION_NAME,
    DUPLICATE_STATION_NAME,
    MISSING_DECLARATION,
    DUPLICATE_DECLARATION,
    WRONG_NUMBER_VALUE
}