package fr.ufc.l3info.oprog;

import fr.ufc.l3info.oprog.parser.*;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class Ville implements Iterable<Station> {

    private Set<Station> stationSet;
    private final Set<Abonne> abonneSet;
    private Station stationPrincipale;
    private final JRegistre registre;
    final StationParser parser = StationParser.getInstance();


    public Ville(){
        stationSet = new HashSet<>();
        abonneSet = new HashSet<>();
        registre = new JRegistre();
    }

    public void initialiser(File f) throws IOException {
        stationSet.clear();
        if(f == null){
            return;
        }
        ASTNode n;
        ASTCheckerVisitor checker = new ASTCheckerVisitor();
        try {
            n = parser.parse(f);
            n.accept(checker);
            if(!checker.getErrors().isEmpty()){
                for(String error : checker.getErrors().keySet()){
                    System.out.println(error);
                }
                return;
            }
        } catch (StationParserException e) {
            System.out.println(e.getMessage());
            return;
        }
        ASTStationBuilder builder = new ASTStationBuilder();
        n.accept(builder);
        stationSet = builder.getStations();
        for(Station s : stationSet){
            if(!s.getNom().matches("^\\w"))
            s.setRegistre(registre);
        }
        String nom = n.getChild(0).getChild(0).toString();
        nom = nom.substring(1, nom.length()-1);
        stationPrincipale = getStation(nom);
    }

    public void setStationPrincipale(Station st){
        if(stationSet.contains(st)) {
            stationPrincipale = st;
        }
    }

    public Station getStation(String nom){
        for(Station s: stationSet){
            if(s.getNom().equals(nom)){
                return s;
            }
        }
        return null;
    }

    public Station getStationPlusProche(double lat, double lon){
        double distance = Double.MAX_VALUE;
        Station plusProche = null;
        Station fausseStation = new Station("", lat, lon, 0);
        for(Station s : stationSet){
            double temp_distance = s.distance(fausseStation);
            if(temp_distance < distance){
                distance = temp_distance;
                plusProche = s;
            }
        }
        return plusProche;
    }

    public Abonne creerAbonne(String nom, String RIB) {
        Abonne a;
        try {
            a = new Abonne(nom, RIB);
        } catch (IncorrectNameException e) {
            System.out.println(e.getMessage());
            a = null;
        }
        if(a != null) {
            abonneSet.add(a);
        }
        return a;
    }

    @Override
    public Iterator<Station> iterator() {
        return new ClosestStationIterator(stationSet, stationPrincipale);
    }

    public Map<Abonne, Double> facturation(int mois, int annee) {
        Map<Abonne, Double> res = new HashMap<>();

        ZoneId timeZone = ZoneOffset.UTC;
        LocalDate moisDate = LocalDate.of(annee, mois, 1);
        long debutMois = moisDate.atStartOfDay(timeZone)
                .toInstant()
                .toEpochMilli();

        long finMois =  moisDate.plus(1, ChronoUnit.MONTHS)
                .atStartOfDay(timeZone)
                .toInstant()
                .toEpochMilli() - 1;

        for(Abonne a : abonneSet){
            res.put(a, registre.facturation(a, debutMois, finMois));
        }

        return res;
    }
}
