package fr.ufc.l3info.oprog;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

public class Station {

    private final String nom;
    private final double latitude;
    private final double longitude;
    private final int capacite;
    private final ArrayList<IVelo> bornes;
    private IRegistre registre;
    private int nbBorneLibre;

    public Station(String nom, double latitude, double longitude, int capacite) {
        this.nom = nom;
        this.latitude = latitude;
        this.longitude = longitude;
        this.capacite = capacite;
        if (capacite < 0) {
            this.bornes = new ArrayList<>(0);
            this.nbBorneLibre = 0;
        } else {
            this.nbBorneLibre = capacite;
            this.bornes = new ArrayList<>(capacite);
            for (int i = 0; i < capacite; i++) {
                bornes.add(null);
            }
        }

    }

    public void setRegistre(IRegistre registre) {
        this.registre = registre;
    }

    public String getNom() {
        return this.nom;
    }

    public int capacite() {
        return this.capacite;
    }

    public int nbBornesLibres() {
        return nbBorneLibre;
    }

    public IVelo veloALaBorne(int b) {
        b--;
        if (b < 0 || b >= this.capacite || (this.bornes.isEmpty() || this.bornes.get(b) == null)) {
            return null;
        }
        return this.bornes.get(b);
    }

    public IVelo emprunterVelo(Abonne a, int b) {
        b--;
        if (registre == null || a == null || registre.nbEmpruntsEnCours(a) > 0 || b >= capacite || b < 0
                || bornes.get(b) == null || a.estBloque() || registre.emprunter(a, bornes.get(b), maintenant()) != 0
                || bornes.get(b).decrocher() == -1) {
            return null;
        }

        IVelo v = bornes.get(b);
        if (v.estAbime()) {
            return null;
        }

        bornes.set(b, null);
        nbBorneLibre++;
        return v;
    }

    public int arrimerVelo(IVelo v, int b) {
        b--;
        if (v == null || b < 0 || b >= capacite) {
            return -1;
        }
        if (bornes.get(b) != null || registre == null) {
            return -2;
        }
        if (v.arrimer() == -1) {
            return -3;
        }
        bornes.set(b, v);
        nbBorneLibre--;

        if (v.estAbime()) {
            Abonne emprunteur = registre.emprunteur(v);
            if (emprunteur != null) {
                emprunteur.bloquer();
            }
        }

        if (registre.retourner(v, maintenant()) != 0) {
            return -4;
        }

        return 0;
    }

    public void equilibrer(Set<IVelo> velos) {
        if (velos == null) {
            return;
        }
        ArrayList<IVelo> toAdd = new ArrayList<>();
        int nbDispo = 0;
        for (IVelo v : velos) {
            if (v != null) {
                if (!v.estAbime() && v.prochaineRevision() > 0) {
                    nbDispo++;
                }
            }
        }
        int nbUsed = 0;
        for (int i = 0; i < capacite; i++) {
            if (bornes.get(i) != null) {
                if (bornes.get(i).estAbime()) {
                    bornes.get(i).decrocher();
                    toAdd.add(bornes.get(i));
                    bornes.set(i, null);
                    nbBorneLibre++;
                } else if (bornes.get(i).prochaineRevision() <= 0) {
                    nbUsed++;
                }
            }
        }
        int velosToRemove = capacite / 2 - nbBorneLibre; // > 0 s'il faut enlever des vélos
        Iterator<IVelo> it = velos.iterator();
        IVelo v = null;
        if (it.hasNext()) {
            v = it.next();
        }

        for (int i = 0; i < capacite; i++) {
            while ((v == null || v.estAbime() || v.prochaineRevision() <= 0) && it.hasNext()) {
                v = it.next();
            }
            if (bornes.get(i) == null) {
                if (v != null && velosToRemove < 0 && nbDispo > nbUsed
                        || (nbUsed == capacite - nbBorneLibre && nbDispo < nbUsed && nbDispo + nbUsed <= (capacite + 1) / 2)) {
                    bornes.set(i, v);
                    bornes.get(i).arrimer();
                    nbDispo--;
                    nbBorneLibre--;
                    velosToRemove++;
                    it.remove();
                    if (!it.hasNext()) {
                        break;
                    }
                    v = it.next();
                }
            } else {
                if (bornes.get(i).prochaineRevision() <= 0) {
                    nbUsed--;
                    if (nbUsed < nbDispo && -velosToRemove < nbDispo) {
                        bornes.get(i).decrocher();
                        toAdd.add(bornes.get(i));
                        if (velosToRemove <= 0 && v != null) {
                            nbDispo--;
                            bornes.set(i, v);
                            bornes.get(i).arrimer();
                            it.remove();
                        } else {
                            bornes.set(i, null);
                            nbBorneLibre++;
                            velosToRemove--;
                            continue;
                        }

                        if (it.hasNext()) {
                            v = it.next();
                        }
                        continue;
                    }
                }
                if (nbUsed < velosToRemove) {
                    bornes.get(i).decrocher();
                    velos.add(bornes.get(i));
                    bornes.set(i, null);
                    velosToRemove--;
                    nbBorneLibre++;
                }
            }
        }
        velos.addAll(toAdd);
    }

    public double distance(Station s) {
        if (s == null) {
            return 0;
        }
        double lat1 = latitude * Math.PI / 180;
        double lat2 = s.latitude * Math.PI / 180;
        double long1 = longitude * Math.PI / 180;
        double long2 = s.longitude * Math.PI / 180;
        double a =
                Math.pow(Math.sin((lat2 - lat1) / 2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin((long2 - long1) / 2), 2);
        return 6371 * 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    }

    public long maintenant() {
        return System.currentTimeMillis();
    }

}
