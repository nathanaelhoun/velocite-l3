package fr.ufc.l3info.oprog;

public abstract class  Option implements IVelo{

    private IVelo velo;
    private String nomOption;
    double tarifOption;

    public Option(IVelo v, String nomOption, double tarifOption){
        this.velo = v;
        this.nomOption = nomOption;
        this.tarifOption = tarifOption;
    }

    public double kilometrage() {
        return velo.kilometrage();
    }

    public double prochaineRevision() {
        return velo.prochaineRevision();
    }


    public void parcourir(double km) {
        velo.parcourir(km);
    }

    @Override
    public double tarif() {
        return tarifOption + this.velo.tarif();
    }

    public int decrocher() {
        return velo.decrocher();
    }

    public int arrimer() {
        return velo.arrimer();
    }


    public void abimer() {
        velo.abimer();
    }

    public boolean estAbime() {
        return velo.estAbime();
    }

    public int reviser() {
        return velo.reviser();
    }

    public int reparer() {
        return velo.reparer();
    }

    public String toString() {
        String[] splittedString = velo.toString().split(" -");
        return splittedString[0] + ", " + nomOption + " -" + splittedString[1];
    }



}

class OptCadreAlu extends Option {

    public OptCadreAlu(IVelo v){
        super(v, "cadre aluminium", 0.2);
    }
}

class OptFreinsDisque extends Option {

    public OptFreinsDisque(IVelo v){
        super(v, "freins à disque", 0.3);
    }
}

class OptSuspensionAvant extends Option {

    public OptSuspensionAvant(IVelo v){
        super(v, "suspension avant", 0.5);
    }
}

class OptSuspensionArriere extends Option {

    public OptSuspensionArriere(IVelo v){
        super(v, "suspension arrière", 0.5);
    }
}

class OptAssistanceElectrique extends Option {

    public OptAssistanceElectrique(IVelo v){
        super(v, "assistance électrique", 2);
    }
}