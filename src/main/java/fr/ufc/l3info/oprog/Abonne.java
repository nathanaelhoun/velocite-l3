package fr.ufc.l3info.oprog;

/**
 * Classe représentant un abonné au service VéloCité.
 */
public class Abonne {
    static private int idCount = 0;
    private final int id;
    private final String nom;
    private String rib;
    private boolean isBlocked;

    /**
     * Créé un abonné dont le nom est passé en paramètre, sans informations bancaires.
     * Si le nom de l'abonné n'est pas correct (vide ou ne contenant pas des lettres éventuellement séparées par des espaces ou des traits d'union), le constructeur déclenchera l'exception IncorrectNameException.
     * On notera que le nom peut contenir des espaces inutiles en début et en fin, mais ceux-ci seront retirés pour enregistrer cette donnée.
     *
     * @param nom le nom du nouvel abonné.
     * @throws IncorrectNameException si le nom de l'abonné n'est pas correct.
     */
    public Abonne(String nom) throws IncorrectNameException {
        if (nom != null) {
            nom = nom.trim();
            if (nom.matches("^([\\p{L}]+[-\\s]?)+$")) {
                this.nom = nom;
                this.id = ++idCount;
                this.isBlocked = true;
                return;
            }
        }
        throw new IncorrectNameException();
    }

    /**
     * Créé un abonné dont le nom est passé en paramètre, avec les informations bancaires spécifiées dans le second paramètre.
     * Le comportement attendu est le même que celui du constructeur précédent. Le RIB n'est enregistré que si celui-ci est valide.
     *
     * @param nom le nom du nouvel abonné.
     * @param rib le RIB
     * @throws IncorrectNameException si le nom de l'abonné n'est pas correct.
     */
    public Abonne(String nom, String rib) throws IncorrectNameException {
        this(nom);
        this.miseAJourRIB(rib);
    }

    /**
     * Vérifie que le rib donné en paramètre est bien de la forme 11111-22222-33333333333-44
     * et qu'il est également valide
     *
     * @param rib le rib de l'abonné
     * @return true si le rib est valide, false s'il n'est pas sous la bonne forme ou non valide
     */
    private boolean validationRib(String rib) {
        if (rib != null) {
            if (rib.matches("^([0-9]{5}-){2}[0-9]{11}-[0-9]{2}$")) {
                long key;
                key = 97 - ((89 * Integer.parseInt(rib.substring(0, 5)) + 15 * Integer.parseInt(rib.substring(6, 11)) + 3 * Long.parseLong(rib.substring(12, 23))) % 97);
                return key == Integer.parseInt(rib.substring(24));
            }
        }
        return false;
    }

    /**
     * Renvoie l'identifiant de l'abonné, généré automatiquement à sa création.
     *
     * @return l'identifiant de l'abonné.
     */
    public int getID() {
        return this.id;
    }

    /**
     * Renvoie le nom de l'abonné.
     *
     * @return le nom de l'abonné, sans les éventuels espace en début et en fin de chaîne.
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Met à jour l'ancien RIB pour un nouveau. Si le nouveau RIB n'est pas valide, l'abonné conserve ses anciennes coordonnées bancaires.
     *
     * @param rib nouveau RIB pour la mise à jour.
     */
    public void miseAJourRIB(String rib) {
        if (validationRib(rib)) {
            // Débloque le compte seulement si il était bloqué car il n'avait pas de rib
            if (this.isBlocked && this.rib == null) {
                this.isBlocked = false;
            }
            this.rib = rib;
        }
    }

    /**
     * Permet de bloquer volontairement un abonné.
     */
    public void bloquer() {
        this.isBlocked = true;
    }

    /**
     * Permet de débloquer un abonné.
     */
    public void debloquer() {
        if (this.rib != null) {
            this.isBlocked = false;
        }
    }

    /**
     * Vérifie si un abonné est bloqué. Celui-ci peut être bloqué volontairement ou parce que ses coordonnées bancaires sont invalides.
     *
     * @return true si l'abonné est considéré comme bloqué, false sinon.
     */
    public boolean estBloque() {
        return this.isBlocked;
    }

    /**
     * permet de tester si deux abonnés sont identiques. Pour cela, on vérifiera si leur identifiant est le même.
     *
     * @param a l'abonné avec lequel est comparé l'instance courante.
     * @return true si les deux objets ont le même ID, false sinon.
     */
    public boolean equals(Object a) {
        if (a instanceof Abonne) {
            return this.id == ((Abonne) a).id;
        }
        return false;
    }

    /**
     * Utilisée en interne par Java pour obtenir un hash de l'objet. Cette méthode est utilisée pour les structures de collection de type HashSet ou HashMap.
     *
     * @return le hash de l'instance courante.
     */
    public int hashCode() {
        return this.id;
    }

}

class IncorrectNameException extends Exception {
    public IncorrectNameException() {
        super("Le nom fourni n'est pas correct.");
    }
}

