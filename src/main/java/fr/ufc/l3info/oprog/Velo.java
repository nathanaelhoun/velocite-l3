package fr.ufc.l3info.oprog;

public class Velo implements IVelo{

    private String cadre;
    private boolean estArrime;
    private boolean estAbime;
    private double kilometrage;
    private double prochaineRevision;
    private double tarif;

    public Velo() {
        this.cadre = "mixte";
        this.estAbime = false;
        this.estArrime = false;
        this.kilometrage = 0;
        this.prochaineRevision = 500;
        this.tarif = 2.0;
    }



    public Velo(char t){
        this();
        if(t == 'f' || t == 'F'){
            this.cadre = "femme";
            return;
        }
        if(t == 'h' || t == 'H'){
            this.cadre = "homme";
        }
    }

    /**
     * Indique le nombre de kilomètres total qu'a déjà parcouru le vélo.
     *
     * @return le kilométrage du vélo.
     */
    @Override
    public double kilometrage() {
        return this.kilometrage;
    }



    /**
     * Indique le nombre de kilomètres qu'il reste au vélo avant la prochaine révision.
     * Une valeur négative ou nulle indiquera qu'il est temps d'effectuer la révision.
     *
     * @return le nombre de kilomètres avant la prochaine révision.
     */
    @Override
    public double prochaineRevision() {
        return this.prochaineRevision;
    }

    /**
     * Fait parcourir au vélo en cours d'emprunt le nombre de kilomètres passé en paramètre.
     * Si le vélo n'est pas emprunté, cette méthode est sans effet.
     *
     * @param km le nombre de kilomètres parcourus.
     */
    @Override
    public void parcourir(double km) {
        if(km <= 0 || this.estArrime){
            return;
        }
        this.kilometrage += km;
        this.prochaineRevision -= km;
    }

    /**
     * Tarif horaire pour le vélo.
     *
     * @return Le tarif de location pour une heure de vélo (positif ou nul)
     */
    @Override
    public double tarif() {
        return this.tarif;
    }

    /**
     * Permet de décrocher le vélo de sa borne.
     *
     * @return 0 si le vélo a effectivement pu être décroché,
     * -1 si le vélo est déjà décroché.
     */
    @Override
    public int decrocher() {
        if(this.estArrime) {
            this.estArrime = false;
            return 0;
        }
        return -1;
    }

    /**
     * Permet de raccrocher le vélo à une borne.
     *
     * @return 0 si le vélo a effectivement pu être accroché,
     * -1 si le vélo est déjà accroché.
     */
    @Override
    public int arrimer() {
        if(!this.estArrime) {
            this.estArrime = true;
            return 0;
        }
        return -1;
    }

    /**
     * Abime le vélo, quelque soit son état d'origine.
     */
    @Override
    public void abimer() {
        this.estAbime = true;
    }

    /**
     * Indique si le vélo est en bon état ou abimé.
     *
     * @return true si le vélo est abimé, false sinon.
     */
    @Override
    public boolean estAbime() {
        return this.estAbime;
    }

    /**
     * Permet de réviser le vélo lorsque celui-ci est décroché. Cette action a pour effet
     * de réinitialiser le décompte des kilomètres à parcourir avant la prochaine révision.
     * Si le vélo était abimé, la révision a pour effet de le réparer.
     *
     * @return 0 si la révision a pu être effectuée,
     * -1 sinon (le vélo est encore accroché).
     */
    @Override
    public int reviser() {
        if(this.estArrime){
            return -1;
        }
        if(this.estAbime){
            this.reparer();
        }
        this.prochaineRevision = 500;
        return 0;
    }

    /**
     * Permet de réparer un vélo. La réparation s'effectue sur un vélo abimé qui n'est pas accroché.
     *
     * @return 0 si le vélo a pu être réparé,
     * -1 si le vélo est accroché,
     * -2 si le vélo est décroché, mais qu'il n'est pas abimé.
     */
    @Override
    public int reparer() {
        if(this.estArrime){
            return -1;
        }
        if(!this.estAbime){
            return -2;
        }
        this.estAbime = false;
        return 0;
    }

    @Override
    public String toString() {
        String str = "Vélo cadre " + this.cadre + " - " + Math.round(this.kilometrage * 10)/10.0 + " km";
        if(this.prochaineRevision <= 0) {
            str += " (révision nécessaire)";
        }
        return str;
    }
}

