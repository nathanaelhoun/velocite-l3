package fr.ufc.l3info.oprog;

import java.util.*;

/**
 * Classe implantant un registre simple en Java.
 */
public class JRegistre implements IRegistre {

    private final Map<Abonne, Set<Emprunt>> empruntsParAbonne;
    private final Map<IVelo, Set<Emprunt>> empruntsParVelo;
    private final Map<IVelo, Abonne> empruntsEnCours;

    public JRegistre() {
        empruntsParAbonne = new HashMap<>();
        empruntsParVelo = new HashMap<>();
        empruntsEnCours = new HashMap<>();
    }

    @Override
    public int emprunter(Abonne a, IVelo v, long d) {
        if (a == null || v == null) {
            return -1;
        }
        if (empruntsEnCours.containsKey(v)) {
            return -2;
        }
        if (empruntsParVelo.containsKey(v)) {
            for (Emprunt em : empruntsParVelo.get(v)) {
                if (em.contientDate(d)) {
                    return -2;
                }
            }
        }
        if (!empruntsParAbonne.containsKey(a)) {
            empruntsParAbonne.put(a, new HashSet<>());
        }
        if (!empruntsParVelo.containsKey(v)) {
            empruntsParVelo.put(v, new HashSet<>());

        }
        Emprunt em = new Emprunt(v, d);
        empruntsParAbonne.get(a).add(em);
        empruntsParVelo.get(v).add(em);
        empruntsEnCours.put(v, a);
        return 0;
    }

    @Override
    public int retourner(IVelo v, long d) {
        if (v == null) {
            return -1;
        }
        if (!empruntsEnCours.containsKey(v)) {
            return -2;
        }

        if (empruntsParVelo.containsKey(v)) {
            for (Emprunt em : empruntsParVelo.get(v)) {
                if (!em.estEnCours() && em.contientDate(d)) {
                    return -3;
                }
            }
        }

        Abonne a = empruntsEnCours.get(v);
        for (Emprunt em : empruntsParAbonne.get(a)) {
            if (em.estEnCours() && em.concerne(v)) {
                if (!em.termine(d)) {
                    return -3;
                }
                break;
            }
        }

        empruntsEnCours.remove(v);
        return 0;
    }

    @Override
    public int nbEmpruntsEnCours(Abonne a) {
        int nb = 0;
        if (empruntsParAbonne.containsKey(a)) {
            for (Emprunt e : empruntsParAbonne.get(a)) {
                if (e.estEnCours()) {
                    nb++;
                }
            }
        }
        return nb;
    }

    @Override
    public double facturation(Abonne a, long debut, long fin) {
        double facture = 0.0;
        if (empruntsParAbonne.get(a) != null) {
            int nbEmprunts = 0;

            ArrayList<Emprunt> empruntsTriesParDate = new ArrayList<>(empruntsParAbonne.get(a));
            empruntsTriesParDate.sort(
                    Comparator.comparingInt(obj -> (int) obj.debut)
            );


            for (Emprunt e : empruntsTriesParDate) {
                if (e.finitEntre(debut, fin)) {
                    double multiplicateur = (1.0 - Math.floor(nbEmprunts / 10.0) / 10);
                    if (multiplicateur < 0.5) {
                        multiplicateur = 0.5;
                    }

                    double cout = e.cout();
                    facture += multiplicateur * cout;

                    if (e.duree() >= 60 * 5) {
                        nbEmprunts++;
                    }
                }
            }
        }
        return facture;
    }

    @Override
    public Abonne emprunteur(IVelo velo) {
        return this.empruntsEnCours.get(velo);
    }

    private static class Emprunt {
        private final IVelo velo;
        private final long debut;
        private long fin;
        private boolean enCours;
//        private final Abonne abo;

        public Emprunt(IVelo v, long d) {
//            abo = a;
            velo = v;
            debut = d;
            fin = d - 1;
            enCours = true;
        }

        public boolean concerne(IVelo v) {
            return v == velo;
        }

        public boolean estEnCours() {
            return enCours;
        }

        public boolean termine(long f) {
            if (enCours && f >= debut) {
                fin = f;
                enCours = false;
                return true;
            }
            return false;
        }

        public boolean contientDate(long d) {
            return d >= debut && (enCours || d <= fin);
        }

        public boolean finitEntre(long d, long f) {
            return fin >= d && fin <= f;
        }

        /**
         * @return la durée de l'emprunt en secondes
         */
        public long duree() {
            if (estEnCours()) {
                return 0;
            }

            return (fin - debut) / 1000;
        }

        public double cout() {
            if (enCours)
                return 0;

            long delta = Math.abs(fin - debut);
            long nbMin = delta / (60 * 1000);
            return (velo.tarif() * nbMin) / 60;
        }
    }
}

