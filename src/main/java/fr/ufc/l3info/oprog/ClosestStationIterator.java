package fr.ufc.l3info.oprog;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ClosestStationIterator implements Iterator<Station> {

    private final Iterator<Station> it;

    ClosestStationIterator(Set<Station> stations, Station s) {
        // Trier les stations par ordre de proximité avec s
        List<Station> stationsProches = new ArrayList<>();

        if (stations != null && s != null && stations.contains(s)) {
            stationsProches.addAll(stations);
            stationsProches.sort(
                    (obj1, obj2) -> (int) Math.floor(obj1.distance(s) - obj2.distance(s))
            );
        }

        this.it = stationsProches.iterator();
    }

    @Override
    public boolean hasNext() {
        return this.it.hasNext();
    }

    @Override
    public Station next() {
        return this.it.next();
    }


    /**
     * Ne fait rien
     */
    @Override
    public void remove() {
    }
}
