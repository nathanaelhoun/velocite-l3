package fr.ufc.l3info.oprog;

import java.util.Arrays;

public class FabriqueVelo {
    private static final FabriqueVelo INST = new FabriqueVelo();

    public FabriqueVelo() {}

    public static FabriqueVelo getInstance(){
        return INST;
    }

    public IVelo construire(char t, String... options){
        IVelo velo = new Velo(t);
        if(options == null){
            return velo;
        }
        for (String o: options) {
            if(o == null){
                continue;
            }
            switch (o) {
                case "CADRE_ALUMINIUM":
                    if (!velo.toString().contains("cadre aluminium")) {
                        velo = new OptCadreAlu(velo);
                    }
                    break;
                case "SUSPENSION_AVANT":
                    if (!velo.toString().contains("suspension avant")) {
                        velo = new OptSuspensionAvant(velo);
                    }
                    break;
                case "SUSPENSION_ARRIERE":
                    if (!velo.toString().contains("suspension arrière")) {
                        velo = new OptSuspensionArriere(velo);
                    }
                    break;
                case "FREINS_DISQUE":
                    if (!velo.toString().contains("freins à disque")) {
                        velo = new OptFreinsDisque(velo);
                    }
                    break;
                case "ASSISTANCE_ELECTRIQUE":
                    if (!velo.toString().contains("assistance électrique")) {
                        velo = new OptAssistanceElectrique(velo);
                    }
                    break;
            }
        }
        return velo;
    }
}
