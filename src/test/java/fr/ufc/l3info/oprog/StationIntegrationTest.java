package fr.ufc.l3info.oprog;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

@RunWith(MockitoJUnitRunner.class)
public class StationIntegrationTest {
    static final double DELTA_TRES_LONGUE_DISTANCE = 20;
    static final double LAT_VIOTTE = 6.022715427111734;
    static final double LONG_VIOTTE = 47.24650155142733;
    static final int CAPACITE_VIOTTE = 20;
    final String RIB_CORRECT = "11111-11111-11111111111-48";
    private final char[] cadres = {'m', 'h', 'f'};
    private final String[] options = {"CADRE_ALUMINIUM", "FREINS_DISQUE", "SUSPENSION_AVANT", "SUSPENSION_ARRIERE",
            "ASSISTANCE_ELECTRIQUE"};
    private final FabriqueVelo fabrique = FabriqueVelo.getInstance();
    private final String VALID_RIB = "14568-56487-12345678912-38";
    int NB_INITIAL_BORNES_VIDES_VIOTTE;
    Station gareViotte, gareAEquilibrer;
    IRegistre registreEmpruntsRetours;
    Abonne abonneNonBloque, abonneNonBloque2;
    IVelo veloGareViotte1, veloGareViotte3;
    private int NB_BORNE;
    private Station s;
    private IVelo velo;
    private Abonne abonne;
    private Abonne abonne2;
    private Abonne abonneMauvaisRib;
    private IRegistre registre;

    @Before
    public void beforefunc() throws IncorrectNameException {
        NB_BORNE = 35;
        s = new Station("Hauts de Chazal", 47.221563, 5.951402, NB_BORNE);
        abonne = new Abonne("Fab", VALID_RIB);
        abonne2 = new Abonne("Fab", VALID_RIB);
        String INVALID_RIB = "11111-22222-33333333333-44";
        abonneMauvaisRib = new Abonne("Fab", INVALID_RIB);
        velo = new Velo();
        registre = new JRegistre();
        s.setRegistre(registre);

        this.abonneNonBloque = new Abonne("Fred");
        this.abonneNonBloque.miseAJourRIB(RIB_CORRECT);
        assert (!this.abonneNonBloque.estBloque());

        this.abonneNonBloque2 = new Abonne("VéloTaffeurDeLXtreme");
        this.abonneNonBloque2.miseAJourRIB(RIB_CORRECT);
        assert (!this.abonneNonBloque2.estBloque());

        this.velo = new Velo();

        this.veloGareViotte1 = new Velo();
        this.veloGareViotte3 = FabriqueVelo.getInstance().construire('h', "CADRE_ALUMINIUM", "SUSPENSION_ARRIERE", "SUSPENSION_AVANT", "ASSISTANCE_ELECTRIQUE");

        this.registreEmpruntsRetours = new JRegistre();

        this.gareViotte = new Station("Viotte", LAT_VIOTTE, LONG_VIOTTE, CAPACITE_VIOTTE);
        this.gareViotte.setRegistre(this.registreEmpruntsRetours);
        this.gareViotte.arrimerVelo(veloGareViotte1, 1);
        this.gareViotte.arrimerVelo(veloGareViotte3, 3);

        this.NB_INITIAL_BORNES_VIDES_VIOTTE = gareViotte.nbBornesLibres();

        this.gareAEquilibrer = new Station("Gare a équilibrer", LAT_VIOTTE, LONG_VIOTTE, 20);
        this.gareAEquilibrer.setRegistre(new JRegistre());
        for (int i = 1; i < 5; i++) {
            this.gareAEquilibrer.arrimerVelo(creerVeloBonEtat(), i);
        }

        for (int i = 6; i < 10; i++) {
            this.gareAEquilibrer.arrimerVelo(creerVeloAbime(), i);
        }

        for (int i = 12; i < 16; i++) {
            IVelo veloAReviser = new Velo();
            veloAReviser.parcourir(500 + (i - 12) * 100);

            assert (!veloAReviser.estAbime());
            assert (veloAReviser.prochaineRevision() <= 0);
            this.gareAEquilibrer.arrimerVelo(veloAReviser, i);
        }
    }


    private IVelo creerVeloBonEtat() {
        IVelo veloEnBonEtat = new Velo();
        veloEnBonEtat.parcourir(Math.random() * 300.0);

        assert (!veloEnBonEtat.estAbime());
        assert (veloEnBonEtat.prochaineRevision() > 0);
        return veloEnBonEtat;
    }

    private IVelo creerVeloAbime() {
        IVelo veloAbime = new Velo();
        veloAbime.abimer();
        // Certains doivent être révisés, d'autres non
        veloAbime.parcourir(Math.random() * 1000.0);

        assert (veloAbime.estAbime());
        return veloAbime;
    }

    private IVelo creerVeloAReviser() {
        IVelo veloAReviser = new Velo();
        // Doit être révisé maintenant ou depuis longtemps
        double rand = Math.random();
        if (rand < 0.5) {
            veloAReviser.parcourir(500);
        } else {
            veloAReviser.parcourir(500 + rand * 100);
        }

        assert (!veloAReviser.estAbime());
        assert (veloAReviser.prochaineRevision() <= 0);
        return veloAReviser;
    }

    private int verificationVelosStationNonAbimesNonAReviser(Station s) {
        int nombreDeNull = 0;
        for (int i = 1; i < s.capacite() + 1; ++i) {
            IVelo veloBorneI = s.veloALaBorne(i);
            if (veloBorneI == null) {
                ++nombreDeNull;
                continue;
            }

            Assert.assertFalse(veloBorneI.estAbime());
            Assert.assertFalse(veloBorneI.prochaineRevision() <= 0);
        }

        return nombreDeNull;
    }

    @After
    public void cleanUp() {
        s = null;
        abonne = null;
        abonne2 = null;
        abonneMauvaisRib = null;
        velo = null;
        registre = null;
    }

    @Test
    public void constructeurTest() {
        Station st = new Station("Hauts de Chazal", 47.221563, 5.951402, 35);
        Assert.assertEquals(NB_BORNE, st.capacite());
        Assert.assertEquals("Hauts de Chazal", st.getNom());
    }

    @Test
    public void constructeurNull() {
        Station st = new Station(null, 47.221563, 5.951402, 35);
        Assert.assertEquals(NB_BORNE, st.capacite());
        Assert.assertNull(st.getNom());
    }

    @Test
    public void constructeurCapaciteVide() {
        Station st = new Station("Hauts de Chazal", 47.221563, 5.951402, 0);
        Assert.assertEquals(0, st.capacite());
        Assert.assertEquals("Hauts de Chazal", st.getNom());
    }

    @Test
    public void constructeurCapaciteNegative() {
        Station st = new Station("Hauts de Chazal", 47.221563, 5.951402, -5);
        Assert.assertEquals(-5, st.capacite());
        Assert.assertEquals("Hauts de Chazal", st.getNom());
    }

    @Test
    public void nbBornesLibre() {
        Assert.assertEquals(NB_BORNE, s.nbBornesLibres());
        for (int i = 1; i < 10; i++) {
            int rand = (int) (Math.random() * options.length);
            int rand_cadre = (int) (Math.random() * cadres.length);
            s.arrimerVelo(fabrique.construire(cadres[rand_cadre], options[rand]), i);
        }
        Assert.assertEquals(NB_BORNE - 9, s.nbBornesLibres());
        for (int i = 0; i < 10; i++) {
            s.arrimerVelo(fabrique.construire('m'), i);
        }
        Assert.assertEquals(NB_BORNE - 9, s.nbBornesLibres());
        for (int i = 10; i < 20; i++) {
            int rand = (int) (Math.random() * options.length);
            int rand_cadre = (int) (Math.random() * cadres.length);
            s.arrimerVelo(fabrique.construire(cadres[rand_cadre], options[rand]), i);
        }
        Assert.assertEquals(NB_BORNE - 19, s.nbBornesLibres());
        for (int i = 20; i < 40; i++) {
            s.arrimerVelo(fabrique.construire('m'), i);
        }
        Assert.assertEquals(0, s.nbBornesLibres());
        for (int i = NB_BORNE; i < 1; i--) {
            s.emprunterVelo(abonne, i);
            Assert.assertEquals(NB_BORNE - i, s.nbBornesLibres());
        }
    }

    @Test
    public void nbBornesLibreNull() {
        for (int i = 0; i < 10; i++) {
            s.arrimerVelo(null, i);
        }
        Assert.assertEquals(NB_BORNE, s.nbBornesLibres());
    }

    @Test
    public void nbBornesLibreDejaArrime() {
        velo.arrimer();
        for (int i = 0; i < 10; i++) {
            s.arrimerVelo(velo, i);
        }
        Assert.assertEquals(NB_BORNE, s.nbBornesLibres());
    }

    @Test
    public void veloALaBorneTest() {
        Assert.assertNull(s.veloALaBorne(1));
        s.arrimerVelo(velo, 1);
        Assert.assertNotNull(s.veloALaBorne(1));
        Assert.assertNull(s.veloALaBorne(0));
        Assert.assertNull(s.veloALaBorne(-1));
        Assert.assertNull(s.veloALaBorne(NB_BORNE + 1));
    }

    @Test
    public void emprunterVeloTestPlusieursEmprunts() {
        Assert.assertNull(s.emprunterVelo(abonne, 1));
        for (int i = 0; i <= NB_BORNE; i++) {
            int rand = (int) (Math.random() * options.length);
            int rand_cadre = (int) (Math.random() * cadres.length);
            s.arrimerVelo(fabrique.construire(cadres[rand_cadre], options[rand]), i);
        }
        Assert.assertNull(s.emprunterVelo(abonne, 0));
        Assert.assertNotNull(s.emprunterVelo(abonne, 1));
        Assert.assertNull(s.emprunterVelo(abonne, 1));
        Assert.assertNull(s.emprunterVelo(abonne2, 1));
        Assert.assertNull(s.emprunterVelo(abonne, 2));
        Assert.assertNotNull(s.emprunterVelo(abonne2, NB_BORNE));
    }

    @Test
    public void emprunterVeloTestBornesInvalides() {
        for (int i = 0; i < NB_BORNE; i++) {
            int rand = (int) (Math.random() * options.length);
            int rand_cadre = (int) (Math.random() * cadres.length);
            s.arrimerVelo(fabrique.construire(cadres[rand_cadre], options[rand]), i);
        }
        Assert.assertNull(s.emprunterVelo(abonne, 0));
        Assert.assertNull(s.emprunterVelo(abonne, -1));
        Assert.assertNull(s.emprunterVelo(abonne, NB_BORNE + 1));
    }

    @Test
    public void emprunterVeloTestRegistreNull() {
        s.arrimerVelo(velo, 1);
        s.setRegistre(null);
        Assert.assertNull(s.emprunterVelo(abonne, 1));
    }

    @Test
    public void emprunterVeloTestAbonneNull() {
        s.arrimerVelo(velo, 1);
        Assert.assertNull(s.emprunterVelo(null, 1));
    }

    @Test
    public void emprunterVeloTestAbonneBloque() {
        s.arrimerVelo(velo, 1);
        Assert.assertNull(s.emprunterVelo(abonneMauvaisRib, 1));
        abonneMauvaisRib.debloquer();
        Assert.assertTrue(abonneMauvaisRib.estBloque());
        Assert.assertNull(s.emprunterVelo(abonneMauvaisRib, 1));
        abonne.bloquer();
        Assert.assertNull(s.emprunterVelo(abonne, 1));
        abonne.miseAJourRIB(VALID_RIB);
        Assert.assertNull(s.emprunterVelo(abonne, 1));
        abonne.debloquer();
        Assert.assertNotNull(s.emprunterVelo(abonne, 1));
    }

    @Test
    public void arrimerVeloTest() {
        Assert.assertEquals(-4, s.arrimerVelo(velo, 1));
        for (int i = 2; i <= NB_BORNE; i++) {
            int rand = (int) (Math.random() * options.length);
            int rand_cadre = (int) (Math.random() * cadres.length);
            Assert.assertEquals(-4, s.arrimerVelo(fabrique.construire(cadres[rand_cadre], options[rand]), i));
        }
        for (int i = 1; i <= NB_BORNE; i++) {
            IVelo v = s.veloALaBorne(i);
            s.emprunterVelo(abonne, i);
            s.arrimerVelo(v, i);
        }
    }

    @Test
    public void arrimerVeloTestIncorrectParam() {
        Assert.assertEquals(-1, s.arrimerVelo(null, 1));
        Assert.assertEquals(-1, s.arrimerVelo(velo, 0));
        Assert.assertEquals(-1, s.arrimerVelo(velo, -1));
        Assert.assertEquals(-1, s.arrimerVelo(velo, NB_BORNE + 1));
    }

    @Test
    public void arrimerVeloTestRegistreNull() {
        s.setRegistre(null);
        Assert.assertEquals(-2, s.arrimerVelo(velo, 1));
    }

    @Test
    public void arrimerVeloTestBorneOccupee() {
        s.arrimerVelo(velo, 1);
        Assert.assertEquals(-2, s.arrimerVelo(velo, 1));
        s.arrimerVelo(fabrique.construire('h'), 2);
        Assert.assertEquals(-2, s.arrimerVelo(velo, 2));
    }

    @Test
    public void arrimerVeloTestVeloDejaArrime() {
        s.arrimerVelo(velo, 1);
        Assert.assertEquals(-3, s.arrimerVelo(velo, 2));
        IVelo v = fabrique.construire('m');
        v.arrimer();
        Assert.assertEquals(-3, s.arrimerVelo(v, 2));
    }

    @Test
    public void arrimerVeloTestErreurRegistre() {
        Assert.assertEquals(-4, s.arrimerVelo(velo, 1));
        s.emprunterVelo(abonne, 1);
        Assert.assertEquals(0, s.arrimerVelo(velo, 1));

        // instanciation du spy
        Station spyStation = Mockito.spy(s);
        spyStation.emprunterVelo(abonne, 1);
//        Mockito.when(spyStation.maintenant()).thenReturn(System.currentTimeMillis() - 10 * 60 * 1000);
//        Assert.assertEquals(-4, spyStation.arrimerVelo(velo, 2)); // crash une fois sur deux
        IRegistre spyRegistre = Mockito.spy(registre);
        Mockito.when(spyRegistre.retourner(ArgumentMatchers.any(), ArgumentMatchers.anyLong())).thenReturn(-1);
        s.setRegistre(spyRegistre);
        Assert.assertEquals(-4, s.arrimerVelo(fabrique.construire('h'), 3));
        Mockito.when(spyRegistre.retourner(ArgumentMatchers.any(), ArgumentMatchers.anyLong())).thenReturn(-2);
        Assert.assertEquals(-4, s.arrimerVelo(fabrique.construire('f'), 4));
    }

    @Test
    public void distanceTest() {
        Station s2 = new Station("Révolution", 47.240445, 6.022909, 10);
        Assert.assertEquals(5.79, s.distance(s2), 0.01);
        Assert.assertEquals(5.79, s2.distance(s), 0.01);
        Assert.assertEquals(s.distance(s2), s2.distance(s), 0);
    }

    @Test
    public void distanceTestNull() {
        Assert.assertEquals(0, s.distance(null), 0.0);
    }

    @Test
    public void maintenantTest() {
        Assert.assertTrue(s.maintenant() <= s.maintenant());
        Assert.assertEquals(s.maintenant(), System.currentTimeMillis());
        long start = s.maintenant();
        // Do some stuff
        for (int i = 0; i < 100000; i++) {
            int a = 2;
            double b = Math.pow(a * 42.0 / 666.0, 8);
        }
        Assert.assertTrue(start < s.maintenant());
    }

    @Test
    public void equilibrerWithEmptySetTest() {
        Set<IVelo> velos = new HashSet<>();
        s.equilibrer(velos);
        Assert.assertEquals(NB_BORNE, s.nbBornesLibres());
    }

    @Test
    public void equilibrerWithNullSetTest() {
        s.equilibrer(null);
        Assert.assertEquals(NB_BORNE, s.nbBornesLibres());
    }

    @Test
    public void equilibrerWithLargeSetTest() {
        Set<IVelo> velos = new HashSet<>();
        for (int i = 0; i < NB_BORNE; i++) {
            velos.add(fabrique.construire('m'));
        }
        s.equilibrer(velos);
        Assert.assertEquals(NB_BORNE / 2, s.nbBornesLibres());
        for (int i = 1; i <= NB_BORNE; i++) {
            if (s.veloALaBorne(i) != null) {
                Assert.assertFalse(s.veloALaBorne(i).estAbime());
                Assert.assertTrue(s.veloALaBorne(i).prochaineRevision() > 0);
            }
        }
        s.equilibrer(velos);
        Assert.assertEquals(NB_BORNE / 2, s.nbBornesLibres());
        for (int i = 1; i <= NB_BORNE; i++) {
            IVelo v = fabrique.construire('m');
            if (i % 2 == 0) {
                v.abimer();
            } else {
                v.parcourir(700);
            }
            s.arrimerVelo(v, i);
        }
        Assert.assertEquals(0, s.nbBornesLibres());
        s.equilibrer(velos);
        Assert.assertEquals(NB_BORNE / 2, s.nbBornesLibres());
        for (int i = 1; i <= NB_BORNE; i++) {
            if (s.veloALaBorne(i) != null) {
                Assert.assertFalse(s.veloALaBorne(i).estAbime());
                Assert.assertTrue(s.veloALaBorne(i).prochaineRevision() > 0);
            }
        }
    }

    @Test
    public void equilibrerWithSmallSetTest() {
        Set<IVelo> velos = new HashSet<>();
        for (int i = 0; i < 2; i++) {
            velos.add(new Velo());
        }
        s.equilibrer(velos);
        Assert.assertEquals(NB_BORNE - 2, s.nbBornesLibres());
        s.equilibrer(velos);
        Assert.assertEquals(NB_BORNE - 2, s.nbBornesLibres());
        for (int i = 1; i <= NB_BORNE; i++) {
            IVelo v = fabrique.construire('m');
            v.abimer();
            s.arrimerVelo(v, i);
        }
        s.equilibrer(velos);
        Assert.assertEquals(NB_BORNE - 2, s.nbBornesLibres());
        Assert.assertTrue(s.veloALaBorne(1).prochaineRevision() > 0);
        Assert.assertTrue(s.veloALaBorne(2).prochaineRevision() > 0);
        for (int i = 1; i <= NB_BORNE; i++) {
            IVelo v = fabrique.construire('m');
            v.parcourir(700);
            s.arrimerVelo(v, i);
        }
        s.equilibrer(velos);
        Assert.assertEquals(NB_BORNE / 2, s.nbBornesLibres());
        Assert.assertTrue(s.veloALaBorne(1).prochaineRevision() > 0);
        Assert.assertTrue(s.veloALaBorne(2).prochaineRevision() > 0);
    }

    @Test
    public void testCreation() {
        Station gareGranvelle = new Station("Granvelle", 6.025353690286829, 47.23531038159631, 10);
        Assert.assertEquals("Granvelle", gareGranvelle.getNom());
        Assert.assertEquals(10, gareGranvelle.capacite());
        Assert.assertEquals(10, gareGranvelle.nbBornesLibres());
        for (int i = 0; i < 10; i++) {
            Assert.assertNull(gareGranvelle.veloALaBorne(i));
        }
    }

    @Test
    public void testCreationTailleNegative() {
        Station gareGranvelle = new Station("Granvelle", 6.025353690286829, 47.23531038159631, -5);
        Assert.assertEquals("Granvelle", gareGranvelle.getNom());
        Assert.assertEquals(-5, gareGranvelle.capacite());
        Assert.assertEquals(0, gareGranvelle.nbBornesLibres());
    }

    @Test
    public void testVeloALaBorne() {
        Assert.assertEquals(this.veloGareViotte1.toString(), this.gareViotte.veloALaBorne(1).toString());
        Assert.assertEquals(this.veloGareViotte3.toString(), this.gareViotte.veloALaBorne(3).toString());
    }

    @Test
    public void testVeloALaBorneVide() {
        Assert.assertNull(this.gareViotte.veloALaBorne(2));
        Assert.assertNull(this.gareViotte.veloALaBorne(4));
        Assert.assertNull(this.gareViotte.veloALaBorne(5));
        Assert.assertNull(this.gareViotte.veloALaBorne(6));
        Assert.assertNull(this.gareViotte.veloALaBorne(7));
        Assert.assertNull(this.gareViotte.veloALaBorne(8));
        Assert.assertNull(this.gareViotte.veloALaBorne(9));
        Assert.assertNull(this.gareViotte.veloALaBorne(10));
    }

    @Test
    public void testEmprunterVelo() {
        IVelo veloEmprunte = this.gareViotte.emprunterVelo(this.abonneNonBloque, 1);
        Assert.assertNotNull(veloEmprunte);
        Assert.assertEquals(this.veloGareViotte1.toString(), veloEmprunte.toString());

        IVelo veloEmprunte2 = this.gareViotte.emprunterVelo(this.abonneNonBloque2, 3);
        Assert.assertNotNull(veloEmprunte2);
        Assert.assertEquals(this.veloGareViotte3.toString(), veloEmprunte2.toString());

        Assert.assertNotSame(veloEmprunte, veloEmprunte2);
    }

    @Test
    public void testEmprunterVeloBorneNegatif() {
        Assert.assertNull(this.gareViotte.emprunterVelo(this.abonneNonBloque, -1));
    }

    @Test
    public void testEmprunterVeloBorneTropGrande() {
        Assert.assertNull(this.gareViotte.emprunterVelo(this.abonneNonBloque, 10000));
    }

    @Test
    public void testEmprunterVeloBorneVide() {
        Assert.assertNull(this.gareViotte.emprunterVelo(this.abonneNonBloque, 10));
    }

    @Test
    public void testEmprunterVeloRegistreNonConnecte() {
        this.gareViotte.setRegistre(null);

        Assert.assertNull(this.gareViotte.emprunterVelo(this.abonneNonBloque, 1));
    }

    @Test
    public void testEmprunterVeloEmpruntDejaEnCours() {
        IVelo veloEmprunte = this.gareViotte.emprunterVelo(this.abonneNonBloque, 1);
        Assert.assertNotNull(veloEmprunte);
        Assert.assertEquals(this.veloGareViotte1, veloEmprunte);

        IVelo veloEmprunte2 = this.gareViotte.emprunterVelo(this.abonneNonBloque, 3);
        Assert.assertNull(veloEmprunte2);
    }

    @Test
    public void testEmprunterVeloAbonneBloque() throws IncorrectNameException {
        Abonne abonneBloque = new Abonne("Déliquant");
        assert (abonneBloque.estBloque());

        Assert.assertNull(this.gareViotte.emprunterVelo(abonneBloque, 1));
        Assert.assertNull(this.gareViotte.emprunterVelo(abonneBloque, 2));
        Assert.assertNull(this.gareViotte.emprunterVelo(abonneBloque, 3));
        Assert.assertNull(this.gareViotte.emprunterVelo(abonneBloque, 4));
        Assert.assertNull(this.gareViotte.emprunterVelo(abonneBloque, 5));
    }

    @Test
    public void testArrimerVelo() {
        IVelo veloEmprunte = this.gareViotte.emprunterVelo(this.abonneNonBloque, 1);
        Assert.assertNotNull(veloEmprunte);

        Assert.assertEquals(0, this.gareViotte.arrimerVelo(veloEmprunte, 2));
    }


    @Test
    public void testArrimerVeloBorneNegatif() {
        IVelo veloEmprunte = this.gareViotte.emprunterVelo(this.abonneNonBloque, 1);
        Assert.assertNotNull(veloEmprunte);

        Assert.assertEquals(-1, this.gareViotte.arrimerVelo(veloEmprunte, -1));
    }

    @Test
    public void testArrimerVeloBorneTropGrande() {
        IVelo veloEmprunte = this.gareViotte.emprunterVelo(this.abonneNonBloque, 1);
        Assert.assertNotNull(veloEmprunte);

        Assert.assertEquals(-1, this.gareViotte.arrimerVelo(veloEmprunte, 10000));
    }

    @Test
    public void testArrimerVeloVeloNull() {
        Assert.assertEquals(-1, this.gareViotte.arrimerVelo(null, 2));
    }

    @Test
    public void testArrimerVeloBorneOccupee() {
        IVelo veloEmprunte = this.gareViotte.emprunterVelo(this.abonneNonBloque, 1);
        Assert.assertNotNull(veloEmprunte);

        Assert.assertEquals(-2, this.gareViotte.arrimerVelo(veloEmprunte, 3));
    }

    @Test
    public void testArrimerVeloRegistreNonConnecte() {
        IVelo veloEmprunte = this.gareViotte.emprunterVelo(this.abonneNonBloque, 1);
        Assert.assertNotNull(veloEmprunte);

        this.gareViotte.setRegistre(null);
        Assert.assertEquals(-2, this.gareViotte.arrimerVelo(veloEmprunte, 2));
    }

    @Test
    public void testArrimerVeloPasDempruntEnCours() {
        Assert.assertEquals(-4, this.gareViotte.arrimerVelo(this.velo, 2));
    }

    @Test
    public void testArrimerVeloDateRetourIncorrecte() {
        IVelo veloEmprunte = this.gareViotte.emprunterVelo(this.abonneNonBloque, 1);
        Assert.assertNotNull(veloEmprunte);

        Station gareEspionnee = Mockito.spy(this.gareViotte);
        Mockito.when(gareEspionnee.maintenant()).thenReturn(gareViotte.maintenant() - 10000);

        Assert.assertEquals(-4, gareEspionnee.arrimerVelo(veloEmprunte, 2));
    }


    @Test
    public void testArrimerVeloNonArrimable() {
        IVelo veloEmprunte = this.gareViotte.emprunterVelo(this.abonneNonBloque, 1);
        Assert.assertNotNull(veloEmprunte);

        veloEmprunte.arrimer();

        Assert.assertEquals(-3, this.gareViotte.arrimerVelo(veloEmprunte, 2));
    }


    @Test
    public void testEquilibrerSetVide() {
        HashSet<IVelo> reserve = new HashSet<>();

        this.gareViotte.equilibrer(reserve);

        Assert.assertEquals(NB_INITIAL_BORNES_VIDES_VIOTTE, this.gareViotte.nbBornesLibres());
    }

    @Test
    public void testEquilibrerQueVelosParfaitsEnReserve() {
        HashSet<IVelo> reserve = new HashSet<>();
        for (int i = 0; i < this.gareAEquilibrer.capacite() + 1; i++) {
            reserve.add(creerVeloBonEtat());
        }

        this.gareAEquilibrer.equilibrer(reserve);

        Assert.assertEquals(this.gareAEquilibrer.capacite() / 2, this.gareAEquilibrer.nbBornesLibres());

        int nombreDeNull = verificationVelosStationNonAbimesNonAReviser(this.gareAEquilibrer);
        Assert.assertEquals(this.gareAEquilibrer.nbBornesLibres(), nombreDeNull);
    }

    @Test
    public void testEquilibrerQueVelosAbimesEnReserve() {
        HashSet<IVelo> reserve = new HashSet<>();
        for (int i = 0; i < this.gareAEquilibrer.capacite() + 1; i++) {
            IVelo veloAbimeAvecOption = creerVeloAbime();
            veloAbimeAvecOption = new OptAssistanceElectrique(veloAbimeAvecOption);
            reserve.add(veloAbimeAvecOption);
        }

        this.gareAEquilibrer.equilibrer(reserve);

        Assert.assertEquals(
                "nombre de vélos en bon état + vélos à réviser",
                this.gareAEquilibrer.capacite() - (4 + 4),
                this.gareAEquilibrer.nbBornesLibres()
        );

        int nombreDeNull = 0;
        for (int i = 1; i < this.gareAEquilibrer.capacite() + 1; ++i) {
            IVelo veloBorneI = this.gareAEquilibrer.veloALaBorne(i);
            if (veloBorneI == null) {
                ++nombreDeNull;
                continue;
            }

            Assert.assertFalse(veloBorneI.estAbime());
            // Les vélos avec assistance électrique viennent de la réserve
            Assert.assertFalse(veloBorneI.toString().contains("assistance électrique"));
        }

        Assert.assertEquals(this.gareAEquilibrer.nbBornesLibres(), nombreDeNull);
    }

    @Test
    public void testEquilibrerQueVelosAReviserEnReserve() {
        HashSet<IVelo> reserve = new HashSet<>();
        for (int i = 0; i < this.gareAEquilibrer.capacite() + 1; i++) {
            reserve.add(creerVeloAReviser());
        }

        this.gareAEquilibrer.equilibrer(reserve);

        Assert.assertEquals(
                "Capacité - 4 vélos originaux en bon état - 4 vélos originaux à réviser",
                this.gareAEquilibrer.capacite() - (4 + 4),
                this.gareAEquilibrer.nbBornesLibres()
        );

        int nombreDeNull = 0;
        int nombreDeVeloAReviser = 0;
        for (int i = 1; i < this.gareAEquilibrer.capacite() + 1; ++i) {
            IVelo veloBorneI = this.gareAEquilibrer.veloALaBorne(i);
            if (veloBorneI == null) {
                ++nombreDeNull;
                continue;
            }

            Assert.assertFalse(veloBorneI.estAbime());

            if (veloBorneI.prochaineRevision() <= 0) {
                ++nombreDeVeloAReviser;
            }
        }

        Assert.assertEquals(
                "Le nombre de vélos à réviser initial",
                4,
                nombreDeVeloAReviser
        );

        Assert.assertEquals(this.gareAEquilibrer.nbBornesLibres(), nombreDeNull);
    }

    @Test
    public void testEquilibrerPasAssezDeBonsVelos() {
        HashSet<IVelo> reserve = new HashSet<>();
        reserve.add(creerVeloBonEtat());
        reserve.add(creerVeloAReviser());

        this.gareAEquilibrer.equilibrer(reserve);

        Assert.assertEquals(
                "Capacité - (nombre de vélos en bon état initial + vélos à réviser + 1 bon vélo qui vient de la réserve)",
                this.gareAEquilibrer.capacite() - (4 + 4 + 1),
                this.gareAEquilibrer.nbBornesLibres()
        );

        int nombreDeNull = 0;
        int nombreDeVeloAReviser = 0;
        for (int i = 1; i < this.gareAEquilibrer.capacite() + 1; ++i) {
            IVelo veloBorneI = this.gareAEquilibrer.veloALaBorne(i);
            if (veloBorneI == null) {
                ++nombreDeNull;
                continue;
            }

            Assert.assertFalse(veloBorneI.estAbime());
            if (veloBorneI.prochaineRevision() <= 0) {
                ++nombreDeVeloAReviser;
            }
        }

        Assert.assertEquals(
                "Le nombre de vélo à réviser initial",
                4,
                nombreDeVeloAReviser
        );

        Assert.assertEquals(this.gareAEquilibrer.nbBornesLibres(), nombreDeNull);
    }

    @Test
    public void testEquilibrerTropDeVelosEnBonEtatDeBase() {
        HashSet<IVelo> reserve = new HashSet<>();
        for (int i = 0; i < this.gareAEquilibrer.capacite() + 1; i++) {
            reserve.add(creerVeloBonEtat());
        }

        // Ajout des vélos en bon état à la gare
        this.gareAEquilibrer.arrimerVelo(creerVeloBonEtat(), 5);
        this.gareAEquilibrer.arrimerVelo(creerVeloBonEtat(), 10);
        this.gareAEquilibrer.arrimerVelo(creerVeloBonEtat(), 11);
        for (int i = 16; i < this.gareAEquilibrer.capacite() + 1; i++) {
            this.gareAEquilibrer.arrimerVelo(creerVeloBonEtat(), i);
        }
        assert (this.gareAEquilibrer.nbBornesLibres() == 0);

        this.gareAEquilibrer.equilibrer(reserve);

        Assert.assertEquals(
                this.gareAEquilibrer.capacite() / 2,
                this.gareAEquilibrer.nbBornesLibres()
        );

        int nombreDeNull = 0;
        for (int i = 1; i < this.gareAEquilibrer.capacite() + 1; ++i) {
            IVelo veloBorneI = this.gareAEquilibrer.veloALaBorne(i);
            if (veloBorneI == null) {
                ++nombreDeNull;
                continue;
            }

            Assert.assertFalse(veloBorneI.estAbime());
            Assert.assertFalse(veloBorneI.prochaineRevision() <= 0);
        }

        Assert.assertEquals(this.gareAEquilibrer.nbBornesLibres(), nombreDeNull);
    }

    @Test
    public void testEquilibrerCapaciteImpaire() {
        Station testStationImpaire = new Station("Nom station", LAT_VIOTTE, LONG_VIOTTE, 5);

        HashSet<IVelo> reserve = new HashSet<>();
        for (int i = 0; i < testStationImpaire.capacite() + 1; i++) {
            reserve.add(creerVeloBonEtat());
        }

        testStationImpaire.arrimerVelo(creerVeloAbime(), 1);
        testStationImpaire.arrimerVelo(creerVeloBonEtat(), 2);
        testStationImpaire.arrimerVelo(creerVeloAReviser(), 3);

        testStationImpaire.equilibrer(reserve);

        // Math.floor car nbBornesLibres = capacité - nbBornesUtilisées
        Assert.assertEquals(
                (int) Math.floor(testStationImpaire.capacite() / 2.0),
                testStationImpaire.nbBornesLibres()
        );

        int nombreDeNull = verificationVelosStationNonAbimesNonAReviser(testStationImpaire);
        Assert.assertEquals(testStationImpaire.nbBornesLibres(), nombreDeNull);
    }

    @Test
    public void testEquilibrerQuelquesBonsVelosQuelquesMauvaisVelos() {
        HashSet<IVelo> reserve = new HashSet<>();
        for (int i = 0; i < 2; i++) {
            reserve.add(creerVeloBonEtat());
        }
        for (int i = 2; i < 4; i++) {
            reserve.add(creerVeloAbime());
        }
        for (int i = 4; i < 6; i++) {
            reserve.add(creerVeloAReviser());
        }

        this.gareAEquilibrer.equilibrer(reserve);

        Assert.assertEquals(
                this.gareAEquilibrer.capacite() / 2,
                this.gareAEquilibrer.nbBornesLibres()
        );

        int nombreDeNull = 0;
        int nombreVeloAReviser = 0;
        for (int i = 1; i < this.gareAEquilibrer.capacite() + 1; ++i) {
            IVelo veloBorneI = this.gareAEquilibrer.veloALaBorne(i);
            if (veloBorneI == null) {
                ++nombreDeNull;
                continue;
            }

            Assert.assertFalse(veloBorneI.estAbime());
            if (veloBorneI.prochaineRevision() <= 0) {
                ++nombreVeloAReviser;
            }
        }

        Assert.assertEquals(this.gareAEquilibrer.nbBornesLibres(), nombreDeNull);
        Assert.assertEquals("Nombre de vélo à réviser initial était", 4, nombreVeloAReviser);
    }

    @Test
    public void testDistanceTokyo() {
        Station tokyo = new Station("tokyo", 138.252924, 36.204824, 0);

        Assert.assertEquals(14590.2293, this.gareViotte.distance(tokyo), DELTA_TRES_LONGUE_DISTANCE);
    }

    @Test
    public void testDistanceNull() {
        Assert.assertEquals(0.0, this.gareViotte.distance(null), 0.00001);
    }

    @Test
    public void testDistance0() {
        Assert.assertEquals(0.0, this.gareViotte.distance(this.gareViotte), 0.00001);
    }

    @Test
    public void testMaintenant() {
        Assert.assertEquals(System.currentTimeMillis(), this.gareViotte.maintenant());
    }

    @Test
    public void testMaintenantAvecDelai() {
        Station spyGareViotte = Mockito.spy(this.gareViotte);
        long dans10minutes = System.currentTimeMillis() + 10 * 60 * 1000;
        Mockito.when(spyGareViotte.maintenant()).thenReturn(dans10minutes);

        Assert.assertTrue(this.gareViotte.maintenant() < spyGareViotte.maintenant());
    }
}
