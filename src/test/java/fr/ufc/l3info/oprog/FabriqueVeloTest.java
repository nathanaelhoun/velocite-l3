package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Test unitaire pour les options.
 */
public class FabriqueVeloTest {
    final double DELTA_DISTANCE = 0.0001;
    final double DELTA_EURO = 0.001;
    final double DISTANCE_ENTRE_REVISIONS = 500;
    private FabriqueVelo fv;

    @Before
    @Test
    public void before_func() {
        fv = FabriqueVelo.getInstance();
    }

    @Test
    public void get_InstanceSingleton() {
        FabriqueVelo f = FabriqueVelo.getInstance();
        Assert.assertEquals(fv, f);
    }

    @Test
    public void construireBasique() {
        IVelo v = fv.construire('a');
        Assert.assertTrue(v.toString().contains("mixte"));
        Assert.assertEquals(v.tarif(), 2.0, 0);
    }

    @Test
    public void construireBasique2() {
        IVelo v = fv.construire((char) 8);
        Assert.assertTrue(v.toString().contains("mixte"));
        Assert.assertEquals(v.tarif(), 2.0, 0);
    }

    @Test
    public void construireBasique3() {
        IVelo v = fv.construire('8');
        Assert.assertTrue(v.toString().contains("mixte"));
        Assert.assertEquals(v.tarif(), 2.0, 0);
    }

    @Test
    public void construireHomme() {
        IVelo v = fv.construire('H');
        Assert.assertTrue(v.toString().contains("homme"));
        Assert.assertEquals(v.tarif(), 2.0, 0);
        IVelo v2 = fv.construire('h');
        Assert.assertTrue(v2.toString().contains("homme"));
        Assert.assertEquals(v2.tarif(), 2.0, 0);
        Assert.assertNotEquals(v, v2);
    }

    @Test
    public void construireFemme() {
        IVelo v = fv.construire('F');
        Assert.assertTrue(v.toString().contains("femme"));
        Assert.assertEquals(v.tarif(), 2.0, 0);
        IVelo v2 = fv.construire('f');
        Assert.assertTrue(v2.toString().contains("femme"));
        Assert.assertEquals(v2.tarif(), 2.0, 0);
        Assert.assertNotEquals(v, v2);
    }

    @Test
    public void addCadresAluValide() {
        IVelo v = fv.construire('a', "CADRE_ALUMINIUM");
        Assert.assertTrue(v.toString().contains("cadre aluminium"));
        Assert.assertFalse(v.toString().contains("CADRE_ALUMINIUM"));
        Assert.assertFalse(v.toString().contains("CADRE ALUMINIUM"));
        Assert.assertFalse(v.toString().contains("cadre_aluminium"));
        Assert.assertEquals(v.tarif(), 2.2, 0.01);
    }

    @Test
    public void addCadresAluNonValide() {
        IVelo v = fv.construire('a', "CADRE ALUMINIUM");
        Assert.assertFalse(v.toString().contains("cadre aluminium"));
        Assert.assertEquals(v.tarif(), 2.0, 0.01);
    }

    @Test
    public void addFreinsDisqueValide() {
        IVelo v = fv.construire('a', "FREINS_DISQUE");
        Assert.assertTrue(v.toString().contains("freins à disque"));
        Assert.assertFalse(v.toString().contains("freins disque"));
        Assert.assertFalse(v.toString().contains("FREINS_DISQUE"));
        Assert.assertEquals(v.tarif(), 2.3, 0.01);
    }

    @Test
    public void addFreinsDisqueNonValide() {
        IVelo v = fv.construire('a', "FREINSDISQUE");
        Assert.assertFalse(v.toString().contains("freins à disque"));
        Assert.assertEquals(v.tarif(), 2.0, 0.01);
    }

    @Test
    public void addSuspAvantValide() {
        IVelo v = fv.construire('a', "SUSPENSION_AVANT");
        Assert.assertTrue(v.toString().contains("suspension avant"));
        Assert.assertFalse(v.toString().contains("SUSPENSION_AVANT"));
        Assert.assertFalse(v.toString().contains("suspension arrière"));
        Assert.assertEquals(v.tarif(), 2.5, 0.01);
    }

    @Test
    public void addSuspAvantNonValide() {
        IVelo v = fv.construire('a', "suspension_avant");
        Assert.assertFalse(v.toString().contains("suspension avant"));
        Assert.assertEquals(v.tarif(), 2.0, 0.01);
    }

    @Test
    public void addSuspArriereValide() {
        IVelo v = fv.construire('a', "SUSPENSION_ARRIERE");
        Assert.assertTrue(v.toString().contains("suspension arrière"));
        Assert.assertFalse(v.toString().contains("suspension avant"));
        Assert.assertFalse(v.toString().contains("SUSPENSION_ARRIERE"));
        Assert.assertEquals(v.tarif(), 2.5, 0.01);
    }

    @Test
    public void addSuspArriereNonValide() {
        IVelo v = fv.construire('a', "SUSPENSION-ARRIERE");
        Assert.assertFalse(v.toString().contains("suspension arrière"));
        Assert.assertEquals(v.tarif(), 2.0, 0.01);
    }

    @Test
    public void addAssistElecValide() {
        IVelo v = fv.construire('a', "ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(v.toString().contains("assistance électrique"));
        Assert.assertFalse(v.toString().contains("ASSISTANCE_ELECTRIQUE"));
        Assert.assertEquals(v.tarif(), 4.0, 0.01);
    }

    @Test
    public void addAssistElecNonValide() {
        IVelo v = fv.construire('a', "ASSISTANCE ELECTRIQUE");
        Assert.assertFalse(v.toString().contains("assistance électrique"));
        Assert.assertEquals(v.tarif(), 2.0, 0.01);
    }

    /********************** TESTS FABRIQUE ***********************/
    @Test
    public void combinaisonOptionValide() {
        IVelo v = fv.construire('h', "ASSISTANCE_ELECTRIQUE", "CADRE_ALUMINIUM", "SUSPENSION_ARRIERE");
        Assert.assertTrue(v.toString().contains("assistance électrique"));
        Assert.assertTrue(v.toString().contains("cadre aluminium"));
        Assert.assertTrue(v.toString().contains("suspension arrière"));
        Assert.assertTrue(v.toString().contains("homme"));
        Assert.assertEquals(v.tarif(), 4.7, 0.01);
    }

    @Test
    public void combinaisonOptionNonValide() {
        IVelo v = fv.construire('f', "ASSISTANCE_ELECTRIQUE", "CADREALUMINIUM", "SUSPENSION_ARRIERE");
        Assert.assertTrue(v.toString().contains("assistance électrique"));
        Assert.assertFalse(v.toString().contains("cadre aluminium"));
        Assert.assertTrue(v.toString().contains("suspension arrière"));
        Assert.assertTrue(v.toString().contains("femme"));
        Assert.assertEquals(v.tarif(), 4.5, 0.01);
    }

    @Test
    public void combinaisonOptionNonValide2() {
        IVelo v = fv.construire('\0', "ASSISTANcE_ELECTRIQUE", "FREINS_DISQUE", "SUSPENSION_ARRIERE");
        Assert.assertEquals(v.tarif(), 2.8, 0.01);
        Assert.assertTrue(v.toString().contains("freins à disque"));
        Assert.assertTrue(v.toString().contains("suspension arrière"));
        Assert.assertFalse(v.toString().contains("assistance électrique"));
        Assert.assertTrue(v.toString().contains("mixte"));
        Assert.assertEquals(v.tarif(), 2.8, 0.01);
    }

    @Test
    public void combinaisonOptionNonValide3() {
        IVelo v = fv.construire('\0', null, null, null);
        Assert.assertEquals("Vélo cadre mixte - 0.0 km", v.toString());
        Assert.assertEquals(v.tarif(), 2.0, 0.01);
    }

    @Test
    public void combinaisonAllOptionValide() {
        IVelo v = fv.construire('\0', "CADRE_ALUMINIUM", "SUSPENSION_AVANT", "ASSISTANCE_ELECTRIQUE", "FREINS_DISQUE",
                "SUSPENSION_ARRIERE");
        Assert.assertTrue(v.toString().contains("freins à disque"));
        Assert.assertTrue(v.toString().contains("suspension arrière"));
        Assert.assertTrue(v.toString().contains("assistance électrique"));
        Assert.assertTrue(v.toString().contains("suspension avant"));
        Assert.assertTrue(v.toString().contains("cadre aluminium"));
        Assert.assertEquals(v.tarif(), 5.5, 0.01);
    }

    @Test
    public void combinaisonAllOptionValideDouble() {
        IVelo v = fv.construire('\0', "CADRE_ALUMINIUM", "SUSPENSION_AVANT", "ASSISTANCE_ELECTRIQUE", "FREINS_DISQUE",
                "SUSPENSION_ARRIERE", "CADRE_ALUMINIUM", "SUSPENSION_AVANT", "ASSISTANCE_ELECTRIQUE", "FREINS_DISQUE",
                "SUSPENSION_ARRIERE");
        Assert.assertTrue(v.toString().contains("freins à disque"));
        String[] str = v.toString().split("freins à disque");
        Assert.assertEquals(2, str.length);
        Assert.assertTrue(v.toString().contains("suspension arrière"));
        str = v.toString().split("suspension arrière");
        Assert.assertEquals(2, str.length);
        Assert.assertTrue(v.toString().contains("assistance électrique"));
        str = v.toString().split("assistance électrique");
        Assert.assertEquals(2, str.length);
        Assert.assertTrue(v.toString().contains("suspension avant"));
        str = v.toString().split("suspension avant");
        Assert.assertEquals(2, str.length);
        Assert.assertTrue(v.toString().contains("cadre aluminium"));
        str = v.toString().split("cadre aluminium");
        Assert.assertEquals(2, str.length);
        Assert.assertEquals(v.tarif(), 5.5, 0.01);
    }

    @Test
    public void combinaisonOptionVeloNullHomme() {
        IVelo v = fv.construire('h', null);
        Assert.assertEquals("Vélo cadre homme - 0.0 km", v.toString());
        Assert.assertEquals(v.tarif(), 2.0, 0.01);
        IVelo v2 = fv.construire('H', null);
        Assert.assertEquals("Vélo cadre homme - 0.0 km", v2.toString());
        Assert.assertEquals(v2.tarif(), 2.0, 0.01);
        Assert.assertNotEquals(v, v2);
    }

    @Test
    public void combinaisonOptionVeloNullFemme() {
        IVelo v = fv.construire('f', null);
        Assert.assertEquals("Vélo cadre femme - 0.0 km", v.toString());
        Assert.assertEquals(v.tarif(), 2.0, 0.01);
        IVelo v2 = fv.construire('F', null);
        Assert.assertEquals("Vélo cadre femme - 0.0 km", v2.toString());
        Assert.assertEquals(v2.tarif(), 2.0, 0.01);
        Assert.assertNotEquals(v, v2);
    }

    @Test
    public void combinaisonOptionValideWithNull() {
        IVelo v = fv.construire('\0', "CADRE_ALUMINIUM", null, "ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(v.toString().contains("cadre aluminium"));
        Assert.assertTrue(v.toString().contains("assistance électrique"));
        Assert.assertEquals("Vélo cadre mixte, cadre aluminium, assistance électrique - 0.0 km", v.toString());
        Assert.assertEquals(v.tarif(), 4.2, 0.01);
    }

    @Test
    public void combinaisonOptionValideWithNull2() {
        IVelo v = fv.construire('\0', "CADRE_ALUMINIUM", null, "CADRE_ALUMINIUM");
        Assert.assertTrue(v.toString().contains("cadre aluminium"));
        String[] str = v.toString().split("cadre aluminium");
        Assert.assertEquals(2, str.length);
        Assert.assertEquals("Vélo cadre mixte, cadre aluminium - 0.0 km", v.toString());
        Assert.assertEquals(v.tarif(), 2.2, 0.01);
    }

    @Test
    public void combinaisonOptionValideWithNull3() {
        IVelo v = fv.construire('\0', null, "CADRE_ALUMINIUM");
        Assert.assertEquals("Vélo cadre mixte, cadre aluminium - 0.0 km", v.toString());
        Assert.assertEquals(v.tarif(), 2.2, 0.01);
    }

    @Test
    public void combinaisonOptionDouble() {
        IVelo v = fv.construire('\0', "CADRE_ALUMINIUM", "CADRE_ALUMINIUM");
        Assert.assertTrue(v.toString().contains("cadre aluminium"));
        String[] str = v.toString().split("cadre aluminium");
        Assert.assertEquals(2, str.length);
        Assert.assertEquals("Vélo cadre mixte, cadre aluminium - 0.0 km", v.toString());
        Assert.assertEquals(v.tarif(), 2.2, 0.01);
    }

    @Test
    public void combinaisonOptionDouble2() {
        IVelo v = fv.construire('\0', "cADRE_ALUMINIUM", "CADRE_ALUMINIUM");
        Assert.assertEquals("Vélo cadre mixte, cadre aluminium - 0.0 km", v.toString());
        Assert.assertEquals(v.tarif(), 2.2, 0.01);
    }

    @Test
    public void combinaisonOptionDouble3() {
        IVelo v = fv.construire('\0', "cadre_aluminium", "CADREALUMINIUM");
        Assert.assertEquals("Vélo cadre mixte - 0.0 km", v.toString());
        Assert.assertEquals(v.tarif(), 2.0, 0.01);
    }

    @Test
    public void combinaisonsOption() {
        IVelo v = fv.construire('\0', "CADRE_ALUMINIUM", "FREINS_DISQUE");
        Assert.assertTrue(v.toString().contains("cadre aluminium"));
        Assert.assertTrue(v.toString().contains("freins à disque"));
        Assert.assertEquals(v.tarif(), 2.5, 0.01);

        IVelo v2 = fv.construire('\0', "CADRE_ALUMINIUM", "SUSPENSION_AVANT");
        Assert.assertTrue(v2.toString().contains("cadre aluminium"));
        Assert.assertTrue(v2.toString().contains("suspension avant"));
        Assert.assertEquals(v2.tarif(), 2.7, 0.01);

        IVelo v3 = fv.construire('\0', "CADRE_ALUMINIUM", "SUSPENSION_ARRIERE");
        Assert.assertTrue(v3.toString().contains("cadre aluminium"));
        Assert.assertTrue(v3.toString().contains("suspension arrière"));
        Assert.assertEquals(v3.tarif(), 2.7, 0.01);

        IVelo v4 = fv.construire('\0', "CADRE_ALUMINIUM", "ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(v4.toString().contains("cadre aluminium"));
        Assert.assertTrue(v4.toString().contains("assistance électrique"));
        Assert.assertEquals(v4.tarif(), 4.2, 0.01);

        IVelo v5 = fv.construire('\0', "CADRE_ALUMINIUM", "FREINS_DISQUE", "SUSPENSION_AVANT");
        Assert.assertTrue(v5.toString().contains("cadre aluminium"));
        Assert.assertTrue(v5.toString().contains("freins à disque"));
        Assert.assertTrue(v5.toString().contains("suspension avant"));
        Assert.assertEquals(v5.tarif(), 3, 0.01);

        IVelo v6 = fv.construire('\0', "CADRE_ALUMINIUM", "FREINS_DISQUE", "SUSPENSION_ARRIERE");
        Assert.assertTrue(v6.toString().contains("cadre aluminium"));
        Assert.assertTrue(v6.toString().contains("freins à disque"));
        Assert.assertTrue(v6.toString().contains("suspension arrière"));
        Assert.assertEquals(v6.tarif(), 3, 0.01);

        IVelo v7 = fv.construire('\0', "CADRE_ALUMINIUM", "FREINS_DISQUE", "ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(v7.toString().contains("cadre aluminium"));
        Assert.assertTrue(v7.toString().contains("freins à disque"));
        Assert.assertTrue(v7.toString().contains("assistance électrique"));
        Assert.assertEquals(v7.tarif(), 4.5, 0.01);

        IVelo v8 = fv.construire('\0', "CADRE_ALUMINIUM", "FREINS_DISQUE", "SUSPENSION_AVANT", "SUSPENSION_ARRIERE");
        Assert.assertTrue(v8.toString().contains("cadre aluminium"));
        Assert.assertTrue(v8.toString().contains("freins à disque"));
        Assert.assertTrue(v8.toString().contains("suspension avant"));
        Assert.assertTrue(v8.toString().contains("suspension arrière"));
        Assert.assertEquals(v8.tarif(), 3.5, 0.01);

        IVelo v9 = fv.construire('\0', "CADRE_ALUMINIUM", "FREINS_DISQUE", "SUSPENSION_AVANT", "ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(v9.toString().contains("cadre aluminium"));
        Assert.assertTrue(v9.toString().contains("freins à disque"));
        Assert.assertTrue(v9.toString().contains("suspension avant"));
        Assert.assertTrue(v9.toString().contains("assistance électrique"));
        Assert.assertEquals(v9.tarif(), 5, 0.01);

        IVelo v10 = fv.construire('\0', "FREINS_DISQUE", "ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(v10.toString().contains("freins à disque"));
        Assert.assertTrue(v10.toString().contains("assistance électrique"));
        Assert.assertEquals(v10.tarif(), 4.3, 0.01);

        IVelo v11 = fv.construire('\0', "FREINS_DISQUE", "SUSPENSION_AVANT");
        Assert.assertTrue(v11.toString().contains("freins à disque"));
        Assert.assertTrue(v11.toString().contains("suspension avant"));
        Assert.assertEquals(v11.tarif(), 2.8, 0.01);

        IVelo v12 = fv.construire('\0', "FREINS_DISQUE", "SUSPENSION_ARRIERE");
        Assert.assertTrue(v12.toString().contains("freins à disque"));
        Assert.assertTrue(v12.toString().contains("suspension arrière"));
        Assert.assertEquals(v12.tarif(), 2.8, 0.01);

        IVelo v13 = fv.construire('\0', "FREINS_DISQUE", "SUSPENSION_ARRIERE", "SUSPENSION_AVANT");
        Assert.assertTrue(v13.toString().contains("suspension arrière"));
        Assert.assertTrue(v13.toString().contains("freins à disque"));
        Assert.assertTrue(v13.toString().contains("suspension avant"));
        Assert.assertEquals(v13.tarif(), 3.3, 0.01);

        IVelo v14 = fv.construire('\0', "FREINS_DISQUE", "SUSPENSION_ARRIERE", "ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(v14.toString().contains("suspension arrière"));
        Assert.assertTrue(v14.toString().contains("freins à disque"));
        Assert.assertTrue(v14.toString().contains("assistance électrique"));
        Assert.assertEquals(v14.tarif(), 4.8, 0.01);

        IVelo v15 = fv.construire('\0', "FREINS_DISQUE", "SUSPENSION_AVANT", "ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(v15.toString().contains("suspension avant"));
        Assert.assertTrue(v15.toString().contains("freins à disque"));
        Assert.assertTrue(v15.toString().contains("assistance électrique"));
        Assert.assertEquals(v15.tarif(), 4.8, 0.01);

        IVelo v16 = fv.construire('\0', "FREINS_DISQUE", "SUSPENSION_AVANT", "ASSISTANCE_ELECTRIQUE",
                "SUSPENSION_ARRIERE");
        Assert.assertTrue(v16.toString().contains("suspension avant"));
        Assert.assertTrue(v16.toString().contains("freins à disque"));
        Assert.assertTrue(v16.toString().contains("assistance électrique"));
        Assert.assertTrue(v16.toString().contains("suspension arrière"));
        Assert.assertEquals(v16.tarif(), 5.3, 0.01);

        IVelo v17 = fv.construire('\0', "SUSPENSION_AVANT", "ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(v17.toString().contains("suspension avant"));
        Assert.assertTrue(v17.toString().contains("assistance électrique"));
        Assert.assertEquals(v17.tarif(), 4.5, 0.01);

        IVelo v18= fv.construire('\0', "SUSPENSION_AVANT", "SUSPENSION_ARRIERE");
        Assert.assertTrue(v18.toString().contains("suspension avant"));
        Assert.assertTrue(v18.toString().contains("suspension arrière"));
        Assert.assertEquals(v18.tarif(), 3, 0.01);

        IVelo v19 = fv.construire('\0', "SUSPENSION_AVANT", "SUSPENSION_ARRIERE", "ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(v19.toString().contains("suspension avant"));
        Assert.assertTrue(v19.toString().contains("suspension arrière"));
        Assert.assertTrue(v19.toString().contains("assistance électrique"));
        Assert.assertEquals(v19.tarif(), 5, 0.01);

        IVelo v20= fv.construire('\0', "SUSPENSION_ARRIERE", "ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(v20.toString().contains("assistance électrique"));
        Assert.assertTrue(v20.toString().contains("suspension arrière"));
        Assert.assertEquals(v20.tarif(), 4.5, 0.01);

        IVelo v21 = fv.construire('\0', "CADRE_ALUMINIUM", "SUSPENSION_ARRIERE", "ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(v21.toString().contains("cadre aluminium"));
        Assert.assertTrue(v21.toString().contains("suspension arrière"));
        Assert.assertTrue(v21.toString().contains("assistance électrique"));
        Assert.assertEquals(v21.tarif(), 4.7, 0.01);

        IVelo v22 = fv.construire('\0', "CADRE_ALUMINIUM", "SUSPENSION_AVANT", "ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(v22.toString().contains("cadre aluminium"));
        Assert.assertTrue(v22.toString().contains("suspension avant"));
        Assert.assertTrue(v22.toString().contains("assistance électrique"));
        Assert.assertEquals(v22.tarif(), 4.7, 0.01);

        IVelo v23 = fv.construire('\0', "CADRE_ALUMINIUM", "SUSPENSION_AVANT", "SUSPENSION_ARRIERE");
        Assert.assertTrue(v23.toString().contains("cadre aluminium"));
        Assert.assertTrue(v23.toString().contains("suspension avant"));
        Assert.assertTrue(v23.toString().contains("suspension arrière"));
        Assert.assertEquals(v23.tarif(), 3.2, 0.01);

        IVelo v24 = fv.construire('\0', "CADRE_ALUMINIUM", "ASSISTANCE_ELECTRIQUE", "SUSPENSION_ARRIERE");
        Assert.assertTrue(v24.toString().contains("cadre aluminium"));
        Assert.assertTrue(v24.toString().contains("assistance électrique"));
        Assert.assertTrue(v24.toString().contains("suspension arrière"));
        Assert.assertEquals(v24.tarif(), 4.7, 0.01);

        IVelo v25 = fv.construire('\0', "CADRE_ALUMINIUM", "FREINS_DISQUE", "SUSPENSION_ARRIERE",
                "ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(v25.toString().contains("cadre aluminium"));
        Assert.assertTrue(v25.toString().contains("freins à disque"));
        Assert.assertTrue(v25.toString().contains("suspension arrière"));
        Assert.assertTrue(v25.toString().contains("assistance électrique"));
        Assert.assertEquals(v25.tarif(), 5, 0.01);

        IVelo v26 = fv.construire('\0', "CADRE_ALUMINIUM", "SUSPENSION_ARRIERE", "SUSPENSION_AVANT",
                "ASSISTANCE_ELECTRIQUE");
        Assert.assertTrue(v26.toString().contains("cadre aluminium"));
        Assert.assertTrue(v26.toString().contains("suspension arrière"));
        Assert.assertTrue(v26.toString().contains("suspension avant"));
        Assert.assertTrue(v26.toString().contains("assistance électrique"));
        Assert.assertEquals(v26.tarif(), 5.2, 0.01);
    }

    @Ignore
    public void verifierStringVelo(String chaine, String... chainesOptions) {
        Assert.assertTrue("Partie de base de la chaîne", chaine.startsWith("Vélo cadre mixte"));
        chaine = chaine.replaceAll("Vélo cadre mixte", "");
        Assert.assertFalse("Partie de base de la chaîne", chaine.startsWith("Vélo cadre mixte"));

        Assert.assertTrue("Partie de base de la chaîne", chaine.endsWith(" - 0.0 km"));
        chaine = chaine.replaceAll(" - 0.0 km", "");
        Assert.assertFalse("Partie de base de la chaîne", chaine.endsWith(" - 0.0 km"));


        for (String opt : chainesOptions) {
            Assert.assertTrue("Option présente : " + opt, chaine.contains(opt));
            chaine = chaine.replaceAll(opt, "");
            Assert.assertFalse("Option absente : " + opt, chaine.contains(opt));
        }

        Assert.assertEquals("", chaine);
    }


    @Test
    public void testGetInstance() {
        Assert.assertNotNull(this.fv);
        FabriqueVelo fab2 = FabriqueVelo.getInstance();
        Assert.assertEquals(this.fv, fab2);
    }

    @Test
    public void testConstruireDefaut() {
        IVelo v = this.fv.construire('a');
        Assert.assertEquals("Vélo cadre mixte - 0.0 km", v.toString());
        Assert.assertFalse(v.estAbime());
        Assert.assertEquals(2.0, v.tarif(), this.DELTA_EURO);

        Velo v2 = new Velo('z');
        Assert.assertEquals("Vélo cadre mixte - 0.0 km", v2.toString());
        Assert.assertFalse(v2.estAbime());
        Assert.assertEquals(2.0, v2.tarif(), this.DELTA_EURO);
    }


    @Test
    public void testConstruireHomme() {
        IVelo v2 = this.fv.construire('h');
        Assert.assertEquals("Vélo cadre homme - 0.0 km", v2.toString());
        Assert.assertFalse(v2.estAbime());
        Assert.assertEquals(2.0, v2.tarif(), this.DELTA_EURO);

        v2 = this.fv.construire('H');
        Assert.assertEquals("Vélo cadre homme - 0.0 km", v2.toString());
        Assert.assertFalse(v2.estAbime());
        Assert.assertEquals(2.0, v2.tarif(), this.DELTA_EURO);
    }

    @Test
    public void testConstruireFemme() {
        IVelo v3 = this.fv.construire('F');
        Assert.assertEquals("Vélo cadre femme - 0.0 km", v3.toString());
        Assert.assertFalse(v3.estAbime());
        Assert.assertEquals(2.0, v3.tarif(), this.DELTA_EURO);

        v3 = this.fv.construire('f');
        Assert.assertEquals("Vélo cadre femme - 0.0 km", v3.toString());
        Assert.assertFalse(v3.estAbime());
        Assert.assertEquals(2.0, v3.tarif(), this.DELTA_EURO);
    }

    @Test
    public void testConstruireAvecUneOption() {
        IVelo v = this.fv.construire('a', "CADRE_ALUMINIUM");
        Assert.assertEquals("Vélo cadre mixte, cadre aluminium - 0.0 km", v.toString());

        v = this.fv.construire('a', "FREINS_DISQUE");
        Assert.assertEquals("Vélo cadre mixte, freins à disque - 0.0 km", v.toString());

        v = this.fv.construire('a', "SUSPENSION_AVANT");
        Assert.assertEquals("Vélo cadre mixte, suspension avant - 0.0 km", v.toString());

        v = this.fv.construire('a', "SUSPENSION_ARRIERE");
        Assert.assertEquals("Vélo cadre mixte, suspension arrière - 0.0 km", v.toString());

        v = this.fv.construire('a', "ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals("Vélo cadre mixte, assistance électrique - 0.0 km", v.toString());
    }

    @Test
    public void testConstruireAvecUneOptionHomme() {
        IVelo v = this.fv.construire('h', "CADRE_ALUMINIUM");
        Assert.assertEquals("Vélo cadre homme, cadre aluminium - 0.0 km", v.toString());

        v = this.fv.construire('H', "FREINS_DISQUE");
        Assert.assertEquals("Vélo cadre homme, freins à disque - 0.0 km", v.toString());

        v = this.fv.construire('h', "SUSPENSION_AVANT");
        Assert.assertEquals("Vélo cadre homme, suspension avant - 0.0 km", v.toString());

        v = this.fv.construire('H', "SUSPENSION_ARRIERE");
        Assert.assertEquals("Vélo cadre homme, suspension arrière - 0.0 km", v.toString());

        v = this.fv.construire('h', "ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals("Vélo cadre homme, assistance électrique - 0.0 km", v.toString());
    }

    @Test
    public void testConstruireAvecUneOptionFemme() {
        IVelo v = this.fv.construire('F', "CADRE_ALUMINIUM");
        Assert.assertEquals("Vélo cadre femme, cadre aluminium - 0.0 km", v.toString());

        v = this.fv.construire('f', "FREINS_DISQUE");
        Assert.assertEquals("Vélo cadre femme, freins à disque - 0.0 km", v.toString());

        v = this.fv.construire('f', "SUSPENSION_AVANT");
        Assert.assertEquals("Vélo cadre femme, suspension avant - 0.0 km", v.toString());

        v = this.fv.construire('f', "SUSPENSION_ARRIERE");
        Assert.assertEquals("Vélo cadre femme, suspension arrière - 0.0 km", v.toString());

        v = this.fv.construire('f', "ASSISTANCE_ELECTRIQUE");
        Assert.assertEquals("Vélo cadre femme, assistance électrique - 0.0 km", v.toString());
    }

    @SuppressWarnings("ConfusingArgumentToVarargsMethod")
    @Test
    public void testConstruireOptionNull() {
        IVelo velo = this.fv.construire('h', null);
        Assert.assertEquals("Vélo cadre homme - 0.0 km", velo.toString());
        Assert.assertEquals(2.0, velo.tarif(), this.DELTA_EURO);
    }

    @Test
    public void testConstruireOptionIncorrecte() {
        IVelo v = this.fv.construire(
                'a',
                "DIEODIOE",
                "DIOOEDIEOD",
                "ASSISTANCE_AVANT",
                "SUSPENSION_MILIEU",
                "MOTEUR_DE_COURSE",
                "SUSPENSION AVANT",
                "SUSPENSION_ARRIÈRE",
                "cadre_aluminium",
                null
        );
        this.verifierStringVelo(v.toString());
    }

    @Test
    public void testConstruireAvecPlusieursOptions() {
        String[] chainesOptions = {
                ", cadre aluminium",
                ", freins à disque",
                ", suspension avant",
                ", suspension arrière",
                ", assistance électrique"
        };

        IVelo v = this.fv.construire(
                'a',
                "CADRE_ALUMINIUM",
                "FREINS_DISQUE",
                "SUSPENSION_AVANT",
                "SUSPENSION_ARRIERE",
                "ASSISTANCE_ELECTRIQUE"
        );

        this.verifierStringVelo(v.toString(), chainesOptions);
    }

    @Test
    public void testConstruireBonneOptionPuisMauvaise() {
        IVelo v = this.fv.construire(
                'a',
                "CADRE_ALUMINIUM",
                "qdsfjkqmsldf",
                "ASSISTANCE_AVANT",
                "SUSPENSION_ELECTRIQUE"
        );

        String[] chainesOptions = {
                ", cadre aluminium",
        };

        this.verifierStringVelo(v.toString(), chainesOptions);
    }

    @Test
    public void testConstruireMauvaiseOptionPuisBonne() {
        IVelo v = this.fv.construire(
                'a',
                "qsdfsdfdsqf",
                "FREINS_DISQUE",
                "ezarizerpjizo",
                "ASSISTANCE_ELECTRIQUE",
                "SUSPENSION_ARRIERE"
        );

        String[] chainesOptions = {
                ", freins à disque",
                ", suspension arrière",
                ", assistance électrique"
        };

        this.verifierStringVelo(v.toString(), chainesOptions);
    }
}