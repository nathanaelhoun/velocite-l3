package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import static org.junit.Assert.*;

public class VilleTest {
    final String path = "./target/classes/data/";
    final String RIB_CORRECT = "11111-11111-11111111111-48";
    final String RIB_INCORRECT_CLE = "11111-11111-11111111111-11";
    final double DELTA_EURO = 0.001;

    final double LAT_DD = 47.2455782;
    final double LONG_DD = 5.9874875;

    Ville besancon;
    Station stationMairie;
    Abonne dd;
    Abonne jpg;
    IVelo velo;

    private long emprunterVeloBorne1Pendant(Station sEmprunt, Station sRendu, Abonne a, long debutEmprunt, long duree) {
        assert (!a.estBloque());
        Station spyStation = Mockito.spy(sEmprunt);
        Mockito.when(spyStation.maintenant()).thenReturn(debutEmprunt);
        IVelo v = spyStation.emprunterVelo(a, 1);
        assert (v != null);

        long finEmprunt = debutEmprunt + duree;
        Station spyStationRendu = Mockito.spy(sRendu);
        Mockito.when(spyStationRendu.maintenant()).thenReturn(finEmprunt);

        assert (spyStationRendu.arrimerVelo(v, 1) == 0);
        return finEmprunt;
    }

    private long ddEmprunternVelos(long startPremierEmprunt, int n, long duree) {
        assert (!dd.estBloque());
        long start = startPremierEmprunt;
        long end = start;
        Station spyStation = Mockito.spy(this.stationMairie);
        for (int i = 0; i < n; i++) {
            start = end + 6000;
            Mockito.when(spyStation.maintenant()).thenReturn(start);
            IVelo velo = spyStation.emprunterVelo(dd, 1);
            assert (velo != null);
            end = start + duree;
            Mockito.when(spyStation.maintenant()).thenReturn(end);
            assert (spyStation.arrimerVelo(velo, 1) == 0);
        }

        return end;
    }

    @Before
    public void beforeFunc() throws IOException {
        this.besancon = new Ville();
        this.besancon.initialiser(new File(path + "stationsBesancon.txt"));

        this.stationMairie = this.besancon.getStation("Mairie");
        this.besancon.setStationPrincipale(this.stationMairie);

        // Ajout d'emprunts
        this.dd = this.besancon.creerAbonne("Dadeau", RIB_CORRECT);
        this.jpg = this.besancon.creerAbonne("JPGros", RIB_CORRECT);
        Station station1 = this.besancon.getStation("Place de la Liberté");
        Station station2 = this.besancon.getStation("Xavier Marmier");

        velo = new Velo();

        station1.arrimerVelo(velo, 1);
        stationMairie.arrimerVelo(new Velo(), 1);

        // Emprunts en février 2019
        long HEURE = 3600000;
        emprunterVeloBorne1Pendant(station1, station1, jpg, 1549105803000L, HEURE); // 2 février 2019
        emprunterVeloBorne1Pendant(station1, station1, jpg, 1551352203000L, HEURE * 2);// 28 février 2019
        emprunterVeloBorne1Pendant(station1, station1, jpg, 1551438603000L, HEURE * 2);// 01 mars 2019

        long finEmprunt = ddEmprunternVelos(1549401003000L, 10, 5 * 60000L); // 05 février 2019
        finEmprunt = ddEmprunternVelos(finEmprunt, 51, 10 * 60000L); // 05 février 2019

        // Emprunts en janvier 2020
        emprunterVeloBorne1Pendant(station1, station1, dd, 1577826603000L, HEURE); // 31 dec 2019 22h 10
        finEmprunt = emprunterVeloBorne1Pendant(station1, station1, dd, 1578082891000L, HEURE); // 3 janvier 2020
        emprunterVeloBorne1Pendant(station1, station1, dd, finEmprunt + HEURE, HEURE);
        finEmprunt = ddEmprunternVelos(1578255691000L, 30, HEURE); // 5 janvier 2020
        emprunterVeloBorne1Pendant(station1, station1, dd, finEmprunt + HEURE, HEURE);

        // Emprunts en février 2020
        emprunterVeloBorne1Pendant(station1, station1, jpg, 1580641803000L, HEURE); // 2 février 2020
        emprunterVeloBorne1Pendant(station1, station1, jpg, 1582974603000L, HEURE * 2);// 29 février 2020
        emprunterVeloBorne1Pendant(station1, station1, jpg, 1583061003000L, HEURE * 2);// 01 mars 2020

        finEmprunt = ddEmprunternVelos(1580937003000L, 10, 5 * 60000L); // 05 février 2020
        finEmprunt = ddEmprunternVelos(finEmprunt, 51, 10 * 60000L); // 05 février 2019

        // Emprunts en aout 2020 à cheval avec juillet
        emprunterVeloBorne1Pendant(station1, station1, jpg, 1596226203000L, 36 * HEURE); // 31 juillet 2020
    }

    @Test
    public void testImplementeIterable() {
        Ville v = new Ville();
        for (Station s : v) {
            s.capacite();
        }
    }

    @Test
    public void testInitialisationCorrecte() throws IOException {
        Ville v = new Ville();
        v.initialiser(new File(path + "stationsOK.txt"));

        Station s1 = v.getStation("21 - Avenue Fontaine Argent, Boulevard Diderot");
        Station s2 = v.getStation("Avenue du Maréchal Foch");

        assertNotNull(s1);
        assertNotNull(s2);
        assertEquals(12, s1.capacite());
        assertEquals(10, s2.capacite());
        assertNull(s1.veloALaBorne(2));
        assertNull(s1.veloALaBorne(3));
    }

    @Test(expected = IOException.class)
    public void testInitialisationIOException() throws IOException {
        Ville v = new Ville();
        v.initialiser(new File("Cefichiernexistepasdutoutcestsur.txt"));
    }

    @Test
    public void testInitialisationMauvaisFichier() throws IOException {
        Ville v = new Ville();

        v.initialiser(new File(path + "stationsRienNeVa.txt"));

        // La ville ne doit pas avoir de stations
        assertFalse(v.iterator().hasNext());
    }

    @Test
    public void testReinitialiserStationNull() throws IOException {
        Ville v = new Ville();
        v.initialiser(null);

        assertFalse(v.iterator().hasNext());
    }

    @Test
    public void testReinitialiserStationNouvellesStations() throws IOException {
        Ville v = new Ville();
        v.initialiser(new File(path + "stationsOK.txt"));

        assertNotNull(v.getStation("21 - Avenue Fontaine Argent, Boulevard Diderot"));
        assertNotNull(v.getStation("Avenue du Maréchal Foch"));
        assertNull(v.getStation("Gare Viotte"));

        v.initialiser(new File(path + "stationsOK2.txt"));

        assertNull(v.getStation("21 - Avenue Fontaine Argent, Boulevard Diderot"));
        assertNotNull(v.getStation("Avenue du Maréchal Foch"));
        assertNotNull(v.getStation("Gare Viotte"));
    }

    @Test
    public void testReinitialiserVideBienLesStations() throws IOException {
        Ville v = new Ville();
        v.initialiser(new File(path + "stationsOK.txt"));

        assertNotNull(v.getStation("21 - Avenue Fontaine Argent, Boulevard Diderot"));
        assertNotNull(v.getStation("Avenue du Maréchal Foch"));
        assertNull(v.getStation("Gare Viotte"));

        v.initialiser(new File(path + "stationsRienNeVa.txt"));

        // La ville ne doit pas avoir de stations
        assertFalse(v.iterator().hasNext());
    }

    @Test
    public void testSetStationPrincipale() throws IOException {
        Ville v = new Ville();
        v.initialiser(new File(path + "stationsOK.txt"));

        Station s = v.iterator().next();
        assertEquals("21 - Avenue Fontaine Argent, Boulevard Diderot", s.getNom());

        // Tant que cette méthode n'a pas été appelée, la première station du fichier d'initialisation est
        // considérée comme étant la station principale.
        v.setStationPrincipale(v.getStation("Avenue du Maréchal Foch"));

        s = v.iterator().next();
        assertEquals("Avenue du Maréchal Foch", s.getNom());
    }

    @Test
    public void testSetStationPrincipaleNonExistante() {
        Station s = this.besancon.iterator().next();
        assertEquals("Mairie", s.getNom());

        Station bureau410C = new Station("Frédéric Dadeau", LAT_DD, LONG_DD, 2);
        this.besancon.setStationPrincipale(bureau410C);

        assertEquals("Mairie", s.getNom());
    }

    @Test
    public void testGetStationPlusProcheAucuneStation() {
        Ville v = new Ville();
        Station s = v.getStationPlusProche(LAT_DD, LONG_DD);
        assertNull(s);
    }

    @Test
    public void testGetStationPlusProche() {
        Station s = this.besancon.getStationPlusProche(LAT_DD, LONG_DD);
        assertEquals("Xavier Marmier", s.getNom()); // c'est pas tout près...
    }

    @Test
    public void testGetStationPlusProcheMemeStation() {
        final double LAT_BEAUX_ARTS = 47.2409725216023;
        final double LONG_BEAUX_ARTS = 6.025236465176138;
        Station s = this.besancon.getStationPlusProche(LAT_BEAUX_ARTS, LONG_BEAUX_ARTS);
        assertEquals("Beaux Arts", s.getNom());
    }

    @Test
    public void testCreerAbonneNomValide() {
        Ville v = new Ville();

        Abonne a = v.creerAbonne("Fred", RIB_CORRECT);
        Assert.assertEquals("Fred", a.getNom());

        Abonne b = v.creerAbonne(" Baptiste Lé-peria", RIB_CORRECT);
        Assert.assertEquals("Baptiste Lé-peria", b.getNom());

        Abonne c = v.creerAbonne("  Baptiste Lé peria ", RIB_CORRECT);
        Assert.assertEquals("Baptiste Lé peria", c.getNom());
    }

    @Test
    public void testCreerAbonneNomMauvais1() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne(" Fabian  Devel", RIB_CORRECT);
        assertNull(a);
    }

    @Test
    public void testCreerAbonneNomMauvais2() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne(" Fabian--Devel", RIB_CORRECT);
        assertNull(a);
    }

    @Test
    public void testCreerAbonneNomMauvais3() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne(" Fabian- Devel", RIB_CORRECT);
        assertNull(a);
    }

    @Test
    public void testCreerAbonneNomMauvais4() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne(" Fabian -Devel", RIB_CORRECT);
        assertNull(a);
    }

    @Test
    public void testCreerAbonneNomMauvais5() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne(" Fabian Devel 25", RIB_CORRECT);
        assertNull(a);
    }

    @Test
    public void testCreerAbonneNomMauvais6() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne(null, RIB_CORRECT);
        assertNull(a);
    }

    @Test
    public void testCreerAbonneNomMauvais7() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne(" ", RIB_CORRECT);
        assertNull(a);
    }

    @Test
    public void testCreerAbonneNomMauvais8() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne("  F&bian_devel", RIB_CORRECT);
        assertNull(a);
    }

    @Test
    public void testCreerAbonneNomMauvais9() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne("", RIB_CORRECT);
        assertNull(a);
    }

    @Test
    public void testCreerAbonneNomRibMauvais() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne("Fabian--Devel", RIB_CORRECT);
        assertNull(a);
    }

    @Test
    public void testCreerAbonneNomRibMauvais2() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne(" Fabian  Devel", RIB_CORRECT);
        assertNull(a);
    }

    @Test
    public void testCreerAbonneNomRibMauvais3() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne(" Fabian -Devel", RIB_CORRECT);
        assertNull(a);
    }

    @Test
    public void testCreerAbonneNomRibMauvais4() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne(" Fabian- Devel", RIB_CORRECT);
        assertNull(a);
    }

    @Test
    public void testCreerAbonneNomRibNull() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne(null, RIB_CORRECT);
        assertNull(a);
    }

    @Test
    public void testCreerAbonneNomRibVide() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne("", RIB_CORRECT);
        assertNull(a);
    }

    @Test
    public void testCreerAbonneNomRibVideEspace() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne(" ", RIB_CORRECT);
        assertNull(a);
    }

    @Test
    public void testCreerAbonneNomRib() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne("Fabian Devel", RIB_INCORRECT_CLE);
        Assert.assertEquals("Fabian Devel", a.getNom());
        Assert.assertTrue(a.estBloque());
    }

    @Test
    public void testCreerAbonneNomRib2() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne(" Fabian Devel", null);
        Assert.assertEquals("Fabian Devel", a.getNom());
        Assert.assertTrue(a.estBloque());
    }

    @Test
    public void testCreerAbonneNomRibBon() {
        // création d'un nouvel abonné
        Ville v = new Ville();
        Abonne a = v.creerAbonne(" Fabian Devel ", RIB_CORRECT);
        Assert.assertEquals("Fabian Devel", a.getNom());
        Assert.assertFalse(a.estBloque());
    }

    @Test
    public void testIterator() {
        Iterator<Station> it = this.besancon.iterator();
        assertNotNull(it);
        assertTrue(it.hasNext());
        assertEquals(stationMairie, it.next());

        assertTrue(it.hasNext());
        Station huitSeptembre = this.besancon.getStation("8 Septembre");
        assertEquals(huitSeptembre, it.next());
    }


    @Test
    public void testFacturationJanvier() {
        Map<Abonne, Double> fac = this.besancon.facturation(1, 2020);
        assertEquals(2, fac.size());

        // Emprunts en janvier pour dd : 33 * 1h d'un vélo classique
        // O emprunt pour jpg
        double tarif = this.velo.tarif();
        double prix = 10 * tarif * (1 + 0.9 + 0.8) + 3 * tarif * 0.7;

        assertEquals("Facturation de Janvier pour DD", prix, fac.get(dd), DELTA_EURO);
        assertEquals("Aucun emprunt pour jpg", 0.0, fac.get(jpg), DELTA_EURO);
    }

    @Test
    public void testFacturationAucunEmprunt() {
        Map<Abonne, Double> fac = this.besancon.facturation(3, 1492);

        assertEquals(2, fac.size());

        assertEquals(
                "Aucun emprunt terminé dans la période de facturation",
                0,
                fac.get(this.dd),
                DELTA_EURO
        );

        assertEquals(
                "Aucun emprunt terminé dans la période de facturation",
                0,
                fac.get(this.jpg),
                DELTA_EURO
        );
    }

    @Test
    public void testFacturationFevrierBissextile() {
        // Emprunts en février 2020
        Map<Abonne, Double> fac = this.besancon.facturation(2, 2020);
        assertEquals(2, fac.size());

        double tarif = this.velo.tarif();

        double prixDD = 10 * tarif * (1 / 12.0) // Emprunts de 5 minutes plein pot mais qui comptent dans la régression
                + 10 * tarif * (1 / 6.0) * (0.9 + 0.8 + 0.7 + 0.6) // Emprunts qui dégressent en prix
                + 11 * tarif * 1 / 6 * 0.5; // 51ème

        double prixJpg = 3 * tarif * 1;

//        assertEquals("10 * 5min + 51 * 10min pour dd", prixDD, fac.get(dd), DELTA_EURO);
        assertEquals("3 * 1h pour jpg", prixJpg, fac.get(jpg), DELTA_EURO);
    }

    @Test
    public void testFacturationFevrierNonBissextile() {
        // Emprunts en février 2019
        Map<Abonne, Double> fac = this.besancon.facturation(2, 2019);
        assertEquals(2, fac.size());

        double tarif = this.velo.tarif();

        double prixDD = 10 * tarif * (1 / 12.0) // Emprunts de 5 minutes plein pot mais qui comptent dans la régression
                + 10 * tarif * (1 / 6.0) * (0.9 + 0.8 + 0.7 + 0.6) // Emprunts qui dégressent en prix
                + 11 * tarif * 1 / 6 * 0.5; // 51ème

        double prixJpg = 3 * tarif * 1;

//        assertEquals("10 * 5min + 51 * 10min pour dd", prixDD, fac.get(dd), DELTA_EURO);
        assertEquals("3 * 1h pour jpg", prixJpg, fac.get(jpg), DELTA_EURO);
    }

    @Test
    public void testFacturationAChevalSurDeuxMois() {
        Map<Abonne, Double> fac = this.besancon.facturation(7, 2020);
        assertEquals(2, fac.size());

        assertEquals(
                "Aucun emprunt terminé dans la période de facturation",
                0,
                fac.get(this.jpg),
                DELTA_EURO
        );

        assertEquals(
                "Aucun emprunt terminé dans la période de facturation",
                0,
                fac.get(this.dd),
                DELTA_EURO
        );


        fac = this.besancon.facturation(8, 2020);
        assertEquals(2, fac.size());


        double tarif = this.velo.tarif();

        assertEquals(
                "1 * 36h",
                tarif * 36,
                fac.get(this.jpg),
                DELTA_EURO
        );

        assertEquals(
                "Aucun emprunt terminé dans la période de facturation",
                0,
                fac.get(this.dd),
                DELTA_EURO
        );
    }
}






























