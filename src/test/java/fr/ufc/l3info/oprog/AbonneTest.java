package fr.ufc.l3info.oprog;


import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;

/**
 * Test unitaire pour les abonnés.
 */
public class AbonneTest {
    String VALID_RIB = "14568-56487-12345678912-38";
    String INVALID_RIB = "11111-22222-33333333333-44";
    final String RIB_CORRECT = "11111-11111-11111111111-48";
    final String RIB_INCORRECT_CLE = "11111-11111-11111111111-11";
    final String RIB_INCORRECT_FORMAT = "11111-11111-11111111111";

    /*********************** Abonne(string) *************************/
    @Test
    public void testNomValide() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred");
        // vérification de son nom
        Assert.assertEquals("Fred", a.getNom());
        Abonne b = new Abonne(" Baptiste Lé-peria");
        // vérification de son nom
        Assert.assertEquals("Baptiste Lé-peria", b.getNom());
        Abonne c = new Abonne("  Baptiste Lé peria ");
        // vérification de son nom
        Assert.assertEquals("Baptiste Lé peria", c.getNom());
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomMauvais1() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne(" Fabian  Devel");
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomMauvais2() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne(" Fabian--Devel");
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomMauvais3() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne(" Fabian- Devel");
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomMauvais4() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne(" Fabian -Devel");
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomMauvais5() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne(" Fabian Devel 25");
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomMauvais6() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne(null);
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomMauvais7() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne(" ");
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomMauvais8() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("  F&bian_devel");
    }
    @Test(expected = IncorrectNameException.class)
    public void testNomMauvais9() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("");
    }

    /*********************** Abonne(string, string) *************************/
    @Test(expected = IncorrectNameException.class)
    public void testNomRibMauvais() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fabian--Devel", VALID_RIB);
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomRibMauvais2() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne(" Fabian  Devel", VALID_RIB);
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomRibMauvais3() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne(" Fabian -Devel", VALID_RIB);
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomRibMauvais4() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne(" Fabian- Devel", VALID_RIB);
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomRibNull() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne(null, VALID_RIB);
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomRibVide() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("", VALID_RIB);
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomRibVideEspace() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne(" ", VALID_RIB);
    }

    @Test
    public void testNomRib() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fabian Devel", INVALID_RIB);
        Assert.assertEquals("Fabian Devel", a.getNom());
        Assert.assertTrue(a.estBloque());
    }

    @Test
    public void testNomRib2() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne(" Fabian Devel", null);
        Assert.assertEquals("Fabian Devel", a.getNom());
        Assert.assertTrue(a.estBloque());
    }

    @Test
    public void testNomRibBon() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne(" Fabian Devel ", VALID_RIB);
        Assert.assertEquals("Fabian Devel", a.getNom());
        Assert.assertFalse(a.estBloque());
    }

    /******************************** getId() ******************************/

    @Test
    public void testGetId() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred");
        int id = a.getID();
        Abonne b = new Abonne(" Baptiste Lé-peria");
        Assert.assertEquals(id+1, b.getID());
        Abonne c = new Abonne(" Baptiste Lé peria ");
        Assert.assertEquals(id+2, c.getID());
    }

    @Test
    public void testGetIdWithRib() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred", VALID_RIB);
        int id = a.getID();
        Abonne b = new Abonne(" Baptiste Lé-peria", INVALID_RIB);
        Assert.assertEquals(id+1, b.getID());
        Abonne c = new Abonne(" Baptiste Lé peria ");
        Assert.assertEquals(id+2, c.getID());
    }

    /****************************** miseAJourRib() ***************************/
    @Test
    public void miseAJourRib() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred");
        Assert.assertTrue(a.estBloque());
        a.miseAJourRIB("rib invalide");
        Assert.assertTrue(a.estBloque());
        a.miseAJourRIB(null);
        Assert.assertTrue(a.estBloque());
        a.bloquer();
        Assert.assertTrue(a.estBloque());
        a.debloquer();
        Assert.assertTrue(a.estBloque()); // Impossibilité de débloquer un utilisateur sans rib correct
        a.miseAJourRIB(VALID_RIB);
        Assert.assertFalse(a.estBloque());
        a.miseAJourRIB(INVALID_RIB);
        Assert.assertFalse(a.estBloque());
        a.bloquer();
        Assert.assertTrue(a.estBloque());
        a.miseAJourRIB(VALID_RIB);
        /* Mise à jour du rib ne débloque pas l'utilisateur s'il a été bloqué volontairement*/
        Assert.assertTrue(a.estBloque());
        a.debloquer();
        Assert.assertFalse(a.estBloque());
    }

    @Test
    public void miseAJourRib2() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred", VALID_RIB);
        Assert.assertFalse(a.estBloque());
        a.debloquer();
        Assert.assertFalse(a.estBloque());
        a.miseAJourRIB(VALID_RIB);
        Assert.assertFalse(a.estBloque());
        a.miseAJourRIB(INVALID_RIB);
        Assert.assertFalse(a.estBloque());
    }

    @Test
    public void miseAJourRib3() throws IncorrectNameException {
        // création d'un nouvel abonné
        Abonne a = new Abonne("Fred", INVALID_RIB);
        Assert.assertTrue(a.estBloque());
        a.bloquer();
        Assert.assertTrue(a.estBloque());
        a.debloquer();
        Assert.assertTrue(a.estBloque());
        a.miseAJourRIB(VALID_RIB);
        Assert.assertFalse(a.estBloque());
    }
    /************************ equals() ************************/

    @Test
    public void testReflexivite() throws IncorrectNameException {
        Abonne a = new Abonne("Fred");
        Assert.assertFalse(a.equals("b"));
        Assert.assertFalse(a.equals("Fred"));
        Assert.assertTrue(a.equals(a));
        Assert.assertEquals(a.hashCode(), a.hashCode());
    }

    @Test
    public void testEgalite() throws IncorrectNameException {
        Abonne a = new Abonne("Fred");
        Abonne b = a;
        Assert.assertFalse(b.equals("b"));
        Assert.assertFalse(b.equals("Fred"));
        Assert.assertTrue(b.equals(a));
        Assert.assertEquals(a.hashCode(), b.hashCode());
    }

    @Test
    public void testEquals2() throws IncorrectNameException {
        Abonne a = new Abonne("Fred");
        Abonne b = new Abonne("Dadeau");
        Assert.assertFalse(a.equals(b));
        Assert.assertNotEquals(a.hashCode(), b.hashCode());
        Assert.assertFalse(a.equals(null));
    }

    @Test
    public void testEqualsWithRib() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", VALID_RIB);
        Abonne b = new Abonne("Dadeau", VALID_RIB);
        Assert.assertFalse(a.equals(b));
        Assert.assertNotEquals(a.hashCode(), b.hashCode());
    }

    @Test
    public void testTransitivite() throws IncorrectNameException {
        Abonne a = new Abonne("Fred");
        Abonne b = a;
        Abonne c = b;
        Assert.assertFalse(c.equals("b"));
        Assert.assertFalse(c.equals("Fred"));
        Assert.assertTrue(c.equals(a));
        Assert.assertEquals(c.hashCode(), a.hashCode());
        Assert.assertEquals(c.hashCode(), a.getID());
    }

    @Test
    public void testEqualsFaux() throws IncorrectNameException {
        Abonne a = new Abonne("Fred");
        Assert.assertTrue(a.hashCode() == a.getID());
        Abonne b = new Abonne("Fred");
        Assert.assertFalse(b.hashCode() == a.hashCode());
        Assert.assertFalse(a.equals(b));
    }

    @Test
    public void testNom() throws IncorrectNameException {
        Abonne a = new Abonne("Fred");
        Assert.assertEquals("Fred", a.getNom());
    }

    @Test
    public void testNomAvecEspace() throws IncorrectNameException {
        Abonne a = new Abonne("Fred Dd");
        Assert.assertEquals("Fred Dd", a.getNom());
    }

    @Test
    public void testNomAvecTiret() throws IncorrectNameException {
        Abonne a = new Abonne("Fred-Dd");
        Assert.assertEquals("Fred-Dd", a.getNom());
    }

    @Test
    public void testNomTrim() throws IncorrectNameException {
        Abonne a = new Abonne("  Fred Dd  ");
        Assert.assertEquals("Fred Dd", a.getNom());
    }

    @Test
    public void testNomAvecAccent() throws IncorrectNameException {
        Abonne a = new Abonne("Frédéric-Dadeau");
        Assert.assertEquals("Frédéric-Dadeau", a.getNom());
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomNull() throws IncorrectNameException {
        Abonne a = new Abonne(null);
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomVide() throws IncorrectNameException {
        Abonne a = new Abonne("  ");
    }


    @Test(expected = IncorrectNameException.class)
    public void testNomIncorrectTiretEspace() throws IncorrectNameException {
        Abonne a = new Abonne("Fred- Dd");
        Abonne b = new Abonne("Fred -Dd");
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomIncorrectDoubleEspace() throws IncorrectNameException {
        Abonne a = new Abonne("Fred  Dd");
    }

    @Test(expected = IncorrectNameException.class)
    public void testNomIncorrectTiret() throws IncorrectNameException {
        Abonne a = new Abonne("Fred--Dd");
    }


    @Test(expected = IncorrectNameException.class)
    public void testNomChiffre() throws IncorrectNameException {
        Abonne a = new Abonne("FredDd2");
    }

    @Test
    public void testIncrementationID() throws IncorrectNameException {
        Abonne a = new Abonne("Fred");
        Abonne b = new Abonne("Julien");
        Abonne c = new Abonne("Fred");

        Abonne aCopie;
        aCopie = a;

        Assert.assertNotEquals(a.getID(), b.getID());
        Assert.assertNotEquals(a.getID(), c.getID());
        Assert.assertNotEquals(b.getID(), c.getID());
        Assert.assertNotEquals(b.getID(), aCopie.getID());
        Assert.assertNotEquals(c.getID(), aCopie.getID());

        Assert.assertEquals(a.getID(), aCopie.getID());

        Assert.assertTrue("Incrémentation des ids", a.getID() < b.getID());
        Assert.assertTrue("Incrémentation des ids", b.getID() < c.getID());
    }

    @Test
    public void testMiseAJourRIBCorrect() throws IncorrectNameException {
        Abonne a = new Abonne("Fred");

        Assert.assertTrue(a.estBloque());
        a.debloquer();
        Assert.assertTrue(a.estBloque());

        a.miseAJourRIB(this.RIB_CORRECT);
        Assert.assertFalse(a.estBloque());
    }

    @Test
    public void testMiseAJourRIBIncorrect() throws IncorrectNameException {
        Abonne a = new Abonne("Fred");

        Assert.assertTrue(a.estBloque());
        a.debloquer();
        Assert.assertTrue(a.estBloque());

        a.miseAJourRIB(this.RIB_INCORRECT_CLE);
        Assert.assertTrue(a.estBloque());

        a.miseAJourRIB(this.RIB_INCORRECT_FORMAT);
        Assert.assertTrue(a.estBloque());

        a.miseAJourRIB(this.RIB_CORRECT);
        Assert.assertFalse(a.estBloque());
    }

    @Test
    public void testRIBConstructeurCorrect() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", this.RIB_CORRECT);
        Assert.assertFalse(a.estBloque());
    }

    @Test
    public void testRIBConstructeurIncorrect() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", this.RIB_INCORRECT_CLE);
        Assert.assertTrue(a.estBloque());
        a.miseAJourRIB(this.RIB_CORRECT);
        Assert.assertFalse(a.estBloque());
    }

    @Test
    public void testRIBnull() throws IncorrectNameException {
        Abonne a = new Abonne("Fred");
        Assert.assertTrue(a.estBloque());
        a.miseAJourRIB(null);
        Assert.assertTrue(a.estBloque());
    }

    @Test
    public void testEqualsVrai() throws IncorrectNameException {
        Abonne a = new Abonne("Fred");
        Abonne b;
        b = a;
        Assert.assertEquals(a, b);
        Assert.assertEquals(b, a);
    }

    @Test
    public void testEqualsFauxNath() throws IncorrectNameException {
        Abonne a = new Abonne("Fred");
        Abonne b = new Abonne("Fred");
        Assert.assertNotEquals(a, b);
        Assert.assertNotEquals(b, a);
        Assert.assertNotEquals(a, "1");
        Assert.assertNotEquals(a, 1);
        Assert.assertNotEquals(a, null);
        Assert.assertNotEquals(b, null);
    }

    @Test
    public void testEstBloque() throws IncorrectNameException {
        Abonne a = new Abonne("Fred", this.RIB_CORRECT);
        Assert.assertFalse(a.estBloque());

        a.bloquer();
        Assert.assertTrue(a.estBloque());

        a.debloquer();
        Assert.assertFalse(a.estBloque());
    }

    @Test
    public void testHashCode() throws IncorrectNameException {
        HashSet<Abonne> testHashSet = new HashSet<>();

        Abonne abonne = new Abonne("Fred");
        testHashSet.add(abonne);
        for (int i = 2; i < 1000; i++) {
            Abonne nouveauAbonne = new Abonne("Nom" + (char) ('A' + i % 25));

            Assert.assertNotEquals(abonne.hashCode(), nouveauAbonne.hashCode());

            Assert.assertFalse(testHashSet.contains(nouveauAbonne));

            testHashSet.add(nouveauAbonne);
            Assert.assertEquals(testHashSet.size(), i);
            testHashSet.add(nouveauAbonne);
            Assert.assertEquals(testHashSet.size(), i);

            Abonne copie;
            copie = nouveauAbonne;
            Assert.assertTrue(testHashSet.contains(copie));
        }
    }

}
