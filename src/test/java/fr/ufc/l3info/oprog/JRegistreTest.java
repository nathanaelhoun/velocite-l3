package fr.ufc.l3info.oprog;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Test d'intégration pour la classe JRegistre.
 */
public class JRegistreTest {
    private Abonne a;
    private Abonne ab;
    private JRegistre r;
    private IVelo velo;
    private Station station;
    private int NB_BORNE;
    private long HEURE_ET_DEMIE = 5400000;
    private long HEURE_PAS_COMPLETE = 3000000;

    final long TEMPS_DEBUT_EMPRUNT = 1603354000000L;
    final long TEMPS_FIN_D_EMPRUNT = 1603355000000L;
    final long TEMPS_INTERMEDIAIRE = 1603354486152L;

    final double DELTA_EURO = 0.001;

    @Before
    public void beforeFunc() throws IncorrectNameException {
        r = new JRegistre();
        a = new Abonne("Fab");
        ab = new Abonne("Fab");
        velo = new Velo();
        NB_BORNE = 35;
        station = new Station("Hauts de Chazal", 47.221563, 5.951402, NB_BORNE);
        station.setRegistre(r);
        for (int i = 1; i <= NB_BORNE; i++) {
            station.arrimerVelo(new Velo(), i);
        }
    }

    @After
    public void cleanUp() {
        station = null;
        a = null;
        ab = null;
        velo = null;
        r = null;
    }

    @Test
    public void constructeurTest(){
        JRegistre reg = new JRegistre();
        Assert.assertNotNull(reg);
        Assert.assertEquals(0, reg.nbEmpruntsEnCours(a));
    }

    @Test
    public void emprunterTestBasic(){
        Assert.assertEquals(0, r.emprunter(a, velo, station.maintenant()));
    }

    @Test
    public void emprunterTestDejaEmprunte(){
        Assert.assertEquals(0, r.emprunter(a, velo, station.maintenant()));
        Assert.assertEquals(-2, r.emprunter(a, velo, station.maintenant()));
    }

    @Test
    public void emprunterTestNull(){
        Assert.assertEquals(-1, r.emprunter(a, null, station.maintenant()));
        Assert.assertEquals(-1, r.emprunter(null, velo, station.maintenant()));
    }

    @Test
    public void emprunterTestPlusieursVelo(){
        for (int i = 1; i <= NB_BORNE; i++) {
            Assert.assertEquals(0, r.emprunter(a, station.veloALaBorne(i), station.maintenant()));
        }
        Assert.assertEquals(0, r.emprunter(a, velo, station.maintenant()));
    }

    @Test
    public void emprunterTestDansLePasse(){
        Assert.assertEquals(0, r.emprunter(a, velo, 0));
        Assert.assertEquals(-2, r.emprunter(a, velo, station.maintenant() - 10));
    }

    @Test
    public void emprunterTestDansLeFutur(){
        Assert.assertEquals(0, r.emprunter(a, velo, station.maintenant() + 10000000));
    }

    @Test
    public void emprunterTestChevauchementActuel() {
        r.emprunter(a, velo, station.maintenant());
        r.retourner(velo, station.maintenant() + 100);
        Assert.assertEquals(-2, r.emprunter(a, velo, station.maintenant() + 50));
    }

    @Test
    public void emprunterTestChevauchementPasse() {
        long start = station.maintenant() - 100;
        r.emprunter(a, velo, start);
        r.retourner(velo, start + 5);
        Assert.assertEquals(-2, r.emprunter(a, velo, start + 2));
    }

    @Test
    public void emprunterTestChevauchementFutur() {
        long start = station.maintenant();
        r.emprunter(a, velo, start + 100);
        r.retourner(velo, start + 150);
        Assert.assertEquals(-2, r.emprunter(a, velo, start + 120));
    }

    @Test
    public void retournerTest() {
        r.emprunter(a, velo, station.maintenant());
        Assert.assertEquals(0, r.retourner(velo, station.maintenant() + 10));
    }

    @Test
    public void retournerTestVeloNull() {
        r.emprunter(a, velo, station.maintenant());
        Assert.assertEquals(-1, r.retourner(null, station.maintenant()));
    }

    @Test
    public void retournerTestVeloPasEmprunte() {
        Assert.assertEquals(-2, r.retourner(velo, station.maintenant()));
    }

    @Test
    public void retournerTestDansPasse() {
        r.emprunter(a, velo, station.maintenant());
        Assert.assertEquals(-3, r.retourner(velo, station.maintenant() - 2));
    }

    @Test
    public void retournerTestChevauchement1() {
        long start = station.maintenant();
        r.emprunter(a, velo, start + 10);
        r.retourner(velo, start + 100);
        r.emprunter(a, velo, start);
        Assert.assertEquals(-3, r.retourner(velo, start + 20));
        Assert.assertEquals(0, r.retourner(velo, start + 5));
    }

    @Test
    public void retournerTestChevauchement2() {
        long start = station.maintenant();
        r.emprunter(a, velo, start + 10);
        r.retourner(velo, start + 100);
        r.emprunter(a, velo, start);
        Assert.assertEquals(-3, r.retourner(velo, start + 20));
        Assert.assertEquals(0, r.retourner(velo, start + 105));
    }

    @Test
    public void retournerTestPlusieursEmprunts(){
        r.emprunter(a, velo, station.maintenant() + 100);
        Assert.assertEquals(-2, r.emprunter(a, velo, station.maintenant()));
        Assert.assertEquals(-2, r.emprunter(a, velo, station.maintenant() - 100));
        Assert.assertEquals(-3, r.retourner(velo, station.maintenant()));
        Assert.assertEquals(-3, r.retourner(velo, station.maintenant() + 50));
        Assert.assertEquals(0, r.retourner(velo, station.maintenant() + 110));
    }

    @Test
    public void retournerTestCompose(){
        r.emprunter(a, velo, station.maintenant());
        Assert.assertEquals(-3, r.retourner(velo, station.maintenant() - 10));
        Assert.assertEquals(0, r.retourner(velo, station.maintenant() + 10));
        Assert.assertEquals(-2, r.retourner(velo, station.maintenant()));;
    }

    @Test
    public void retournerTestPlusieursVelo() {
        IVelo v = new Velo();
        r.emprunter(a, velo, station.maintenant());
        Assert.assertEquals(0, r.emprunter(a, v, station.maintenant()));
        Assert.assertEquals(0, r.retourner(velo, station.maintenant() + 1));
        Assert.assertEquals(-2, r.retourner(velo, station.maintenant() + 10));
        Assert.assertEquals(0, r.retourner(v, station.maintenant()));
    }


    @Test
    public void retournerTestPlusieursAbonne() {
        r.emprunter(a, velo, station.maintenant());
        Assert.assertEquals(-2, r.emprunter(ab, velo, station.maintenant() + 10));
        Assert.assertEquals(0, r.retourner(velo, station.maintenant() + 1));
        Assert.assertEquals(0, r.emprunter(ab, velo, station.maintenant() + 10));
        Assert.assertEquals(-2, r.emprunter(a, velo, station.maintenant()));
        Assert.assertEquals(-3, r.retourner(velo, station.maintenant()));
        Assert.assertEquals(0, r.retourner(velo, station.maintenant() + 10));
    }

    @Test
    public void retournerTestCasProblematique() {
        long start = station.maintenant();
        r.emprunter(a, velo, start);
        Assert.assertEquals(0, r.retourner(velo, start + 100));
        Assert.assertEquals(0, r.emprunter(a, velo, start - 20));
        Assert.assertEquals(-3, r.retourner(velo, start + 10));
        Assert.assertEquals(-2, r.emprunter(a, velo, start + 10));
        Assert.assertEquals(-2, r.emprunter(a, velo, start + 110));
        Assert.assertEquals(0, r.retourner(velo, start - 1));
        Assert.assertEquals(-2, r.emprunter(a, velo, start + 10));
        Assert.assertEquals(0, r.emprunter(a, velo, start + 110));
        Assert.assertEquals(0, r.retourner(velo, start + 120));
    }

    @Test
    public void nbEmpruntsEnCoursTestPlusieursAbonne() {
        Assert.assertEquals(0, r.nbEmpruntsEnCours(a));
        Assert.assertEquals(0, r.nbEmpruntsEnCours(ab));
        r.emprunter(a, velo, station.maintenant());
        Assert.assertEquals(1, r.nbEmpruntsEnCours(a));
        r.emprunter(a, velo, station.maintenant());
        r.emprunter(ab, velo, station.maintenant());
        Assert.assertEquals(1, r.nbEmpruntsEnCours(a));
        Assert.assertEquals(0, r.nbEmpruntsEnCours(ab));
        r.retourner(velo, station.maintenant());
        r.emprunter(ab, velo, station.maintenant() + 10);
        Assert.assertEquals(0, r.nbEmpruntsEnCours(a));
        Assert.assertEquals(1, r.nbEmpruntsEnCours(ab));
    }

    @Test
    public void nbEmpruntsEnCoursTestPlusieursVelo() {
        Assert.assertEquals(0, r.nbEmpruntsEnCours(a));
        for (int i = 1; i <= NB_BORNE; i++) {
            r.emprunter(a, station.veloALaBorne(i), station.maintenant());
        }
        Assert.assertEquals(NB_BORNE, r.nbEmpruntsEnCours(a));
        r.retourner(station.veloALaBorne(1), station.maintenant()-10);
        Assert.assertEquals(NB_BORNE, r.nbEmpruntsEnCours(a));
        for (int i = NB_BORNE; i >= 1; i--) {
            r.retourner(station.veloALaBorne(i), station.maintenant());
        }
        Assert.assertEquals(0, r.nbEmpruntsEnCours(a));
    }

    @Test
    public void facturationTestNegligeable(){
        Assert.assertEquals(0, r.facturation(a, station.maintenant(), station.maintenant() + 20), 0.01);
        r.emprunter(a, station.veloALaBorne(1), station.maintenant());
        r.retourner(station.veloALaBorne(1), station.maintenant() + 2);
        Assert.assertEquals(0, r.facturation(a, station.maintenant(), station.maintenant() + 20), 0.01);
    }

    @Test
    public void facturationTestNull(){
        Assert.assertEquals(0, r.facturation(null, station.maintenant(), station.maintenant() + 20), 0.01);
    }

    @Test
    public void facturationTestInvalide(){
        r.emprunter(a, station.veloALaBorne(1), station.maintenant());
        r.retourner(station.veloALaBorne(1), station.maintenant() + HEURE_ET_DEMIE);
        Assert.assertEquals(0, r.facturation(a, station.maintenant(), station.maintenant() - 20), 0.01);
    }

    @Test
    public void facturationTestPlusieursEmprunts(){
        IVelo v = new Velo();
        v = new OptCadreAlu(v);
        IVelo v2 = new Velo();
        v2 = new OptCadreAlu(v2);
        v2 = new OptSuspensionArriere(v2);
        long start = station.maintenant();
        r.emprunter(a, station.veloALaBorne(1), start);
        r.emprunter(a, v2, start);
        r.emprunter(a, v, start + HEURE_PAS_COMPLETE);
        r.retourner(station.veloALaBorne(1), start + HEURE_ET_DEMIE);
        r.retourner(v, start + 2 * HEURE_PAS_COMPLETE);
        r.retourner(v2, start + 3600000);
        Assert.assertEquals(2 * 1.5 + 2.2 * 5 / 6.0 + 2.7, r.facturation(a, start,
                start + 2*HEURE_ET_DEMIE),
                0.01);
    }

    @Test
    public void facturationTestEmpruntsGlisse(){
        IVelo v = new Velo();
        v = new OptCadreAlu(v);
        long start = station.maintenant();
        r.emprunter(a, station.veloALaBorne(1), start);
        r.emprunter(a, v, start);
        Assert.assertEquals(0, r.facturation(a, start,
                start + HEURE_ET_DEMIE), 0.01);
        r.retourner(station.veloALaBorne(1), start + HEURE_ET_DEMIE);
        r.retourner(v, start + 2 * HEURE_PAS_COMPLETE);
        Assert.assertEquals(2 * 1.5, r.facturation(a, start,
                start + HEURE_ET_DEMIE + 100), 0.01);
    }

    @Test
    public void testEmprunter() {
        Assert.assertEquals(0, this.r.emprunter(this.a, this.velo, System.currentTimeMillis()));
    }

    @Test
    public void testEmprunterParametreIncorrect() {
        Assert.assertEquals(-1, this.r.emprunter(null, this.velo, System.currentTimeMillis()));
        Assert.assertEquals(-1, this.r.emprunter(this.a, null, System.currentTimeMillis()));
        Assert.assertEquals(-1, this.r.emprunter(null, null, System.currentTimeMillis()));
    }

    @Test
    public void testEmprunterDejaEmprunte() {
        Assert.assertEquals(0, this.r.emprunter(this.a, this.velo, System.currentTimeMillis()));
        Assert.assertEquals(-2, this.r.emprunter(this.a, this.velo, System.currentTimeMillis()));
    }

    @Test
    public void testEmprunterDejaEmprunteParQuelquunDautre() throws IncorrectNameException {
        Assert.assertEquals(0, this.r.emprunter(this.a, this.velo, System.currentTimeMillis()));
        Assert.assertEquals(-2, this.r.emprunter(new Abonne("Julien"), this.velo, System.currentTimeMillis()));
    }

    @Test
    public void testEmprunterDejaEmprunteDansLePasse() throws IncorrectNameException {
        Assert.assertEquals(0, this.r.emprunter(this.a, this.velo, this.TEMPS_DEBUT_EMPRUNT));
        Assert.assertEquals(0, this.r.retourner(this.velo, this.TEMPS_FIN_D_EMPRUNT));
        Assert.assertEquals(-2, this.r.emprunter(new Abonne("Julien"), this.velo, this.TEMPS_INTERMEDIAIRE));
    }

    @Test
    public void testRetourner() {
        Assert.assertEquals(0, this.r.emprunter(this.a, this.velo, this.TEMPS_DEBUT_EMPRUNT));

        Assert.assertEquals(0, this.r.retourner(this.velo, this.TEMPS_FIN_D_EMPRUNT));
    }

    @Test
    public void testRetournerNull() {
        Assert.assertEquals(-1, this.r.retourner(null, System.currentTimeMillis()));
    }

    @Test
    public void testRetournerNonEmprunte() throws IncorrectNameException {
        Assert.assertEquals(-2, this.r.retourner(this.velo, System.currentTimeMillis()));

        Assert.assertEquals(0, this.r.emprunter(new Abonne("Redf"), new Velo(), System.currentTimeMillis()));
        Assert.assertEquals(0, this.r.emprunter(new Abonne("Dref"), new Velo(), System.currentTimeMillis()));

        Assert.assertEquals(-2, this.r.retourner(this.velo, System.currentTimeMillis()));
    }

    @Test
    public void testRetournerDateRetourAnterieure() {
        Assert.assertEquals(0, this.r.emprunter(this.a, this.velo, this.TEMPS_FIN_D_EMPRUNT));

        Assert.assertEquals(-3, this.r.retourner(this.velo, this.TEMPS_DEBUT_EMPRUNT));
    }

    @Test
    public void testRetournerDateRetourDansAutreEmprunt() {
        Assert.assertEquals(0, this.r.emprunter(this.a, this.velo, this.TEMPS_DEBUT_EMPRUNT));
        Assert.assertEquals(0, this.r.retourner(this.velo, this.TEMPS_FIN_D_EMPRUNT));
        Assert.assertEquals(0, this.r.emprunter(this.a, this.velo, this.TEMPS_DEBUT_EMPRUNT - 100));

        Assert.assertEquals(-3, this.r.retourner(this.velo, this.TEMPS_INTERMEDIAIRE));
    }

    /**
     * On ajoute des vélos pour vérifier le nombre d'emprunt. Certains ajouts doivent rater, et le nombre d'emprunt
     * ne doit alors pas changer.
     */
    @Test
    public void testNbEmpruntsEnCours() {
        Velo velo1 = new Velo();
        Velo velo2 = new Velo();
        Velo velo3 = new Velo();

        Assert.assertEquals(0, this.r.nbEmpruntsEnCours(this.a));

        Assert.assertEquals(0, this.r.emprunter(this.a, velo1, this.TEMPS_DEBUT_EMPRUNT));
        Assert.assertEquals(1, this.r.nbEmpruntsEnCours(this.a));


        Assert.assertEquals(0, this.r.emprunter(this.a, velo2, System.currentTimeMillis()));
        Assert.assertEquals(2, this.r.nbEmpruntsEnCours(this.a));

        Assert.assertEquals(-2, this.r.emprunter(this.a, velo1, this.TEMPS_DEBUT_EMPRUNT));
        Assert.assertEquals(2, this.r.nbEmpruntsEnCours(this.a));


        Assert.assertEquals(0, this.r.emprunter(this.a, velo3, this.TEMPS_INTERMEDIAIRE));
        Assert.assertEquals(3, this.r.nbEmpruntsEnCours(this.a));

        Assert.assertEquals(-1, this.r.emprunter(this.a, null, this.TEMPS_INTERMEDIAIRE));
        Assert.assertEquals(3, this.r.nbEmpruntsEnCours(this.a));

        Assert.assertEquals(0, this.r.emprunter(this.a, this.velo, this.TEMPS_DEBUT_EMPRUNT));
        Assert.assertEquals(4, this.r.nbEmpruntsEnCours(this.a));
    }

    @Test
    public void testFacturationNull() {
        Assert.assertEquals(
                0.0,
                this.r.facturation(
                        null,
                        this.TEMPS_DEBUT_EMPRUNT,
                        this.TEMPS_FIN_D_EMPRUNT
                ),
                this.DELTA_EURO
        );
    }

    @Test
    public void testFacturationDateIncorrectes() {
        Assert.assertEquals(
                0.0,
                this.r.facturation(
                        this.a,
                        this.TEMPS_FIN_D_EMPRUNT,
                        this.TEMPS_DEBUT_EMPRUNT
                ),
                this.DELTA_EURO
        );
    }

    @Test
    public void testFacturation() throws ParseException {
        final DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        final double TARIF_VELO_HORAIRE = this.velo.tarif();

        final long UNE_HEURE_DEBUT = df.parse("22/10/2020 10:10:00").getTime();
        final long UNE_HEURE_FIN = df.parse("22/10/2020 11:10:00").getTime();

        final long UN_JOUR_DEBUT = df.parse("23/10/2020 10:10:00").getTime();
        final long UN_JOUR_FIN = df.parse("24/10/2020 10:10:00").getTime();

        assert (0 == this.r.emprunter(this.a, this.velo, UNE_HEURE_DEBUT));
        assert (0 == this.r.retourner(this.velo, UNE_HEURE_FIN));

        // Une heure
        Assert.assertEquals(
                TARIF_VELO_HORAIRE,
                this.r.facturation(this.a, UNE_HEURE_DEBUT, UNE_HEURE_FIN),
                DELTA_EURO
        );

        Assert.assertEquals(0, this.r.emprunter(this.a, this.velo, UN_JOUR_DEBUT));
        Assert.assertEquals(0, this.r.retourner(this.velo, UN_JOUR_FIN));

        // La journée
        Assert.assertEquals(
                24 * TARIF_VELO_HORAIRE,
                this.r.facturation(this.a, UN_JOUR_DEBUT, UN_JOUR_FIN),
                DELTA_EURO
        );

        // Tout le mois
        Assert.assertEquals(
                24 * TARIF_VELO_HORAIRE + TARIF_VELO_HORAIRE,
                this.r.facturation(
                        this.a,
                        df.parse("01/10/2020 00:00:00").getTime(),
                        df.parse("31/10/2020 00:00:00").getTime()
                ),
                DELTA_EURO
        );

        // Pendant la journée mais journée pas finie
        Assert.assertEquals(
                TARIF_VELO_HORAIRE,
                this.r.facturation(
                        this.a,
                        df.parse("01/10/2020 00:00:00").getTime(),
                        UN_JOUR_FIN - 100
                ),
                DELTA_EURO
        );
    }

    @Test
    public void testFacturationDemieHeure() throws ParseException {
        final DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        assert (0 == this.r.emprunter(this.a, this.velo, df.parse("01/10/2020 12:59:00").getTime()));
        assert (0 == this.r.retourner(this.velo, df.parse("01/10/2020 13:29:00").getTime()));

        Assert.assertEquals(
                this.velo.tarif() / 2,
                this.r.facturation(
                        this.a,
                        df.parse("01/10/2020 00:00:00").getTime(),
                        df.parse("31/10/2020 00:00:00").getTime()
                ),
                this.DELTA_EURO
        );
    }

    @Test
    public void testFacturationUneMinute() throws ParseException {
        final DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        assert (0 == this.r.emprunter(this.a, this.velo, df.parse("01/10/2020 12:59:00").getTime()));
        assert (0 == this.r.retourner(this.velo, df.parse("01/10/2020 13:00:00").getTime()));

        Assert.assertEquals(
                this.velo.tarif() / 60,
                this.r.facturation(
                        this.a,
                        df.parse("01/10/2020 00:00:00").getTime(),
                        df.parse("31/10/2020 00:00:00").getTime()
                ),
                this.DELTA_EURO
        );
    }

    @Test
    public void testFacturationUneSeconde() throws ParseException {
        final DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        assert (0 == this.r.emprunter(this.a, this.velo, df.parse("01/10/2020 12:00:00").getTime()));
        assert (0 == this.r.retourner(this.velo, df.parse("01/10/2020 12:00:01").getTime()));

        Assert.assertEquals(
                this.velo.tarif() / 60 / 60,
                this.r.facturation(
                        this.a,
                        df.parse("01/10/2020 00:00:00").getTime(),
                        df.parse("31/10/2020 00:00:00").getTime()
                ),
                this.DELTA_EURO
        );
    }

    @Test
    public void testFacturationUneMilliSeconde() throws ParseException {
        final DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss:SSS");

        assert (0 == this.r.emprunter(this.a, this.velo, df.parse("01/10/2020 12:00:00:000").getTime()));
        assert (0 == this.r.retourner(this.velo, df.parse("01/10/2020 12:00:00:001").getTime()));

        Assert.assertEquals(
                this.velo.tarif() / 60 / 60 / 1000,
                this.r.facturation(
                        this.a,
                        df.parse("01/10/2020 00:00:00:000").getTime(),
                        df.parse("31/10/2020 00:00:00:000").getTime()
                ),
                this.DELTA_EURO
        );
    }
}
