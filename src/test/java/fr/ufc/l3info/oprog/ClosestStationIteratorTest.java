package fr.ufc.l3info.oprog;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;

public class ClosestStationIteratorTest {

    private Station stationPrincipale;
    private Set<Station> stationSet;

    @Before
    public void setUp() {
        stationSet = new HashSet<>();
        stationPrincipale = new Station("Gare Viotte", 47.24650155142733, 6.022715427111734, 10);
        stationSet.add(stationPrincipale);
        stationSet.add(new Station("Hauts de Chazal", 47.221563, 5.951402, 10));
        stationSet.add(new Station("Saint Jacques", 47.234970856296655, 6.020961226259578, 10));
    }

    @Test
    public void constructorTestNull(){
        ClosestStationIterator it = new ClosestStationIterator(null, null);
        Assert.assertNotNull(it);
        Assert.assertFalse(it.hasNext());
    }

    @Test
    public void constructorTestVide(){
        ClosestStationIterator it = new ClosestStationIterator(new HashSet<>(), stationPrincipale);
        Assert.assertNotNull(it);
        Assert.assertFalse(it.hasNext());
    }

    @Test
    public void constructorTestPasDeStationPrincipale(){
        ClosestStationIterator it = new ClosestStationIterator(stationSet, null);
        Assert.assertNotNull(it);
        Assert.assertFalse(it.hasNext());
    }

    @Test
    public void constructorTest(){
        ClosestStationIterator it = new ClosestStationIterator(stationSet, stationPrincipale);
        Assert.assertNotNull(it);
        Assert.assertTrue(it.hasNext());
    }

    @Test
    public void hasNextVideSansStation(){
        ClosestStationIterator it = new ClosestStationIterator(new HashSet<>(), null);
        Assert.assertNotNull(it);
        Assert.assertFalse(it.hasNext());
    }

    @Test
    public void nextTest() {
        ClosestStationIterator it = new ClosestStationIterator(stationSet, stationPrincipale);
        Station s = it.next();
        Assert.assertNotNull(s);
        Assert.assertEquals("Gare Viotte", s.getNom());
    }

    @Test (expected = NoSuchElementException.class)
    public void nextTestNull() {
        ClosestStationIterator it = new ClosestStationIterator(new HashSet<>(), null);
        Station s = it.next();
    }

    @Test (expected = NoSuchElementException.class)
    public void nextTestPasDeStationPrincipale() {
        ClosestStationIterator it = new ClosestStationIterator(stationSet, null);
        Station s = it.next();
    }

    @Test
    public void removeTest() {
        ClosestStationIterator it = new ClosestStationIterator(stationSet, stationPrincipale);
        Station s = it.next();
        int size = stationSet.size();
        it.remove();
        Assert.assertNotNull(s);
        Assert.assertEquals(size, stationSet.size());
    }

    @Test
    public void removeTestNull() {
        ClosestStationIterator it = new ClosestStationIterator(new HashSet<>(), null);
        it.remove();
    }


    @Test
    public void allInOneTest(){
        ClosestStationIterator it = new ClosestStationIterator(stationSet, stationPrincipale);
        Assert.assertNotNull(it);
        Assert.assertTrue(it.hasNext());
        Assert.assertEquals("Gare Viotte", it.next().getNom());
        Assert.assertTrue(it.hasNext());
        Assert.assertEquals("Saint Jacques", it.next().getNom());
        Assert.assertTrue(it.hasNext());
        int size = stationSet.size();
        it.remove();
        int size2 = stationSet.size();
        Assert.assertEquals(size, size2);
        Assert.assertEquals("Hauts de Chazal", it.next().getNom());
        Assert.assertFalse(it.hasNext());
    }
}
