package fr.ufc.l3info.oprog;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
// Mockito
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.internal.matchers.GreaterOrEqual;
import org.mockito.internal.matchers.GreaterThan;
import org.mockito.junit.MockitoJUnitRunner;

// JUnit
import org.junit.runner.RunWith;

import java.util.HashSet;
import java.util.Set;

@RunWith(MockitoJUnitRunner.class)
public class StationWithMocksTest {
    private char[] cadres = {'m', 'h', 'f'};
    private String[] options = {"CADRE_ALUMINIUM", "FREINS_DISQUE", "SUSPENSION_AVANT", "SUSPENSION_ARRIERE",
            "ASSISTANCE_ELECTRIQUE"};
    private int NB_BORNE;
    private Station s;
    private IVelo mockVelo;
    private Abonne abonneBloque;
    private Abonne abonneNonBloque;
    private IRegistre mockIRegistre;
    private FabriqueVelo fabrique = FabriqueVelo.getInstance();

    @Before
    public void beforefunc () {
        NB_BORNE = 35;
        s = new Station("Hauts de Chazal", 47.221563, 5.951402, NB_BORNE);
        mockVelo = Mockito.mock(IVelo.class);
        abonneBloque = Mockito.mock(Abonne.class);
        abonneNonBloque = Mockito.mock(Abonne.class);
        Mockito.when(abonneBloque.estBloque()).thenReturn(true);
        Mockito.when(abonneNonBloque.estBloque()).thenReturn(false);
        mockIRegistre = Mockito.mock(IRegistre.class);
        s.setRegistre(mockIRegistre);
    }

    @Test
    public void constructeurTest(){
        Station st = new Station("Hauts de Chazal", 47.221563, 5.951402, 35);
        Assert.assertEquals(NB_BORNE, st.capacite());
        Assert.assertEquals("Hauts de Chazal", st.getNom());
    }

    @Test
    public void constructeurNull() {
        Station st = new Station(null, 47.221563, 5.951402, 35);
        Assert.assertEquals(NB_BORNE, st.capacite());
        Assert.assertNull(st.getNom());
    }

    @Test
    public void constructeurCapaciteVide() {
        Station st = new Station("Hauts de Chazal", 47.221563, 5.951402, 0);
        Assert.assertEquals(0, st.capacite());
        Assert.assertEquals("Hauts de Chazal", st.getNom());
    }

    @Test
    public void constructeurCapaciteNegative() {
        Station st = new Station("Hauts de Chazal", 47.221563, 5.951402, -5);
        Assert.assertEquals(-5, st.capacite());
        Assert.assertEquals("Hauts de Chazal", st.getNom());
    }

    @Test
    public void nbBornesLibre() {
        Assert.assertEquals(NB_BORNE, s.nbBornesLibres());
        Mockito.when(mockVelo.arrimer()).thenReturn(-4);
        Assert.assertEquals(NB_BORNE, s.nbBornesLibres());
        for (int i = 0; i < 10; i++) {
            s.arrimerVelo(mockVelo, i);
        }
        Assert.assertEquals(NB_BORNE - 9, s.nbBornesLibres());
        for (int i = 0; i < 10; i++) {
            int rand = (int)(Math.random()*options.length);
            s.arrimerVelo(fabrique.construire('m', options[rand]), i);
        }
        Assert.assertEquals(NB_BORNE - 9, s.nbBornesLibres());
        for (int i = 10; i < 20; i++) {
            s.arrimerVelo(mockVelo, i);
        }
        Assert.assertEquals(NB_BORNE - 19, s.nbBornesLibres());
        for (int i = 20; i < 40; i++) {
            s.arrimerVelo(mockVelo, i);
        }
        Assert.assertEquals(0, s.nbBornesLibres());
        for (int i = NB_BORNE; i < 1; i--) {
            s.emprunterVelo(abonneNonBloque, i);
            Assert.assertEquals(NB_BORNE - i, s.nbBornesLibres());
        }
    }

    @Test
    public void nbBornesLibreNull() {
        Mockito.when(mockVelo.arrimer()).thenReturn(-1);
        for (int i = 0; i < 10; i++) {
            s.arrimerVelo(mockVelo, i);
        }
        Assert.assertEquals(NB_BORNE, s.nbBornesLibres());
    }

    @Test
    public void nbBornesLibreDejaArrime() {
        for (int i = 0; i < 10; i++) {
            s.arrimerVelo(null, i);
        }
        Assert.assertEquals(NB_BORNE, s.nbBornesLibres());
    }

    @Test
    public void veloALaBorneTest(){
        Assert.assertNull(s.veloALaBorne(1));
        int err = s.arrimerVelo(mockVelo, 1);
        Assert.assertNotNull(s.veloALaBorne(1));
        Assert.assertNull(s.veloALaBorne(0));
        Assert.assertNull(s.veloALaBorne(-1));
        Assert.assertNull(s.veloALaBorne(NB_BORNE + 1));
    }

    @Test
    public void emprunterVeloTest() {
        Mockito.when(mockIRegistre.nbEmpruntsEnCours(abonneNonBloque)).thenReturn(0);
        Assert.assertNull(s.emprunterVelo(abonneNonBloque, 1));
        for (int i = 0; i < NB_BORNE; i++) {
            int rand = (int)(Math.random()*options.length);
            s.arrimerVelo(fabrique.construire('m', options[rand]), i);
        }
        Assert.assertNull(s.emprunterVelo(abonneBloque, 1));
        Assert.assertNull(s.emprunterVelo(abonneNonBloque, 0));
        Assert.assertNotNull(s.emprunterVelo(abonneNonBloque, 1));
        Assert.assertNull(s.emprunterVelo(abonneNonBloque, 1));
        Mockito.when(mockIRegistre.nbEmpruntsEnCours(abonneNonBloque)).thenReturn(1);
        Assert.assertNull(s.emprunterVelo(abonneNonBloque, 2));
        s.setRegistre(null);
        Assert.assertNull(s.emprunterVelo(abonneNonBloque, 2));
    }

    @Test
    public void arrimerVeloTest() {
        Assert.assertEquals(-1, s.arrimerVelo(mockVelo, 0));
        Assert.assertEquals(-1, s.arrimerVelo(mockVelo, -1));
        Assert.assertEquals(-1, s.arrimerVelo(mockVelo, NB_BORNE + 1));
        Assert.assertEquals(-1, s.arrimerVelo(null, 1));
        Assert.assertEquals(0, s.arrimerVelo(mockVelo, 1));
        Assert.assertEquals(-2, s.arrimerVelo(mockVelo, 1));
        Mockito.when(mockVelo.arrimer()).thenReturn(-1);
        Assert.assertEquals(-3, s.arrimerVelo(mockVelo, 2));
        Mockito.when(mockVelo.arrimer()).thenReturn(0);
        Mockito.when(mockIRegistre.retourner(ArgumentMatchers.<IVelo>any(), ArgumentMatchers.anyLong())).thenReturn(-1);
        Assert.assertEquals(-4, s.arrimerVelo(mockVelo, 3));
        Mockito.when(mockIRegistre.retourner(ArgumentMatchers.<IVelo>any(), ArgumentMatchers.anyLong())).thenReturn(-2);
        Assert.assertEquals(-4, s.arrimerVelo(mockVelo, 4));
        Mockito.when(mockIRegistre.retourner(ArgumentMatchers.<IVelo>any(), ArgumentMatchers.anyLong())).thenReturn(-3);
        Assert.assertEquals(-4, s.arrimerVelo(mockVelo, 5));
        s.setRegistre(null);
        Assert.assertEquals(-2, s.arrimerVelo(mockVelo, 1));

    }

    @Test
    public void distanceTest() {
        Station s2 = new Station("Révolution", 47.240445, 6.022909, 10);
        Assert.assertEquals(5.79, s.distance(s2), 0.01);
        Assert.assertEquals(5.79, s2.distance(s), 0.01);
        Assert.assertEquals(s.distance(s2), s2.distance(s), 0);
    }

    @Test
    public void distanceTestNull() {
        Assert.assertEquals(0, s.distance(null), 0.0);
    }

    @Test
    public void maintenantTest() {
        Assert.assertTrue(s.maintenant() <= s.maintenant());
        long start = s.maintenant();
        // Do some stuff
        for (int i = 0; i < 100000; i++) {
            int a = 2;
            double b = Math.pow(a*42/666, 8);
        }
        Assert.assertTrue(start < s.maintenant());
    }

    @Test
    public void equilibrerWithEmptySetTest() {
        Set<IVelo> velos = new HashSet<IVelo>();
        s.equilibrer(velos);
        Assert.assertEquals(NB_BORNE, s.nbBornesLibres());
    }

    @Test
    public void equilibrerWithNullSetTest() {
        s.equilibrer(null);
        Assert.assertEquals(NB_BORNE, s.nbBornesLibres());
    }

    @Test
    public void equilibrerWithLargeSetTest() {
        Set<IVelo> velos = new HashSet<IVelo>();
        for (int i = 0; i < NB_BORNE; i++) {
            velos.add(new Velo());
        }
        s.equilibrer(velos);
        Assert.assertEquals(NB_BORNE/2, s.nbBornesLibres());
        s.equilibrer(velos);
        Assert.assertEquals(NB_BORNE/2, s.nbBornesLibres());
        for (int i = 1; i <= NB_BORNE ; i++) {
            s.arrimerVelo(new Velo(), i);
        }
        Assert.assertEquals(0, s.nbBornesLibres());
        s.equilibrer(velos);
        Assert.assertEquals(NB_BORNE/2, s.nbBornesLibres());
    }

    @Test
    public void equilibrerWithSmallSetTest() {
        Set<IVelo> velos = new HashSet<IVelo>();
        for (int i = 0; i < 2; i++) {
            velos.add(new Velo());
        }
        s.equilibrer(velos);
        Assert.assertEquals(NB_BORNE - 2, s.nbBornesLibres());
        s.equilibrer(velos);
        Assert.assertEquals(NB_BORNE - 2, s.nbBornesLibres());
    }
}
