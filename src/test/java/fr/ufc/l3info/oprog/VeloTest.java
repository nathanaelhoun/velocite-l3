package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test unitaire pour les vélos.
 */
public class VeloTest {

    final double DELTA_DISTANCE = 0.0001;
    final double DELTA_EURO = 0.001;
    final double DISTANCE_ENTRE_REVISIONS = 500;

    private Velo v;

    @Before
    public void before_func(){
        v = new Velo();
    }

    /**************** TESTS CONSTRUCTEURS ****************/
    @Test
    public void create_velo() {
        Assert.assertTrue(v.toString().contains("mixte"));
        Assert.assertEquals(v.tarif(), 2.0, 0.001);
    }

    @Test
    public void create_velo_femme() {
        Velo v1 = new Velo('f');
        Assert.assertTrue(v1.toString().contains("femme"));
        Velo v2 = new Velo('F');
        Assert.assertTrue(v2.toString().contains("femme"));
    }
    @Test
    public void create_velo_homme() {
        Velo v1 = new Velo('h');
        Assert.assertTrue(v1.toString().contains("homme"));
        Velo v2 = new Velo('H');
        Assert.assertTrue(v2.toString().contains("homme"));
    }

    @Test
    public void create_velo_mixte() {
        Velo v1 = new Velo('b');
        Assert.assertTrue(v1.toString().contains("mixte"));

        Velo v2 = new Velo('\0');
        Assert.assertTrue(v2.toString().contains("mixte"));
    }


    /****************** TESTS CYCLE DE VIE *********************/

    @Test
    public void testDecrocherArrimer() {
        Assert.assertEquals(v.decrocher(), -1);
        Assert.assertEquals(v.arrimer(), 0);
        Assert.assertEquals(v.arrimer(), -1);
        Assert.assertEquals(v.decrocher(), 0);
    }

    @Test
    public void testAbimer() {
        Assert.assertFalse(v.estAbime());
        v.abimer();
        Assert.assertTrue(v.estAbime());
        v.abimer();
        Assert.assertTrue(v.estAbime());
    }

    @Test
    public void testAbimerArrimer() {
        v.abimer();
        Assert.assertTrue(v.estAbime());
        v.arrimer();
        Assert.assertTrue(v.estAbime());
        Assert.assertEquals(v.reviser(), -1);
        Assert.assertEquals(v.reparer(), -1);
        Assert.assertTrue(v.estAbime());
    }

    @Test
    public void testAbimerDecrocher() {
        v.arrimer();
        Assert.assertFalse(v.estAbime());
        v.abimer();
        Assert.assertTrue(v.estAbime());
        Assert.assertEquals(v.decrocher(), 0);
        Assert.assertTrue(v.estAbime());
        Assert.assertEquals(v.reviser(), 0);
        Assert.assertFalse(v.estAbime());
    }

    @Test
    public void testReparer() {
        v.abimer();
        Assert.assertEquals(v.reparer(), 0);
        Assert.assertFalse(v.estAbime());
        Assert.assertEquals(v.reparer(), -2);
        Assert.assertFalse(v.estAbime());
        v.abimer();
        v.arrimer();
        Assert.assertEquals(v.reparer(), -1);
        Assert.assertTrue(v.estAbime());
    }

    /***************** TESTS KILOMETRAGE *********************/

    @Test
    public void testKilometrage() {
        Assert.assertEquals(v.kilometrage(), 0, 0);
        v.parcourir(0);
        Assert.assertEquals(v.kilometrage(), 0, 0);
        v.parcourir(-15);
        Assert.assertEquals(v.kilometrage(), 0, 0);
        v.parcourir(15);
        Assert.assertEquals(v.kilometrage(), 15, 0.001);
        v.parcourir(0.0001);
        Assert.assertEquals(v.kilometrage(), 15, 0.001);
        v.parcourir(100.5);
        Assert.assertEquals(v.kilometrage(), 115.5, 0.001);
    }

    @Test
    public void testParcourirArrime() {
        v.arrimer();
        v.parcourir(10);
        Assert.assertEquals(v.kilometrage(), 0, 0);
    }

    @Test
    public void testProchaineRevision() {
        Assert.assertEquals(v.prochaineRevision(), 500, 0);
        v.parcourir(700);
        Assert.assertEquals(v.prochaineRevision(), -200, 0.001);
        v.reviser();
        double km = v.kilometrage();
        Assert.assertEquals(v.prochaineRevision(), 500, 0);
        v.parcourir(70.65);
        Assert.assertEquals(v.prochaineRevision(), 429.35, 0.001);
        v.parcourir(400);
        km = v.kilometrage() - km;
        Assert.assertEquals(v.prochaineRevision(), 500 - km, 0.001);
        v.parcourir(29.3);
        Assert.assertEquals(v.prochaineRevision(), 0, 0.1);
    }

    @Test
    public void testReviser() {
        v.parcourir(25);
        v.arrimer();
        Assert.assertEquals(v.reviser(), -1);
        v.decrocher();
        Assert.assertEquals(v.reviser(), 0);
    }

    /*********************** TEST TOSTRING ********************/
    @Test
    public void testToString() {
        Assert.assertFalse(v.toString().contains("(révision nécessaire)"));
        v.parcourir(700);
        Assert.assertTrue(v.toString().contains("(révision nécessaire)"));
    }

    @Test
    public void testConstructeur() {
        Velo v = new Velo();
        Assert.assertEquals("Vélo cadre mixte - 0.0 km", v.toString());
        Assert.assertFalse(v.estAbime());
        Assert.assertEquals(2.0, v.tarif(), this.DELTA_EURO);

        Velo v2 = new Velo('a');
        Assert.assertEquals("Vélo cadre mixte - 0.0 km", v2.toString());
        Assert.assertFalse(v2.estAbime());
        Assert.assertEquals(2.0, v2.tarif(), this.DELTA_EURO);
    }

    @Test
    public void testConstructeurHomme() {
        Velo v2 = new Velo('h');
        Assert.assertEquals("Vélo cadre homme - 0.0 km", v2.toString());
        Assert.assertFalse(v2.estAbime());
        Assert.assertEquals(2.0, v2.tarif(), this.DELTA_EURO);

        v2 = new Velo('H');
        Assert.assertEquals("Vélo cadre homme - 0.0 km", v2.toString());
        Assert.assertFalse(v2.estAbime());
        Assert.assertEquals(2.0, v2.tarif(), this.DELTA_EURO);
    }

    @Test
    public void testConstructeurFemme() {
        Velo v3 = new Velo('F');
        Assert.assertEquals("Vélo cadre femme - 0.0 km", v3.toString());
        Assert.assertFalse(v3.estAbime());
        Assert.assertEquals(2.0, v3.tarif(), this.DELTA_EURO);

        v3 = new Velo('f');
        Assert.assertEquals("Vélo cadre femme - 0.0 km", v3.toString());
        Assert.assertFalse(v3.estAbime());
        Assert.assertEquals(2.0, v3.tarif(), this.DELTA_EURO);
    }

    @Test
    public void testArrimerDecrocher() {
        Assert.assertEquals(-1, this.v.decrocher());
        Assert.assertEquals(0, this.v.arrimer());
        Assert.assertEquals(-1, this.v.arrimer());
        Assert.assertEquals(0, this.v.decrocher());
        Assert.assertEquals(-1, this.v.decrocher());
        Assert.assertEquals(0, this.v.arrimer());
    }

    @Test
    public void testAbimerReparer() {
        Assert.assertFalse(this.v.estAbime());

        this.v.arrimer();
        Assert.assertEquals(-1, this.v.reparer());

        this.v.decrocher();
        Assert.assertEquals(-2, this.v.reparer());

        this.v.abimer();
        Assert.assertTrue(this.v.estAbime());
        this.v.abimer();
        Assert.assertTrue(this.v.estAbime());

        Assert.assertEquals(0, this.v.reparer());
        Assert.assertFalse(this.v.estAbime());

        this.v.arrimer();
        Assert.assertEquals(-1, this.v.reparer());
        this.v.abimer();
        Assert.assertTrue(this.v.estAbime());
    }

    @Test
    public void testParcourir() {
        Assert.assertEquals(0.0, this.v.kilometrage(), this.DELTA_DISTANCE);
        Assert.assertEquals(this.DISTANCE_ENTRE_REVISIONS, this.v.prochaineRevision(), this.DELTA_DISTANCE);

        this.v.arrimer();
        this.v.parcourir(100);
        Assert.assertEquals(0.0, this.v.kilometrage(), this.DELTA_DISTANCE);

        this.v.decrocher();
        this.v.parcourir(100);
        Assert.assertEquals(100, this.v.kilometrage(), this.DELTA_DISTANCE);
        Assert.assertEquals(this.DISTANCE_ENTRE_REVISIONS - 100, this.v.prochaineRevision(), this.DELTA_DISTANCE);

        this.v.parcourir(0.5);
        Assert.assertEquals(100.5, this.v.kilometrage(), this.DELTA_DISTANCE);

        this.v.parcourir(-10);
        Assert.assertEquals(100.5, this.v.kilometrage(), this.DELTA_DISTANCE);

        this.v.parcourir(400);
        Assert.assertEquals(500.5, this.v.kilometrage(), this.DELTA_DISTANCE);
        Assert.assertEquals(this.DISTANCE_ENTRE_REVISIONS - 500.5, this.v.prochaineRevision(), this.DELTA_DISTANCE);
    }

    @Test
    public void testReviserNath() {
        this.v.arrimer();
        Assert.assertEquals(-1, this.v.reviser());

        this.v.decrocher();
        Assert.assertEquals(0, this.v.reviser());
        Assert.assertEquals(this.DISTANCE_ENTRE_REVISIONS, this.v.prochaineRevision(), this.DELTA_DISTANCE);

        this.v.parcourir(555);
        Assert.assertEquals(this.DISTANCE_ENTRE_REVISIONS - 555, this.v.prochaineRevision(), this.DELTA_DISTANCE);
        Assert.assertEquals("Vélo cadre mixte - 555.0 km (révision nécessaire)", this.v.toString());

        Assert.assertEquals(0, this.v.reviser());
        Assert.assertEquals(this.DISTANCE_ENTRE_REVISIONS, this.v.prochaineRevision(), this.DELTA_DISTANCE);
    }

    @Test
    public void testReviserEtReparer() {
        this.v.decrocher();

        this.v.parcourir(374.5);
        Assert.assertEquals(this.DISTANCE_ENTRE_REVISIONS - 374.5, this.v.prochaineRevision(), this.DELTA_DISTANCE);
        Assert.assertEquals(0, this.v.reviser());
        Assert.assertEquals(this.DISTANCE_ENTRE_REVISIONS, this.v.prochaineRevision(), this.DELTA_DISTANCE);

        this.v.parcourir(34);
        Assert.assertEquals(this.DISTANCE_ENTRE_REVISIONS - 34, this.v.prochaineRevision(), this.DELTA_DISTANCE);
        this.v.abimer();
        Assert.assertTrue(this.v.estAbime());

        Assert.assertEquals(0, this.v.reviser());
        Assert.assertFalse(this.v.estAbime());
        Assert.assertEquals(this.DISTANCE_ENTRE_REVISIONS, this.v.prochaineRevision(), this.DELTA_DISTANCE);
    }
}
