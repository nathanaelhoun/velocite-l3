package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.statements.ExpectException;

/**
 * Test unitaire pour les options.
 */
public class OptionTest {
    private IVelo v;

    final double DELTA_DISTANCE = 0.0001;
    final double DELTA_EURO = 0.001;

    @Before
    public void beforeFunc(){
        v = new Velo('f');
    }

    @Test(expected = NullPointerException.class)
    public void optionVeloNull() {
       IVelo velo = new OptCadreAlu(null);
       Assert.assertNull(velo);
       velo.decrocher();
    }

    @Test
    public void optionVeloHomme(){
        IVelo velo = new Velo('h');
        Assert.assertEquals("Vélo cadre homme - 0.0 km", velo.toString());
        Option o = new OptCadreAlu(velo);
        Assert.assertTrue(o instanceof OptCadreAlu);
        Assert.assertTrue(o instanceof Option);
        Assert.assertTrue(o instanceof IVelo);
        Assert.assertFalse(o instanceof OptAssistanceElectrique);
        Assert.assertEquals("Vélo cadre homme, cadre aluminium - 0.0 km", o.toString());
    }

    @Test
    public void optionVeloFemme(){
        Assert.assertEquals("Vélo cadre femme - 0.0 km", v.toString());
        Option o = new OptSuspensionArriere(v);
        Assert.assertTrue(o instanceof OptSuspensionArriere);
        Assert.assertTrue(o instanceof Option);
        Assert.assertTrue(o instanceof IVelo);
        Assert.assertEquals("Vélo cadre femme, suspension arrière - 0.0 km", o.toString());
    }

    @Test
    public void optionMultiple(){
        Assert.assertEquals("Vélo cadre femme - 0.0 km", v.toString());
        Option o = new OptSuspensionArriere(v);
        Assert.assertEquals("Vélo cadre femme, suspension arrière - 0.0 km", o.toString());
        Assert.assertTrue(o instanceof OptSuspensionArriere);
        o = new OptSuspensionAvant(v);
        Assert.assertEquals("Vélo cadre femme, suspension avant - 0.0 km", o.toString());
        Assert.assertTrue(o instanceof OptSuspensionAvant);
        Assert.assertFalse(o instanceof OptSuspensionArriere);
    }

    @Test
    public void optionMultiple2(){
        Assert.assertEquals("Vélo cadre femme - 0.0 km", v.toString());
        Assert.assertEquals(v.tarif(), 2.0,DELTA_EURO);
        v = new OptSuspensionArriere(v);
        Assert.assertEquals("Vélo cadre femme, suspension arrière - 0.0 km", v.toString());
        Assert.assertEquals(v.tarif(), 2.5,DELTA_EURO);
        Assert.assertTrue(v instanceof OptSuspensionArriere);
        v = new OptSuspensionAvant(v);
        Assert.assertTrue(v.toString().contains("suspension arrière"));
        Assert.assertTrue(v.toString().contains("suspension avant"));
        Assert.assertTrue(v instanceof OptSuspensionAvant);
        Assert.assertEquals(v.tarif(), 3,DELTA_EURO);
    }

    @Test
    public void optionAll(){
        Assert.assertEquals("Vélo cadre femme - 0.0 km", v.toString());
        Assert.assertEquals(v.tarif(), 2.0,DELTA_EURO);
        v = new OptSuspensionArriere(v);
        Assert.assertEquals("Vélo cadre femme, suspension arrière - 0.0 km", v.toString());
        Assert.assertEquals(v.tarif(), 2.5,DELTA_EURO);
        Assert.assertTrue(v instanceof OptSuspensionArriere);
        v = new OptSuspensionAvant(v);
        Assert.assertTrue(v.toString().contains("suspension arrière"));
        Assert.assertTrue(v.toString().contains("suspension avant"));
        Assert.assertTrue(v instanceof OptSuspensionAvant);
        Assert.assertEquals(v.tarif(), 3,DELTA_EURO);
        v = new OptFreinsDisque(v);
        Assert.assertTrue(v.toString().contains("suspension arrière"));
        Assert.assertTrue(v.toString().contains("freins à disque"));
        Assert.assertTrue(v.toString().contains("suspension avant"));
        Assert.assertTrue(v instanceof OptFreinsDisque);
        Assert.assertEquals(v.tarif(), 3.3,DELTA_EURO);
        v = new OptAssistanceElectrique(v);
        Assert.assertTrue(v.toString().contains("suspension arrière"));
        Assert.assertTrue(v.toString().contains("assistance électrique"));
        Assert.assertTrue(v.toString().contains("suspension avant"));
        Assert.assertTrue(v.toString().contains("freins à disque"));
        Assert.assertTrue(v instanceof OptAssistanceElectrique);
        Assert.assertEquals(v.tarif(), 5.3,DELTA_EURO);
        v = new OptCadreAlu(v);
        Assert.assertTrue(v.toString().contains("suspension arrière"));
        Assert.assertTrue(v.toString().contains("assistance électrique"));
        Assert.assertTrue(v.toString().contains("suspension avant"));
        Assert.assertTrue(v.toString().contains("freins à disque"));
        Assert.assertTrue(v.toString().contains("cadre aluminium"));
        Assert.assertTrue(v instanceof OptCadreAlu);
        Assert.assertEquals(v.tarif(), 5.5,DELTA_EURO);
    }

    @Test
    public void methodeVeloPasTouchees(){
        v = new OptAssistanceElectrique(v);
        Assert.assertEquals(-1, v.decrocher());
        Assert.assertEquals(0, v.arrimer());
        Assert.assertFalse(v.estAbime());
        Assert.assertEquals(0.0 ,v.kilometrage(), DELTA_DISTANCE);
        v.parcourir(10);
        Assert.assertEquals(0.0 ,v.kilometrage(), DELTA_DISTANCE);
        Assert.assertEquals(500.0 ,v.prochaineRevision(), DELTA_DISTANCE);
        Assert.assertEquals(0, v.decrocher());
        v.parcourir(700);
        Assert.assertEquals(700.0 ,v.kilometrage(), DELTA_DISTANCE);
        Assert.assertEquals(-200.0 ,v.prochaineRevision(), DELTA_DISTANCE);
        v.abimer();
        Assert.assertTrue(v.estAbime());
        v.reviser();
        Assert.assertFalse(v.estAbime());
        Assert.assertEquals(500.0 ,v.prochaineRevision(), DELTA_DISTANCE);
        Assert.assertEquals(700.0 ,v.kilometrage(), DELTA_DISTANCE);
        Assert.assertEquals(4.0, v.tarif(), DELTA_EURO);
        v.abimer();
        Assert.assertTrue(v.estAbime());
        v.reparer();
        Assert.assertFalse(v.estAbime());
    }

    @Test
    public void testCadreAluToString() {
        IVelo v = new Velo();
        v = new OptCadreAlu(v);

        Assert.assertEquals("Vélo cadre mixte, cadre aluminium - 0.0 km", v.toString());
    }

    @Test
    public void testFreinsDisqueTarif() {
        IVelo v = new Velo();
        v = new OptFreinsDisque(v);

        Assert.assertEquals(2.3, v.tarif(), this.DELTA_EURO);
    }

    @Test
    public void testFreinsDisqueToString() {
        IVelo v = new Velo();
        v = new OptFreinsDisque(v);

        Assert.assertEquals("Vélo cadre mixte, freins à disque - 0.0 km", v.toString());
    }

    @Test
    public void testSuspensionAvantTarif() {
        IVelo v = new Velo();
        v = new OptSuspensionAvant(v);

        Assert.assertEquals(2.5, v.tarif(), this.DELTA_EURO);
    }

    @Test
    public void testSuspensionAvantToString() {
        IVelo v = new Velo();
        v = new OptSuspensionAvant(v);

        Assert.assertEquals("Vélo cadre mixte, suspension avant - 0.0 km", v.toString());
    }

    @Test
    public void testSuspensionArriereTarif() {
        IVelo v = new Velo();
        v = new OptSuspensionArriere(v);

        Assert.assertEquals(2.5, v.tarif(), this.DELTA_EURO);
    }

    @Test
    public void testSuspensionArriereToString() {
        IVelo v = new Velo();
        v = new OptSuspensionArriere(v);

        Assert.assertEquals("Vélo cadre mixte, suspension arrière - 0.0 km", v.toString());
    }

    @Test
    public void testAssistanceElectriqueTarif() {
        IVelo v = new Velo();
        v = new OptAssistanceElectrique(v);

        Assert.assertEquals(4.0, v.tarif(), this.DELTA_EURO);
    }

    @Test
    public void testAssistanceElectriqueToString() {
        IVelo v = new Velo();
        v = new OptAssistanceElectrique(v);

        Assert.assertEquals("Vélo cadre mixte, assistance électrique - 0.0 km", v.toString());
    }

    /**
     * Sert essentiellement à atteindre le 100% de code coverage.
     * Ces méthodes sont censées simplement appeler celles de l'objet original donc pas de modifs à faire
     */
    @Test
    public void testAutresMethodesNonModifiees() {
        IVelo veloBase = new Velo();
        IVelo veloAModifier = new Velo();
        IVelo v = new OptAssistanceElectrique(veloAModifier);
        v = new OptCadreAlu(v);
        v = new OptFreinsDisque(v);
        v = new OptSuspensionArriere(v);
        v = new OptSuspensionAvant(v);
        v = new OptAssistanceElectrique(v);

        Assert.assertEquals(veloBase.arrimer(), v.arrimer());
        Assert.assertEquals(veloBase.kilometrage(), v.kilometrage(), this.DELTA_DISTANCE);
        veloBase.parcourir(100);
        v.parcourir(100);
        Assert.assertEquals(veloBase.kilometrage(), v.kilometrage(), this.DELTA_DISTANCE);
        Assert.assertEquals(veloBase.decrocher(), v.decrocher());
        veloBase.parcourir(100);
        v.parcourir(100);
        Assert.assertEquals(veloBase.kilometrage(), v.kilometrage(), this.DELTA_DISTANCE);

        Assert.assertEquals(veloBase.estAbime(), v.estAbime());
        veloBase.abimer();
        v.abimer();
        Assert.assertEquals(veloBase.estAbime(), v.estAbime());

        Assert.assertEquals(veloBase.prochaineRevision(), v.prochaineRevision(), this.DELTA_DISTANCE);
        Assert.assertEquals(veloBase.reviser(), v.reviser());
        Assert.assertEquals(veloBase.prochaineRevision(), v.prochaineRevision(), this.DELTA_DISTANCE);


        veloBase.parcourir(1000);
        v.parcourir(1000);
        Assert.assertEquals(veloBase.kilometrage(), v.kilometrage(), this.DELTA_DISTANCE);
        Assert.assertEquals(veloBase.prochaineRevision(), v.prochaineRevision(), this.DELTA_DISTANCE);

        Assert.assertEquals(veloBase.arrimer(), v.arrimer());
        Assert.assertEquals(veloBase.reparer(), v.reparer());
        Assert.assertEquals(veloBase.estAbime(), v.estAbime());
        Assert.assertEquals(veloBase.decrocher(), v.decrocher());
        Assert.assertEquals(veloBase.reparer(), v.reparer());
        Assert.assertEquals(veloBase.estAbime(), v.estAbime());
        Assert.assertNotEquals(veloBase.hashCode(), v.hashCode());
        Assert.assertNotEquals("Sont des objets différents", v, veloBase);
    }


    @Test
    public void testStringPlusieursOptions() {
        IVelo veloBase = new Velo();
        IVelo v = new OptAssistanceElectrique(veloBase);
        v = new OptCadreAlu(v);
        v = new OptFreinsDisque(v);
        v = new OptSuspensionArriere(v);
        v = new OptSuspensionAvant(v);
        v = new OptAssistanceElectrique(v);

        String chaine = v.toString();

        Assert.assertTrue("Partie de base de la chaîne", chaine.startsWith("Vélo cadre mixte"));
        chaine = chaine.replaceAll("Vélo cadre mixte", "");
        Assert.assertFalse("Partie de base de la chaîne", chaine.startsWith("Vélo cadre mixte"));

        Assert.assertTrue("Partie de base de la chaîne", chaine.endsWith(" - 0.0 km"));
        chaine = chaine.replaceAll(" - 0.0 km", "");
        Assert.assertFalse("Partie de base de la chaîne", chaine.endsWith(" - 0.0 km"));


        String[] chainesOptions = {
                ", cadre aluminium",
                ", freins à disque",
                ", suspension avant",
                ", suspension arrière",
                ", assistance électrique"
        };

        for (String opt : chainesOptions) {
            Assert.assertTrue("Option : " + opt, chaine.contains(opt));
            chaine = chaine.replaceAll(opt, "");
            Assert.assertFalse("Option : " + opt, chaine.contains(opt));
        }

        Assert.assertEquals("", chaine);
    }

    @Test(expected = NullPointerException.class)
    public void testNull() {
        IVelo v = new OptAssistanceElectrique(null);
        Assert.assertEquals("Vélo cadre mixte, assistance électrique - 0.0 km", v.toString());
    }
}
