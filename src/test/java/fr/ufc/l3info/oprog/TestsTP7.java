package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class TestsTP7 {

    final double DELTA_EURO = 0.001;
    private Abonne a;
    private JRegistre r;
    private IVelo velo;
    private Station station;
    private final long HEURE = 3600000;
    final long TEMPS_DEBUT_FACTURATION = System.currentTimeMillis() - HEURE;
    final long TEMPS_FIN_FACTURATION = TEMPS_DEBUT_FACTURATION + 1000L * 3600L * 24L * 31L; // DEBUT + 1 mois

    @Before
    public void beforeFunc() throws IncorrectNameException {
        r = new JRegistre();
        a = new Abonne("Fab", "14568-56487-12345678912-38");
        velo = new Velo();
        int NB_BORNE = 15;
        station = new Station("Hauts de Chazal", 47.221563, 5.951402, NB_BORNE);
        station.setRegistre(r);
        for (int i = 1; i <= NB_BORNE; i++) {
            station.arrimerVelo(new Velo(), i);
        }

    }

    @Test
    public void bloquerAbonneDestructeurTest() {
        IVelo v = station.emprunterVelo(a, 1);
        v.abimer();
        station.arrimerVelo(v, 1);
        Assert.assertTrue(a.estBloque());
    }

    @Test
    public void emprunteurTestNull() {
        Assert.assertNull(r.emprunteur(null));
    }

    @Test
    public void emprunteurTestVeloNonEmprunter() {
        Assert.assertNull(r.emprunteur(velo));
    }

    @Test
    public void emprunteurTestVeloEmprunter() {
        velo = station.emprunterVelo(a, 1);
        Assert.assertNotNull(r.emprunteur(velo));
    }

    @Test
    public void emprunteurTestDouble() {
        velo = station.emprunterVelo(a, 1);
        Abonne abonne = r.emprunteur(velo);
        Assert.assertEquals(abonne, r.emprunteur(velo));
        Assert.assertEquals(a, r.emprunteur(velo));
    }

    @Test
    public void emprunteurTestVeloRendu() {
        velo = station.emprunterVelo(a, 1);
        Abonne abonne = r.emprunteur(velo);
        station.arrimerVelo(velo, 1);
        Abonne abonne2 = r.emprunteur(velo);
        Assert.assertEquals(a, abonne);
        Assert.assertNotEquals(a, abonne2);
        Assert.assertNull(abonne2);
    }

    @Test
    public void emprunterAbimeTest() {
        station.veloALaBorne(1).abimer();
        velo = station.emprunterVelo(a, 1);
        Assert.assertNull(velo);
    }

    @Test
    public void facturationDegressiveTest10Emprunts() {
        double tarif = station.veloALaBorne(1).tarif();
        emprunternVelos(System.currentTimeMillis(), 10, 60);

        Assert.assertEquals(
                10 * tarif,
                r.facturation(a, TEMPS_DEBUT_FACTURATION, TEMPS_FIN_FACTURATION),
                DELTA_EURO
        );
    }

    @Test
    public void facturationDegressiveTest20Emprunts() {
        double tarif = station.veloALaBorne(1).tarif();
        emprunternVelos(System.currentTimeMillis(), 20, 10);
        double prix = 10 * tarif * 1 / 6.0 * (1 + 0.9);
        Assert.assertEquals(
                prix,
                r.facturation(a, TEMPS_DEBUT_FACTURATION, TEMPS_FIN_FACTURATION),
                DELTA_EURO
        );
    }

    @Test
    public void facturationDegressiveTest30Emprunts() {
        double tarif = station.veloALaBorne(1).tarif();
        emprunternVelos(System.currentTimeMillis(), 30, 10);
        double prix = 10 * tarif * 1 / 6.0 * (1 + 0.9 + 0.8);
        Assert.assertEquals(
                prix,
                r.facturation(a, TEMPS_DEBUT_FACTURATION, TEMPS_FIN_FACTURATION),
                DELTA_EURO
        );
    }

    @Test
    public void facturationDegressiveTest40Emprunts() {
        double tarif = station.veloALaBorne(1).tarif();
        emprunternVelos(System.currentTimeMillis(), 40, 10);
        double prix = 10 * tarif * 1 / 6.0 * (1 + 0.9 + 0.8 + 0.7);
        Assert.assertEquals(prix, r.facturation(
                a,
                TEMPS_DEBUT_FACTURATION, TEMPS_FIN_FACTURATION),
                DELTA_EURO
        );
    }

    @Test
    public void facturationDegressiveTest50Emprunts() {
        double tarif = station.veloALaBorne(1).tarif();
        emprunternVelos(System.currentTimeMillis(), 50, 10);
        double prix = 10 * tarif * 1 / 6.0 * (1 + 0.9 + 0.8 + 0.7 + 0.6);
        Assert.assertEquals(prix, r.facturation(
                a,
                TEMPS_DEBUT_FACTURATION, TEMPS_FIN_FACTURATION),
                DELTA_EURO
        );
    }

    @Test
    public void facturationDegressiveTest51Emprunts() {
        double tarif = station.veloALaBorne(1).tarif();
        emprunternVelos(System.currentTimeMillis(), 51, 10);
        double prix = 10 * tarif * 1 / 6.0 * (1 + 0.9 + 0.8 + 0.7 + 0.6) + tarif * 1 / 6 * 0.5;
        Assert.assertEquals(
                prix,
                r.facturation(a, TEMPS_DEBUT_FACTURATION, TEMPS_FIN_FACTURATION),
                DELTA_EURO
        );
    }

    @Test
    public void facturationDegressiveTest61EmpruntsAvecEmpruntsDe5MinutesPile() {
        double tarif = station.veloALaBorne(1).tarif();
        long finPremiersEmprunts = emprunternVelos(System.currentTimeMillis(), 10, 5);
        emprunternVelos(finPremiersEmprunts + HEURE, 51, 10);

        double prix = 10 * tarif * (1 / 12.0) // Emprunts de 5 minutes plein pot mais qui comptent dans la régression
                + 10 * tarif * (1 / 6.0) * (0.9 + 0.8 + 0.7 + 0.6) // Emprunts qui dégressent en prix
                + 11 * tarif * 1 / 6 * 0.5; // 51ème

        Assert.assertEquals(
                prix,
                r.facturation(a, TEMPS_DEBUT_FACTURATION, TEMPS_FIN_FACTURATION),
                DELTA_EURO
        );
    }

    @Test
    public void facturationDegressiveTest51EmpruntsAvecEmpruntsMoins5Minutes() {
        double tarif = station.veloALaBorne(1).tarif();
        long finPremiersEmprunts = emprunternVelos(System.currentTimeMillis(), 10, 3);
        emprunternVelos(finPremiersEmprunts + HEURE, 51, 10);

        double prix = 10 * tarif * 1 / 20 // Emprunts de 3 minutes qui ne comptent pas dans la dégression
                + 10 * tarif * 1 / 6.0 * (1 + 0.9 + 0.8 + 0.7 + 0.6) // Emprunts qui dégressent
                + tarif * 1 / 6 * 0.5; // 51ème

        Assert.assertEquals(
                prix,
                r.facturation(a, TEMPS_DEBUT_FACTURATION, TEMPS_FIN_FACTURATION),
                DELTA_EURO
        );
    }

    @Test
    public void facturationDegressiveTestEmpruntEncoreEnCours() {
        velo = station.emprunterVelo(a, 1);
        Assert.assertEquals(
                "Aucun emprunt terminé",
                0,
                r.facturation(a, TEMPS_DEBUT_FACTURATION, TEMPS_FIN_FACTURATION),
                DELTA_EURO
        );
    }

    @Test
    public void facturationDegressiveTestEmpruntTermineApresPeriodeFacturation() {
        // Emprunt commencé pendant la période de facturation
        velo = station.emprunterVelo(a, 1);
        Station spyStation = Mockito.spy(this.station);
        Mockito.when(spyStation.maintenant()).thenReturn(TEMPS_FIN_FACTURATION + HEURE);
        spyStation.arrimerVelo(velo, 1);

        long HEURE_ET_DEMIE = 5400000;
        emprunternVelos(TEMPS_FIN_FACTURATION + HEURE_ET_DEMIE, 10, 10);

        Assert.assertEquals(
                "Aucun emprunt terminé dans la période de facturation",
                0,
                r.facturation(a, TEMPS_DEBUT_FACTURATION, TEMPS_FIN_FACTURATION),
                DELTA_EURO
        );
    }

    public long emprunternVelos(long startPremierEmprunt, int n, int duree) {
        long start = startPremierEmprunt;
        long end = start;
        Station spyStation = Mockito.spy(this.station);
        for (int i = 0; i < n; i++) {
            start = end + 6000;
            Mockito.when(spyStation.maintenant()).thenReturn(start);
            velo = spyStation.emprunterVelo(a, 1);
            end = start + duree * 1000 * 60;
            Mockito.when(spyStation.maintenant()).thenReturn(end);
            spyStation.arrimerVelo(velo, 1);
        }

        return end;
    }
}
