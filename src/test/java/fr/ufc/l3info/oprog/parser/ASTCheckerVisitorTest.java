package fr.ufc.l3info.oprog.parser;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ASTCheckerVisitorTest {

    final String path = "./target/classes/data/";
    ASTCheckerVisitor v;
    StationParser parser;

    @Before
    public void before_func() {
        v = new ASTCheckerVisitor();
        parser = StationParser.getInstance();
    }

    @Test
    public void nomStationValableTest() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File(path + "stationsOK.txt"));
        n.accept(v);
        Assert.assertEquals(0, v.getErrors().size());
    }

    @Test
    public void nomStationManquantTest() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File(path + "stationsBugMauvaisNom.txt"));
        n.accept(v);
        Assert.assertEquals(3, v.getErrors().size());
        for(ERROR_KIND err : v.getErrors().values()){
            Assert.assertEquals(ERROR_KIND.EMPTY_STATION_NAME, err);
        }
    }

    @Test
    public void attributDoubleTest() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File(path + "stationsBugDoubleAttribut.txt"));
        n.accept(v);
        Assert.assertEquals(1, v.getErrors().size());
        Assert.assertTrue(v.getErrors().containsValue(ERROR_KIND.DUPLICATE_DECLARATION));
    }

    @Test
    public void attributManquantTest() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File(path + "stationsBugManqueAttribut.txt"));
        n.accept(v);
        Assert.assertEquals(7, v.getErrors().size());
        for(ERROR_KIND err : v.getErrors().values()){
            Assert.assertEquals(ERROR_KIND.MISSING_DECLARATION, err);
        }
    }

    @Test
    public void ListeVideTest() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File(path + "stationsBugListeVide.txt"));
        n.accept(v);
        Assert.assertEquals(1, v.getErrors().size());
        Assert.assertTrue(v.getErrors().containsValue(ERROR_KIND.EMPTY_LIST));
    }

    @Test
    public void nomStationDupliqueTest() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File(path + "stationsBugNomsDupliques.txt"));
        n.accept(v);
        Assert.assertEquals(3, v.getErrors().size());
        for (ERROR_KIND err : v.getErrors().values()) {
            Assert.assertEquals(ERROR_KIND.DUPLICATE_STATION_NAME, err);
        }
    }

    @Test
    public void mauvaiseValeurLatitudeTest() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File(path + "stationsBugMauvaiseLatitude.txt"));
        n.accept(v);
        Assert.assertEquals(2, v.getErrors().size());
        for (ERROR_KIND err : v.getErrors().values()) {
            Assert.assertEquals(ERROR_KIND.WRONG_NUMBER_VALUE, err);
        }
    }

    @Test
    public void mauvaiseValeurLongitudeTest() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File(path + "stationsBugMauvaiseLongitude.txt"));
        n.accept(v);
        Assert.assertEquals(2, v.getErrors().size());
        for (ERROR_KIND err : v.getErrors().values()) {
            Assert.assertEquals(ERROR_KIND.WRONG_NUMBER_VALUE, err);
        }
    }

    @Test
    public void mauvaiseValeurCapaciteTest() throws IOException, StationParserException {
        ASTNode n = parser.parse(new File(path + "stationsBugMauvaiseCapacite.txt"));
        n.accept(v);
        Assert.assertEquals(3, v.getErrors().size());
        for (ERROR_KIND err : v.getErrors().values()) {
            Assert.assertEquals(ERROR_KIND.WRONG_NUMBER_VALUE, err);
        }
    }

    @Test
    public void testParserVide() throws StationParserException, IOException {
        ASTNode n = parser.parse(new File(path + "stationsBugListeVide.txt"));
        ASTCheckerVisitor checker = new ASTCheckerVisitor();
        n.accept(checker);
        Map<String, ERROR_KIND> errors = checker.getErrors();
        assertEquals(1, errors.size());
        assertTrue(errors.containsValue(ERROR_KIND.EMPTY_LIST));
    }

    @Test
    public void testParserDeuxStationsMemeNom() throws StationParserException, IOException {
        ASTNode n = parser.parse(new File(path + "stationsDeuxStationsMemeNom.txt"));
        ASTCheckerVisitor checker = new ASTCheckerVisitor();
        n.accept(checker);
        Map<String, ERROR_KIND> errors = checker.getErrors();
        assertEquals(1, errors.size());
        assertTrue(errors.containsValue(ERROR_KIND.DUPLICATE_STATION_NAME));
    }

    @Test
    public void testParserNomChaineVide() throws StationParserException, IOException {
        ASTNode n = parser.parse(new File(path + "stationsNomChaineVide.txt"));
        ASTCheckerVisitor checker = new ASTCheckerVisitor();
        n.accept(checker);
        Map<String, ERROR_KIND> errors = checker.getErrors();
        assertEquals(1, errors.size());
        assertTrue(errors.containsValue(ERROR_KIND.EMPTY_STATION_NAME));
    }

    @Test
    public void testParserNomChaineAvecQueTab() throws StationParserException, IOException {
        ASTNode n = parser.parse(new File(path + "stationsNomChaineAvecQueTab.txt"));
        ASTCheckerVisitor checker = new ASTCheckerVisitor();
        n.accept(checker);
        Map<String, ERROR_KIND> errors = checker.getErrors();
        assertEquals(1, errors.size());
        assertTrue(errors.containsValue(ERROR_KIND.EMPTY_STATION_NAME));
    }

    @Test
    public void testParserManqueLatitude() throws StationParserException, IOException {
        ASTNode n = parser.parse(new File(path + "stationsManqueLatitude.txt"));
        ASTCheckerVisitor checker = new ASTCheckerVisitor();
        n.accept(checker);
        Map<String, ERROR_KIND> errors = checker.getErrors();
        assertEquals(1, errors.size());
        assertTrue(errors.containsValue(ERROR_KIND.MISSING_DECLARATION));
    }

    @Test
    public void testParserManqueLongitude() throws StationParserException, IOException {
        ASTNode n = parser.parse(new File(path + "stationsManqueLongitude.txt"));
        ASTCheckerVisitor checker = new ASTCheckerVisitor();
        n.accept(checker);
        Map<String, ERROR_KIND> errors = checker.getErrors();
        assertEquals(1, errors.size());
        assertTrue(errors.containsValue(ERROR_KIND.MISSING_DECLARATION));
    }

    @Test
    public void testParserManqueCapacite() throws StationParserException, IOException {
        ASTNode n = parser.parse(new File(path + "stationsManqueCapacite.txt"));
        ASTCheckerVisitor checker = new ASTCheckerVisitor();
        n.accept(checker);
        Map<String, ERROR_KIND> errors = checker.getErrors();
        assertEquals(1, errors.size());
        assertTrue(errors.containsValue(ERROR_KIND.MISSING_DECLARATION));

    }

    @Test
    public void testParserDeuxLatitudeLongitudeManquante() throws StationParserException, IOException {
        ASTNode n = parser.parse(new File(path + "stationsDeuxLatitudesLongitudeManquante.txt"));
        ASTCheckerVisitor checker = new ASTCheckerVisitor();
        n.accept(checker);
        Map<String, ERROR_KIND> errors = checker.getErrors();
        assertEquals(2, errors.size());
        assertTrue(errors.containsValue(ERROR_KIND.DUPLICATE_DECLARATION));
        assertTrue(errors.containsValue(ERROR_KIND.MISSING_DECLARATION));
    }

    @Test
    public void testParserDeuxLongitudes() throws StationParserException, IOException {
        ASTNode n = parser.parse(new File(path + "stationsDeuxLongitudes.txt"));
        ASTCheckerVisitor checker = new ASTCheckerVisitor();
        n.accept(checker);
        Map<String, ERROR_KIND> errors = checker.getErrors();
        assertEquals(1, errors.size());
        assertTrue(errors.containsValue(ERROR_KIND.DUPLICATE_DECLARATION));
    }

    @Test
    public void testParserCapaciteNegatif() throws StationParserException, IOException {
        ASTNode n = parser.parse(new File(path + "stationsCapaciteNegatif.txt"));
        ASTCheckerVisitor checker = new ASTCheckerVisitor();
        n.accept(checker);
        Map<String, ERROR_KIND> errors = checker.getErrors();
        assertEquals(1, errors.size());
        assertTrue(errors.containsValue(ERROR_KIND.WRONG_NUMBER_VALUE));
    }

    @Test
    public void testParserCapaciteDecimaleNulle() throws StationParserException, IOException {
        ASTNode n = parser.parse(new File(path + "stationsCapaciteDecimaleNulle.txt"));
        ASTCheckerVisitor checker = new ASTCheckerVisitor();
        n.accept(checker);
        Map<String, ERROR_KIND> errors = checker.getErrors();
        assertEquals(1, errors.size());
        assertTrue(errors.containsValue(ERROR_KIND.WRONG_NUMBER_VALUE));
    }

    @Test
    public void testParserCapaciteDecimaleNonNulle() throws StationParserException, IOException {
        ASTNode n = parser.parse(new File(path + "stationsCapaciteDecimaleNonNulle.txt"));
        ASTCheckerVisitor checker = new ASTCheckerVisitor();
        n.accept(checker);
        Map<String, ERROR_KIND> errors = checker.getErrors();
        assertEquals(1, errors.size());
        assertTrue(errors.containsValue(ERROR_KIND.WRONG_NUMBER_VALUE));
    }

    @Test
    public void testParserToutesLesErreurs() throws StationParserException, IOException {
        ASTNode n = parser.parse(new File(path + "stationsRienNeVa.txt"));
        ASTCheckerVisitor checker = new ASTCheckerVisitor();
        n.accept(checker);
        Map<String, ERROR_KIND> errors = checker.getErrors();

        assertEquals(9, errors.size());
        assertTrue(errors.containsValue(ERROR_KIND.EMPTY_STATION_NAME));
        assertTrue(errors.containsValue(ERROR_KIND.DUPLICATE_STATION_NAME));
        assertTrue(errors.containsValue(ERROR_KIND.MISSING_DECLARATION));
        assertTrue(errors.containsValue(ERROR_KIND.DUPLICATE_DECLARATION));
        assertTrue(errors.containsValue(ERROR_KIND.WRONG_NUMBER_VALUE));
    }
}
