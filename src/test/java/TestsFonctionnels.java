import fr.ufc.l3info.oprog.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;


public class TestsFonctionnels {
    final String path = "./target/classes/data/";
    final String RIB_CORRECT = "11111-11111-11111111111-48";
    final String RIB_INCORRECT_CLE = "11111-11111-11111111111-11";
    final double DELTA_EURO = 0.001;
    final FabriqueVelo fabrique = FabriqueVelo.getInstance();
    Ville besancon;
    Exploitant baobab;

    @Ignore
    private void emprunterVelo(
            Station sEmprunt, Station sRetour, Abonne a,
            int anneeEmprunt, Month moisEmprunt, int jourEmprunt, int heureEmprunt, int minuteEmprunt,
            int anneeRetour, Month moisRetour, int jourRetour, int heureRetour, int minuteRetour
    ) {
        emprunterVelo(sEmprunt, sRetour, a,
                anneeEmprunt, moisEmprunt, jourEmprunt, heureEmprunt, minuteEmprunt, 0,
                anneeRetour, moisRetour, jourRetour, heureRetour, minuteRetour, 0);
    }

    @Ignore
    private void emprunterVelo(
            Station sEmprunt, Station sRetour, Abonne a,
            int anneeEmprunt, Month moisEmprunt, int jourEmprunt, int heureEmprunt, int minuteEmprunt, int secondeEmprunt,
            int anneeRetour, Month moisRetour, int jourRetour, int heureRetour, int minuteRetour, int secondeRetour
    ) {
        emprunterVelo(sEmprunt, sRetour, a,
                anneeEmprunt, moisEmprunt, jourEmprunt, heureEmprunt, minuteEmprunt, secondeEmprunt,
                anneeRetour, moisRetour, jourRetour, heureRetour, minuteRetour, secondeRetour, false);
    }

    @Ignore
    private void emprunterVelo(
            Station sEmprunt, Station sRetour, Abonne a,
            int anneeEmprunt, Month moisEmprunt, int jourEmprunt, int heureEmprunt, int minuteEmprunt, int secondeEmprunt,
            int anneeRetour, Month moisRetour, int jourRetour, int heureRetour, int minuteRetour, int secondeRetour, boolean casserVelo
    ) {
        assert (!a.estBloque());
        assert (sEmprunt != null);
        assert (sRetour != null);

        long debutEmpruntTime = 1000L *
                LocalDateTime.of(anneeEmprunt, moisEmprunt, jourEmprunt, heureEmprunt, minuteEmprunt, secondeEmprunt)
                        .toEpochSecond(ZoneOffset.UTC);
        long finEmpruntTime = 1000L *
                LocalDateTime.of(anneeRetour, moisRetour, jourRetour, heureRetour, minuteRetour, secondeRetour)
                        .toEpochSecond(ZoneOffset.UTC);

        Station spyStationEmprunt = Mockito.spy(sEmprunt);
        Mockito.when(spyStationEmprunt.maintenant()).thenReturn(debutEmpruntTime);
        // Il y a au moins un vélo dans la station normale
        int borneAvecVelo = 1;
        for (int i = 1; i <= spyStationEmprunt.capacite(); ++i) {
            if (spyStationEmprunt.veloALaBorne(i) != null) {
                borneAvecVelo = i;
                break;
            }
        }
        IVelo v = spyStationEmprunt.emprunterVelo(a, borneAvecVelo);
        assert (v != null);

        if (casserVelo) {
            v.abimer();
        }

        Station spyStationRendu = Mockito.spy(sRetour);
        Mockito.when(spyStationRendu.maintenant()).thenReturn(finEmpruntTime);

        // Il y a au moins une borne libre dans la station de retour
        int borneSansVelo = 1;
        for (int i = spyStationRendu.capacite() - 1; 1 <= i; --i) {
            if (spyStationRendu.veloALaBorne(i) == null) {
                borneSansVelo = i;
                break;
            }
        }
        int status = spyStationRendu.arrimerVelo(v, borneSansVelo);
        assert (status == 0);
    }

    @Before
    public void beforeFunc() throws IOException {
        besancon = new Ville();
        baobab = new Exploitant();
        besancon.initialiser(new File(path + "stationsBesancon.txt"));

        // Ajout de vélos aux stations de Besac
        for (int i = 0; i < 1000; ++i) {
            baobab.acquerirVelo(fabrique.construire('m'));
        }
        baobab.ravitailler(besancon);
    }

    /********** TESTS VILLE ************/
    @Test
    public void testFichier() throws IOException {
        Ville ville = new Ville();
        ville.initialiser(new File(path + "stationsBesancon.txt"));
        assertNotNull(ville.getStation("Gare Viotte"));
    }

    @Test
    public void testVilleCreerAbonne() {
        Abonne fred = besancon.creerAbonne("Freddd70", RIB_INCORRECT_CLE);
        assertNull(fred);
        fred = besancon.creerAbonne("Fred", RIB_INCORRECT_CLE);
        assertNotNull(fred);
        assertEquals("Fred", fred.getNom());
        assertTrue(fred.estBloque());
        fred.miseAJourRIB(RIB_CORRECT);
        assertFalse(fred.estBloque());
    }

    @Test
    public void testVilleCreerAbonne2() {
        Abonne fred = besancon.creerAbonne("Fred", RIB_CORRECT);
        assertFalse(fred.estBloque());
    }

    @Test
    public void testVilleGetStation() {
        Set<Station> setStations = new HashSet<>();
        Iterator<Station> it = besancon.iterator();
        assertTrue(it.hasNext());
        Station stationPrincipale = it.next();
        assertEquals("Gare Viotte", stationPrincipale.getNom());
        besancon.setStationPrincipale(besancon.getStation("Mairie"));
        for (int i = 1; i < 30; i++) {
            assertTrue(it.hasNext());
            it.next();
        }
        assertFalse(it.hasNext());
    }

    @Test
    public void testFacturationVille() {
        Abonne fred = besancon.creerAbonne("fred", RIB_CORRECT);
        Abonne jpg = besancon.creerAbonne("Jean-Phillipe Gros", RIB_CORRECT);

        emprunterVelo(
                besancon.getStation("Mairie"), besancon.getStation("Mairie"), fred,
                2020, Month.NOVEMBER, 1, 0, 0,
                2020, Month.DECEMBER, 1, 0, 0
        );

        emprunterVelo(
                besancon.getStation("Mairie"), besancon.getStation("Mairie"), jpg,
                2020, Month.NOVEMBER, 1, 0, 0,
                2020, Month.NOVEMBER, 2, 5, 0
        );

        Map<Abonne, Double> factureNovembre = besancon.facturation(11, 2020);
        assertEquals("Pas d'emprunt terminé", 0, factureNovembre.get(fred), DELTA_EURO);
        assertEquals("Un emprunt de 29h", 29 * 2.0, factureNovembre.get(jpg), DELTA_EURO);

        Map<Abonne, Double> factureDecembre = besancon.facturation(12, 2020);
        assertEquals("Un emprunt de trente jours", 30 * 24 * 2.0, factureDecembre.get(fred), DELTA_EURO);
        assertEquals("Pas d'emprunt", 0, factureDecembre.get(jpg), DELTA_EURO);
    }

    @Test
    public void testFabriqueVelo() {
        IVelo v = fabrique.construire('\0', "CADRE_ALUMINIUM", "FREINS_DISQUE");
        assertTrue(v.toString().contains("cadre aluminium"));
        assertTrue(v.toString().contains("freins à disque"));
        assertEquals(v.tarif(), 2.5, 0.01);

        IVelo v2 = fabrique.construire('\0', "CADRE_ALUMINIUM", "SUSPENSION_AVANT");
        assertTrue(v2.toString().contains("cadre aluminium"));
        assertTrue(v2.toString().contains("suspension avant"));
        assertEquals(v2.tarif(), 2.7, 0.01);

        IVelo v3 = fabrique.construire('\0', "CADRE_ALUMINIUM", "SUSPENSION_ARRIERE");
        assertTrue(v3.toString().contains("cadre aluminium"));
        assertTrue(v3.toString().contains("suspension arrière"));
        assertEquals(v3.tarif(), 2.7, 0.01);

        IVelo v4 = fabrique.construire('\0', "CADRE_ALUMINIUM", "ASSISTANCE_ELECTRIQUE");
        assertTrue(v4.toString().contains("cadre aluminium"));
        assertTrue(v4.toString().contains("assistance électrique"));
        assertEquals(v4.tarif(), 4.2, 0.01);

        IVelo v5 = fabrique.construire('\0', "CADRE_ALUMINIUM", "FREINS_DISQUE", "SUSPENSION_AVANT");
        assertTrue(v5.toString().contains("cadre aluminium"));
        assertTrue(v5.toString().contains("freins à disque"));
        assertTrue(v5.toString().contains("suspension avant"));
        assertEquals(v5.tarif(), 3, 0.01);

        IVelo v6 = fabrique.construire('\0', "CADRE_ALUMINIUM", "FREINS_DISQUE", "SUSPENSION_ARRIERE");
        assertTrue(v6.toString().contains("cadre aluminium"));
        assertTrue(v6.toString().contains("freins à disque"));
        assertTrue(v6.toString().contains("suspension arrière"));
        assertEquals(v6.tarif(), 3, 0.01);

        IVelo v7 = fabrique.construire('\0', "CADRE_ALUMINIUM", "FREINS_DISQUE", "ASSISTANCE_ELECTRIQUE");
        assertTrue(v7.toString().contains("cadre aluminium"));
        assertTrue(v7.toString().contains("freins à disque"));
        assertTrue(v7.toString().contains("assistance électrique"));
        assertEquals(v7.tarif(), 4.5, 0.01);

        IVelo v8 = fabrique.construire('\0', "CADRE_ALUMINIUM", "FREINS_DISQUE", "SUSPENSION_AVANT", "SUSPENSION_ARRIERE");
        assertTrue(v8.toString().contains("cadre aluminium"));
        assertTrue(v8.toString().contains("freins à disque"));
        assertTrue(v8.toString().contains("suspension avant"));
        assertTrue(v8.toString().contains("suspension arrière"));
        assertEquals(v8.tarif(), 3.5, 0.01);

        IVelo v9 = fabrique.construire('\0', "CADRE_ALUMINIUM", "FREINS_DISQUE", "SUSPENSION_AVANT", "ASSISTANCE_ELECTRIQUE");
        assertTrue(v9.toString().contains("cadre aluminium"));
        assertTrue(v9.toString().contains("freins à disque"));
        assertTrue(v9.toString().contains("suspension avant"));
        assertTrue(v9.toString().contains("assistance électrique"));
        assertEquals(v9.tarif(), 5, 0.01);

        IVelo v10 = fabrique.construire('\0', "FREINS_DISQUE", "ASSISTANCE_ELECTRIQUE");
        assertTrue(v10.toString().contains("freins à disque"));
        assertTrue(v10.toString().contains("assistance électrique"));
        assertEquals(v10.tarif(), 4.3, 0.01);

        IVelo v11 = fabrique.construire('\0', "FREINS_DISQUE", "SUSPENSION_AVANT");
        assertTrue(v11.toString().contains("freins à disque"));
        assertTrue(v11.toString().contains("suspension avant"));
        assertEquals(v11.tarif(), 2.8, 0.01);

        IVelo v12 = fabrique.construire('\0', "FREINS_DISQUE", "SUSPENSION_ARRIERE");
        assertTrue(v12.toString().contains("freins à disque"));
        assertTrue(v12.toString().contains("suspension arrière"));
        assertEquals(v12.tarif(), 2.8, 0.01);

        IVelo v13 = fabrique.construire('\0', "FREINS_DISQUE", "SUSPENSION_ARRIERE", "SUSPENSION_AVANT");
        assertTrue(v13.toString().contains("suspension arrière"));
        assertTrue(v13.toString().contains("freins à disque"));
        assertTrue(v13.toString().contains("suspension avant"));
        assertEquals(v13.tarif(), 3.3, 0.01);

        IVelo v14 = fabrique.construire('\0', "FREINS_DISQUE", "SUSPENSION_ARRIERE", "ASSISTANCE_ELECTRIQUE");
        assertTrue(v14.toString().contains("suspension arrière"));
        assertTrue(v14.toString().contains("freins à disque"));
        assertTrue(v14.toString().contains("assistance électrique"));
        assertEquals(v14.tarif(), 4.8, 0.01);

        IVelo v15 = fabrique.construire('\0', "FREINS_DISQUE", "SUSPENSION_AVANT", "ASSISTANCE_ELECTRIQUE");
        assertTrue(v15.toString().contains("suspension avant"));
        assertTrue(v15.toString().contains("freins à disque"));
        assertTrue(v15.toString().contains("assistance électrique"));
        assertEquals(v15.tarif(), 4.8, 0.01);

        IVelo v16 = fabrique.construire('\0', "FREINS_DISQUE", "SUSPENSION_AVANT", "ASSISTANCE_ELECTRIQUE",
                "SUSPENSION_ARRIERE");
        assertTrue(v16.toString().contains("suspension avant"));
        assertTrue(v16.toString().contains("freins à disque"));
        assertTrue(v16.toString().contains("assistance électrique"));
        assertTrue(v16.toString().contains("suspension arrière"));
        assertEquals(v16.tarif(), 5.3, 0.01);

        IVelo v17 = fabrique.construire('\0', "SUSPENSION_AVANT", "ASSISTANCE_ELECTRIQUE");
        assertTrue(v17.toString().contains("suspension avant"));
        assertTrue(v17.toString().contains("assistance électrique"));
        assertEquals(v17.tarif(), 4.5, 0.01);

        IVelo v18 = fabrique.construire('\0', "SUSPENSION_AVANT", "SUSPENSION_ARRIERE");
        assertTrue(v18.toString().contains("suspension avant"));
        assertTrue(v18.toString().contains("suspension arrière"));
        assertEquals(v18.tarif(), 3, 0.01);

        IVelo v19 = fabrique.construire('\0', "SUSPENSION_AVANT", "SUSPENSION_ARRIERE", "ASSISTANCE_ELECTRIQUE");
        assertTrue(v19.toString().contains("suspension avant"));
        assertTrue(v19.toString().contains("suspension arrière"));
        assertTrue(v19.toString().contains("assistance électrique"));
        assertEquals(v19.tarif(), 5, 0.01);

        IVelo v20 = fabrique.construire('\0', "SUSPENSION_ARRIERE", "ASSISTANCE_ELECTRIQUE");
        assertTrue(v20.toString().contains("assistance électrique"));
        assertTrue(v20.toString().contains("suspension arrière"));
        assertEquals(v20.tarif(), 4.5, 0.01);

        IVelo v21 = fabrique.construire('\0', "CADRE_ALUMINIUM", "SUSPENSION_ARRIERE", "ASSISTANCE_ELECTRIQUE");
        assertTrue(v21.toString().contains("cadre aluminium"));
        assertTrue(v21.toString().contains("suspension arrière"));
        assertTrue(v21.toString().contains("assistance électrique"));
        assertEquals(v21.tarif(), 4.7, 0.01);

        IVelo v22 = fabrique.construire('\0', "CADRE_ALUMINIUM", "SUSPENSION_AVANT", "ASSISTANCE_ELECTRIQUE");
        assertTrue(v22.toString().contains("cadre aluminium"));
        assertTrue(v22.toString().contains("suspension avant"));
        assertTrue(v22.toString().contains("assistance électrique"));
        assertEquals(v22.tarif(), 4.7, 0.01);

        IVelo v23 = fabrique.construire('\0', "CADRE_ALUMINIUM", "SUSPENSION_AVANT", "SUSPENSION_ARRIERE");
        assertTrue(v23.toString().contains("cadre aluminium"));
        assertTrue(v23.toString().contains("suspension avant"));
        assertTrue(v23.toString().contains("suspension arrière"));
        assertEquals(v23.tarif(), 3.2, 0.01);

        IVelo v24 = fabrique.construire('\0', "CADRE_ALUMINIUM", "ASSISTANCE_ELECTRIQUE", "SUSPENSION_ARRIERE");
        assertTrue(v24.toString().contains("cadre aluminium"));
        assertTrue(v24.toString().contains("assistance électrique"));
        assertTrue(v24.toString().contains("suspension arrière"));
        assertEquals(v24.tarif(), 4.7, 0.01);

        IVelo v25 = fabrique.construire('\0', "CADRE_ALUMINIUM", "FREINS_DISQUE", "SUSPENSION_ARRIERE",
                "ASSISTANCE_ELECTRIQUE");
        assertTrue(v25.toString().contains("cadre aluminium"));
        assertTrue(v25.toString().contains("freins à disque"));
        assertTrue(v25.toString().contains("suspension arrière"));
        assertTrue(v25.toString().contains("assistance électrique"));
        assertEquals(v25.tarif(), 5, 0.01);

        IVelo v26 = fabrique.construire('\0', "CADRE_ALUMINIUM", "SUSPENSION_ARRIERE", "SUSPENSION_AVANT",
                "ASSISTANCE_ELECTRIQUE");
        assertTrue(v26.toString().contains("cadre aluminium"));
        assertTrue(v26.toString().contains("suspension arrière"));
        assertTrue(v26.toString().contains("suspension avant"));
        assertTrue(v26.toString().contains("assistance électrique"));
        assertEquals(v26.tarif(), 5.2, 0.01);
    }

    @Test
    public void testFabriqueVeloConstruireOptionIncorrecte() {
        IVelo v = this.fabrique.construire(
                'a',
                "DIEODIOE",
                "DIOOEDIEOD",
                "ASSISTANCE_AVANT",
                "SUSPENSION_MILIEU",
                "MOTEUR_DE_COURSE",
                "SUSPENSION AVANT",
                "SUSPENSION_ARRIÈRE",
                "cadre_aluminium",
                null
        );

        String chaine = v.toString();

        assertTrue("Partie de base de la chaîne", chaine.startsWith("Vélo cadre mixte"));
        chaine = chaine.replaceAll("Vélo cadre mixte", "");
        assertFalse("Partie de base de la chaîne", chaine.startsWith("Vélo cadre mixte"));

        assertTrue("Partie de base de la chaîne", chaine.endsWith(" - 0.0 km"));
        chaine = chaine.replaceAll(" - 0.0 km", "");
        assertFalse("Partie de base de la chaîne", chaine.endsWith(" - 0.0 km"));

        assertEquals("", chaine);
    }

    @Test
    public void testRavitaillerRemplirStations() throws IOException {
        Ville ville = new Ville();
        ville.initialiser(new File(path + "stationsBesancon.txt"));
        this.baobab.ravitailler(ville);
        for (Station s : ville) {
            assertEquals("Nombre de vélos dans la station " + s.getNom() + " de la ville 1", (int) Math.floor(s.capacite() / 2.0), s.nbBornesLibres());
        }

        Ville ville2 = new Ville();
        ville2.initialiser(new File(path + "stationsOK.txt"));
        this.baobab.ravitailler(ville2);
        for (Station s : ville2) {
            assertEquals("Nombre de vélos dans la station " + s.getNom() + " de la ville 2", (int) Math.floor(s.capacite() / 2.0), s.nbBornesLibres());
        }

        Ville ville3 = new Ville();
        ville3.initialiser(new File(path + "stationsOK2.txt"));
        this.baobab.ravitailler(ville3);
        for (Station s : ville3) {
            assertEquals("Nombre de vélos dans la station " + s.getNom() + " de la ville 3", (int) Math.floor(s.capacite() / 2.0), s.nbBornesLibres());
        }
    }

    @Test
    public void testAbonneCasseVelo() {
        besancon.getStation("GareViotte");
        Abonne dd = besancon.creerAbonne("Fred Dadeau", RIB_CORRECT);

        emprunterVelo(
                besancon.getStation("Gare Viotte"),
                besancon.getStation("Belfort"),
                dd,
                2020, Month.MARCH, 8, 10, 39, 0,
                2020, Month.MARCH, 9, 12, 39, 0,
                true
        );

        assertTrue(dd.estBloque());
    }

    @Test
    public void testEntretienVelo() throws IOException {
        Ville v = new Ville();
        v.initialiser(new File(path + "stationsOK.txt"));
        Station s1 = v.getStation("21 - Avenue Fontaine Argent, Boulevard Diderot"); // cap = 12
        Station s2 = v.getStation("Avenue du Maréchal Foch"); // cap = 10

        Exploitant e = new Exploitant();

        for (int i = 0; i < 15; i++) {
            IVelo velo1 = new Velo();
            velo1.abimer();
            e.acquerirVelo(velo1);
        }
        for (int i = 0; i < 15; i++) {
            IVelo velo2 = new Velo();
            velo2.parcourir(1000);
            assert (velo2.prochaineRevision() < 0);
            e.acquerirVelo(velo2);
        }

        // Première tournée
        e.ravitailler(v);

        assertEquals("Pas de vélos dans la station 1", s1.capacite(), s1.nbBornesLibres());
        assertEquals("Pas de vélos dans la station 2", s2.capacite(), s2.nbBornesLibres());

        // Réparation des vélos
        e.entretenirVelos();

        // Deuxième tournée
        e.ravitailler(v);

        assertEquals("Station 1 ravitaillée", (int) Math.floor(s1.capacite() / 2.0), s1.nbBornesLibres());
        assertEquals("Station 2 ravitaillée", (int) Math.floor(s2.capacite() / 2.0), s2.nbBornesLibres());
    }

    @Test
    public void testRavitaillerRecupereLesVelos() {
        // On abime tous les vélos de Besançon (c'est pas gentil)
        for (Station s : besancon) {
            for (int i = 0; i < s.capacite() / 2; i++) {
                IVelo v = s.veloALaBorne(i);
                if (v != null) {
                    v.abimer();
                }
            }

            for (int i = s.capacite() / 2; i < s.capacite(); i++) {
                IVelo v = s.veloALaBorne(i);
                if (v != null) {
                    v.parcourir(10000);
                }
            }
        }

        baobab.ravitailler(besancon);

        for (Station s : besancon) {
            assertEquals(
                    "Station " + s.getNom() + " ravitaillée",
                    (int) Math.floor(s.capacite() / 2.0),
                    s.nbBornesLibres()
            );

            for (int i = 0; i < s.capacite(); i++) {
                IVelo v = s.veloALaBorne(i);
                if (v != null) {
                    assertFalse(v.estAbime());
                    assertFalse(v.prochaineRevision() < 0);
                }
            }
        }
    }
}

