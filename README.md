# Vélocité

Fabian DEVEL & Nathanaël HOUN — L3 Informatique — UE Outils pour la programmation

Application de gestion d'emprunts de vélos, car prendre le vélo, c'est aussi prendre soin de la planète

## Objectifs

Réalisé pendant l'UE d'Outils pour la programmation (L3 - S5), ce projet est entièrement testé à l'aide de Junit, et de
Jacoco pour évaluer le coverage. C'est aussi une introduction à l'utilisation de l'outil de build maven pour Java.

## Construire le projet

Après avoir téléchargé le dépôt, construire le projet et lancer les tests avec Maven.

```bash
mvn build
mvn test
```
